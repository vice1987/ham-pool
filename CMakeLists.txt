project(ham-pool)

cmake_minimum_required(VERSION 2.8.9)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

### If CMAKE_BUILD_TYPE has not been provided, set is as Debug

if( NOT CMAKE_BUILD_TYPE )
	set(CMAKE_BUILD_TYPE Debug)
endif()

### Look for required packages

find_package(Boost 1.42 COMPONENTS program_options system filesystem thread REQUIRED)
find_package(Sparsehash REQUIRED)
find_package(ZLIB REQUIRED)

### Set executable path

set(EXECUTABLE_OUTPUT_PATH  ${PROJECT_BINARY_DIR}/bin/)

### Set include directories

include_directories(${PROJECT_SOURCE_DIR}/src)
include_directories(${PROJECT_SOURCE_DIR}/src/3d_party/bamtools-2.3.0)
include_directories(${Boost_INCLUDE_DIRS})

### 3d-party libraries

## hash libraries
file(GLOB 3D_PARTY_LIBS 
	${PROJECT_SOURCE_DIR}/src/3d_party/city/*.cc
	${PROJECT_SOURCE_DIR}/src/3d_party/murmur/*.cc
)
add_library( hashlibs-static STATIC ${3D_PARTY_LIBS} )


## bamtools
#add_subdirectory( ${PROJECT_SOURCE_DIR}/src/3d_party/bamtools-2.3.0 )

## bio-bwa
add_subdirectory( ${PROJECT_SOURCE_DIR}/src/3d_party/bwa )

### compiler flags (warnings and c++11 standard)

if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang") # Clang compiler flags
		include(${CMAKE_SOURCE_DIR}/cmake/ClangSettings.txt)
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU") # GCC compiler flags
		include(${CMAKE_SOURCE_DIR}/cmake/GnuSettings.txt)
	endif()
endif()

set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11" )

file(GLOB HAM_FILES
    ${PROJECT_SOURCE_DIR}/src/common/*.cc
	${PROJECT_SOURCE_DIR}/src/data_structures/*.cc
	${PROJECT_SOURCE_DIR}/src/errors/*.cc
	${PROJECT_SOURCE_DIR}/src/graph/*.cc
	${PROJECT_SOURCE_DIR}/src/io/*.cc
	${PROJECT_SOURCE_DIR}/src/modules/*.cc
	${PROJECT_SOURCE_DIR}/src/options/*.cc
)

add_library( ham-static ${HAM_FILES} )

### HAM

add_executable(ham ${PROJECT_SOURCE_DIR}/src/ham.cc)

target_link_libraries(ham ham-static)
target_link_libraries(ham hashlibs-static)
target_link_libraries(ham bwa-static)
target_link_libraries(ham ${Boost_LIBRARIES})
target_link_libraries(ham ${ZLIB_LIBRARIES})
#target_link_libraries(ham bamtools-static)

### TESTS

add_executable(bf-test ${PROJECT_SOURCE_DIR}/src/test/bf_test.cc)

target_link_libraries(bf-test ham-static)
target_link_libraries(bf-test hashlibs-static)
target_link_libraries(bf-test bwa-static)
target_link_libraries(bf-test ${Boost_LIBRARIES})
target_link_libraries(bf-test ${ZLIB_LIBRARIES})

### Slow All-against-all

#add_executable(slow-aaa ${PROJECT_SOURCE_DIR}/src/slow_aaa.cc)

#target_link_libraries(slow-aaa ham-static)
#target_link_libraries(slow-aaa hashlibs-static)
#target_link_libraries(slow-aaa bwa-static)
#target_link_libraries(slow-aaa ${Boost_LIBRARIES})
#target_link_libraries(slow-aaa ${ZLIB_LIBRARIES})
