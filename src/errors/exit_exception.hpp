/*
 * File:   exit_exception.hpp
 * Author: riccardo
 *
 * Created on January 9, 2014, 12:36 PM
 */

#ifndef EXIT_EXCEPTION_HPP
#define	EXIT_EXCEPTION_HPP

#include <exception>

class ExitException : public std::exception
{

public:

	int code;

public:

	ExitException( int c );

};

#endif	/* EXIT_EXCEPTION_HPP */

