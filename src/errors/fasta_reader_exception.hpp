#ifndef FASTA_READER_EXCEPTION_HPP
#define FASTA_READER_EXCEPTION_HPP

#include <string>
#include <exception>


class FastaReaderException : public std::exception
{

private:

	std::string m_comment;

public:

	FastaReaderException(const char *comment);
    FastaReaderException(const std::string &comment);
	FastaReaderException(const std::string *comment);

    ~FastaReaderException() throw();

	const char* what() const throw();

}; // class IncorrectFormat


#endif /* FASTA_READER_EXCEPTION_HPP */
