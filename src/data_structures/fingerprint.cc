/*
 * File:   fingerprint.cc
 * Author: riccardo
 *
 * Created on November 27, 2013, 5:50 PM
 */

#include "fingerprint.hpp"


Fingerprint::Fingerprint() { }


void Fingerprint::push_back( const Kmer& kmer )
{
	m_kmers.push_back(kmer);
}


void Fingerprint::push_back( const size_t gap )
{
	m_gaps.push_back(gap);
}


void Fingerprint::push_back( const Kmer& kmer, size_t gap )
{
	m_kmers.push_back(kmer);
	m_gaps.push_back(gap);
}


void Fingerprint::clear()
{
	m_kmers.clear();
	m_gaps.clear();
}


size_t Fingerprint::kmers() const
{
	return m_kmers.size();
}


size_t Fingerprint::gaps() const
{
	return m_gaps.size();
}


const Kmer& Fingerprint::kmer_at( const size_t& i ) const
{
	return m_kmers.at(i);
}


Kmer& Fingerprint::kmer_at( const size_t& i )
{
	return m_kmers.at(i);
}


const size_t& Fingerprint::gap_at( const size_t& i ) const
{
	return m_gaps.at(i);
}


size_t& Fingerprint::gap_at( const size_t& i )
{
	return m_gaps.at(i);
}
