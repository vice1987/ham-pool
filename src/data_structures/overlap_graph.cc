#include "overlap_graph.hpp"

#include <iostream>
#include <fstream>
#include <stack>

#include "common/constants.hpp"

OverlapGraph::OverlapGraph( const std::list<Overlap>& oll, const SequenceCollection& fasta, int32_t max_tail )
{
	this->init_graph(oll,fasta,max_tail);
	this->init_vertices_index();
}


// TODO: return as included contig the one with smaller tails
int32_t OverlapGraph::check_inclusion( const Overlap& o, const SequenceCollection& fasta, int32_t max_tail )
{
	int32_t rt,lt;
	
	// check r in q
	lt = o.r_beg;
	rt = static_cast<int32_t>(fasta[o.rid].length()) - o.r_end;
	if( lt <= max_tail && rt <= max_tail ) return o.rid;

	// check q in r
	lt = o.q_beg;
	rt = static_cast<int32_t>(fasta[o.qid].length()) - o.q_end;
	if( lt <= max_tail && rt <= max_tail ) return o.qid;

	// no inclusion detected
	return -1;
}


void OverlapGraph::init_graph( const std::list<Overlap>& oll, const SequenceCollection& fasta, int32_t max_tail )
{
	std::set< int32_t > ids; // set of contig identifiers
	std::set< int32_t > incl_ids; // set of identifiers of included contigs
	std::map< int32_t, Vertex > id2vertex;
	
	// retrieve all the contig identifiers from the blocks
	for( auto& o : oll )
	{
		// check for inclusion
		int32_t incl_id = this->check_inclusion(o,fasta,max_tail);
		
		if( incl_id != -1 ) incl_ids.insert( incl_id ); // overlap represents an inclusion

		if( incl_id != o.rid ) ids.insert( o.rid );
		if( incl_id != o.qid ) ids.insert( o.qid );
	}

	// create vertices and add their property
	for( auto id : ids )
	{
		if( incl_ids.find(id) != incl_ids.end() ) continue;

		Vertex v = boost::add_vertex(*this);
		boost::put( boost::vertex_kind_t(), *this, v, id );
		id2vertex[id] = v;
	}
	
	// initialize edges
	for( auto& o : oll )
	{
		// if one of the two contig is an "included" sequence, skip the overlap
		if( incl_ids.find(o.qid) != incl_ids.end() or incl_ids.find(o.rid) != incl_ids.end() ) continue;

		int32_t f_id = o.qid;
		int32_t g_id = o.rid;
		
		int32_t f_beg = f_id < g_id ? o.q_beg : o.r_beg;
		int32_t f_end = f_id < g_id ? o.q_end-1 : o.r_end-1;
		int32_t g_beg = g_id < f_id ? o.q_beg : o.r_beg;
		int32_t g_end = g_id < f_id ? o.q_end-1 : o.r_end-1;

		if( o.is_rev ) std::swap(g_beg,g_end);

		const Sequence& f = fasta[f_id];
		const Sequence& g = fasta[g_id];

		int32_t f_left  = f_beg;
		int32_t f_right = f.length()-f_end-1;
		int32_t g_left  = g_beg < g_end ? g_beg : g.length()-g_beg-1;
		int32_t g_right = g_beg < g_end ? g.length()-g_end-1 : g_end;

		OLPGEdgeInfo e_info;

		// inconsistent alignment (either the left or the right ends are too long in both contigs)
		// add dashed_edge
		
		if( (f_left > max_tail && g_left > max_tail) || (f_right > max_tail && g_right > max_tail) ) // this should not happen (long tails had been filtered before)
		{
			e_info.etype = OLPG_ETYPE_DASHED;
		} 
		else // consistent alignment => add bidirected edge
		{
			// compute left and right tails according to the original orientation of g
			int32_t g_left  = g_beg < g_end ? g_beg : g_end;
			int32_t g_right = g_beg < g_end ? g.length()-g_end-1 : g.length()-g_beg-1;
			
			e_info.etype = OLPG_ETYPE_SOLID;
			
			ExtInfo ext_info_1, ext_info_2;
			
			ext_info_1.gid = f_id < g_id ? g_id : f_id; 
			ext_info_2.gid = g_id < f_id ? g_id : f_id; 
			
			if( f_beg > max_tail ) // overlap: f suffix / g prefix
			{
				if( g_beg < g_end ) // g => forward
				{
					e_info.type_1  = f_id < g_id ? OLPG_BDETYPE_IN : OLPG_BDETYPE_OUT;
					e_info.type_2  = g_id < f_id ? OLPG_BDETYPE_IN : OLPG_BDETYPE_OUT;
					
					ext_info_1.start = f_id < g_id ? g_end+1 : f_beg-1 ; ext_info_1.end = f_id < g_id ? g.length()-1 : 0;
					ext_info_1.prev_tail = f_id < g_id ? f_right : g_left;
					
					ext_info_2.start = g_id < f_id ? g_end+1 : f_beg-1 ; ext_info_2.end = g_id < f_id ? g.length()-1 : 0;
					ext_info_2.prev_tail = g_id < f_id ? f_right : g_left;
				}
				else // g => reverse complemented
				{
					e_info.type_1  = f_id < g_id ? OLPG_BDETYPE_OUT : OLPG_BDETYPE_OUT;
					e_info.type_2  = g_id < f_id ? OLPG_BDETYPE_OUT : OLPG_BDETYPE_OUT;
					
					ext_info_1.start = f_id < g_id ? g_end-1 : f_beg-1 ; ext_info_1.end = f_id < g_id ? 0 : 0;
					ext_info_1.prev_tail = f_id < g_id ? f_right : g_right;
					
					ext_info_2.start = g_id < f_id ? g_end-1 : f_beg-1 ; ext_info_2.end = g_id < f_id ? 0 : 0 ;
					ext_info_2.prev_tail = g_id < f_id ? f_right : g_right;
				}
			} 
			else // overlap: f prefix / g suffix
			{
				if( g_beg < g_end ) // g => forward
				{
					e_info.type_1  = f_id < g_id ? OLPG_BDETYPE_OUT : OLPG_BDETYPE_IN;
					e_info.type_2  = g_id < f_id ? OLPG_BDETYPE_OUT : OLPG_BDETYPE_IN;
					
					ext_info_1.start = f_id < g_id ? g_beg-1 : f_end+1 ; ext_info_1.end = f_id < g_id ? 0 : f.length()-1;
					ext_info_1.prev_tail = f_id < g_id ? f_left : g_right;
					
					ext_info_2.start = g_id < f_id ? g_beg-1 : f_end+1 ; ext_info_2.end = g_id < f_id ? 0 : f.length()-1;
					ext_info_2.prev_tail = g_id < f_id ? f_left : g_right;
				}
				else // g => reverse complemented
				{
					e_info.type_1  = f_id < g_id ? OLPG_BDETYPE_IN : OLPG_BDETYPE_IN;
					e_info.type_2  = g_id < f_id ? OLPG_BDETYPE_IN : OLPG_BDETYPE_IN;
					
					ext_info_1.start = f_id < g_id ? g_beg+1 : f_end+1 ; ext_info_1.end = f_id < g_id ? g.length()-1 : f.length()-1; // possible error
					ext_info_1.prev_tail = f_id < g_id ? f_left : g_left;
					
					ext_info_2.start = g_id < f_id ? g_beg+1 : f_end+1 ; ext_info_2.end = g_id < f_id ? g.length()-1 : f.length()-1; // possible error
					ext_info_2.prev_tail = g_id < f_id ? f_left : g_left;
				}
			}
			
			e_info.pos_1.push_back(ext_info_1);
			e_info.pos_2.push_back(ext_info_2);
		}
		
		if( e_info.etype != OLPG_ETYPE_DASHED ) // TODO: handle dashed edges too
		{
			Edge e = boost::add_edge( id2vertex[f_id], id2vertex[g_id], *this ).first;
			boost::put( boost::edge_kind_t(), *this, e, e_info );
		}
	}
}



void OverlapGraph::init_vertices_index()
{
	// obtain a property map for the vertex_index property
	m_index = boost::get( boost::vertex_index, *this );
	
	// initialize the vertex_index property values
	OverlapGraph::VertexIterator vi, vend; size_t cnt = 0; //boost::graph_traits< ContigGraph >::vertices_size_type cnt = 0;
	for( boost::tie(vi,vend) = boost::vertices(*this); vi != vend; ++vi) boost::put(m_index, *vi, cnt++);
}


// return length of edge source(e)->target(e)
uint32_t OverlapGraph::get_out_edge_len( const Edge &e )
{
	OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, boost::source(e,*this) );
	OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, boost::target(e,*this) );
	OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, e);
	
	uint32_t out_edge_len = 0;
	std::list< ExtInfo > &pos = v_info < w_info ? e_info.pos_1 : e_info.pos_2;
	
	for( std::list<ExtInfo>::iterator it = pos.begin(); it != pos.end(); ++it )
		out_edge_len += abs( it->start - it->end );
	
	return out_edge_len;
	//return ( v_info.id() < w_info.id() ? abs(e_info.start_1 - e_info.end_1) : abs(e_info.start_2 - e_info.end_2) );
}


std::vector< OverlapGraph::OutEdge > OverlapGraph::get_out_edge_heap( const Vertex &v )
{
	std::vector<OutEdge> heap;
	
	OutEdgeIterator e_it, e_end;
	for( boost::tie(e_it,e_end) = boost::out_edges(v,*this); e_it != e_end; ++e_it )
	{
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
		if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
		
		OutEdge oe;
		oe.edge = *e_it;
		oe.len = this->get_out_edge_len(*e_it);
		heap.push_back(oe);
	}
	
	std::make_heap( heap.begin(), heap.end() );
	return heap;
}


std::vector< OverlapGraph::OutEdge > OverlapGraph::get_out_edge_heap( const Vertex &v, const bool type )
{
	std::vector<OutEdge> heap;
	
	OutEdgeIterator e_it, e_end;
	for( boost::tie(e_it,e_end) = boost::out_edges(v,*this); e_it != e_end; ++e_it )
	{
		OLPG_VertexInfo  v_info = boost::get( boost::vertex_kind_t(), *this, boost::source(*e_it,*this) );
		OLPG_VertexInfo  w_info = boost::get( boost::vertex_kind_t(), *this, boost::target(*e_it,*this) );
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
		
		if( e_info.etype == OLPG_ETYPE_DASHED ) continue; // skip dashed edges
		
		bool v_type = v_info < w_info ? e_info.type_2 : e_info.type_1;
		if( v_type != type ) continue;
		
		OutEdge oe;
		oe.edge = *e_it;
		oe.len = this->get_out_edge_len(*e_it);
		heap.push_back(oe);
	}
	
	std::make_heap( heap.begin(), heap.end() );
	return heap;
}


void OverlapGraph::remove_transitive_edges()
{
	size_t v_num = boost::num_vertices(*this);
	
	// initialization of data structures for the traversal
	const int FUZZ = 100;
	std::vector<uint8_t> mark( v_num, OLPG_VTYPE_VACANT );
	std::set<Edge> reduce;
	
	VertexIterator v_it,v_end;
	OutEdgeIterator e_it, e_end;
	
	for( boost::tie(v_it,v_end) = boost::vertices(*this); v_it != v_end; ++v_it ) // for v in V
	{
		uint32_t longest = 0;
		OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, *v_it );
		
		for( boost::tie(e_it,e_end) = boost::out_edges(*v_it,*this); e_it != e_end; ++e_it )
		{
			OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
			if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
			
			Vertex w = boost::target(*e_it,*this);
			OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
			
			mark[ m_index[w] ] = OLPG_VTYPE_INPLAY;
			
			uint32_t len = 0;
			std::list<ExtInfo> &pos = v_info < w_info ? e_info.pos_1 : e_info.pos_2;
			for( std::list<ExtInfo>::iterator it = pos.begin(); it != pos.end(); ++it ) len += abs( it->start - it->end );
			
			//uint32_t len = v_info.id() < w_info.id() ? abs(e_info.start_1 - e_info.end_1) : abs(e_info.start_2 - e_info.end_2);
			if( len > longest ) longest = len;
		}
		
		longest += FUZZ;
		
		std::vector< OutEdge > v_out_edges = get_out_edge_heap(*v_it);
		while( v_out_edges.size() > 0 ) // for each edge v->w in order of length
		{
			OutEdge vw_oe = v_out_edges.front(); // v->w
			std::pop_heap(v_out_edges.begin(),v_out_edges.end()); v_out_edges.pop_back();
			
			OLPGEdgeInfo vw_info = boost::get( boost::edge_kind_t(), *this, vw_oe.edge);
			if( vw_info.etype == OLPG_ETYPE_DASHED ) continue;
			
			uint32_t vw_len = this->get_out_edge_len(vw_oe.edge); // len(v->w)
			
			Vertex w = boost::target(vw_oe.edge,*this);
			OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
			
// 			int32_t start_vw = v_info.id() < w_info.id() ? vw_info.start_1 : vw_info.start_2;
// 			int32_t end_vw = v_info.id() < w_info.id() ? vw_info.end_1 : vw_info.end_2;
// 			bool w_type = ( start_vw == 0 || end_vw == 0 ) ? OLPG_BDETYPE_BEGIN : OLPG_BDETYPE_END;
			
			if( mark[ m_index[w] ] == OLPG_VTYPE_INPLAY )
			{
				bool v_type = v_info < w_info ? vw_info.type_1 : vw_info.type_2;
				
				std::vector< OutEdge > w_out_edges = get_out_edge_heap(w,!v_type);
				while( w_out_edges.size() > 0 )
				{
					OutEdge wx_oe = w_out_edges.front(); // w->x
					std::pop_heap(w_out_edges.begin(),w_out_edges.end()); w_out_edges.pop_back();
					
					OLPGEdgeInfo wx_info = boost::get( boost::edge_kind_t(), *this, wx_oe.edge);
					if( wx_info.etype == OLPG_ETYPE_DASHED ) continue;
					
					uint32_t wx_len = this->get_out_edge_len(wx_oe.edge); // len(w->x)
					
					if( wx_len+vw_len > longest ) continue;
					
					Vertex x = boost::target(wx_oe.edge,*this);
					if( mark[ m_index[x] ] == OLPG_VTYPE_INPLAY ) mark[ m_index[x] ] = OLPG_VTYPE_ELIMINATED;
				}
			}
		}
		
		
		v_out_edges = get_out_edge_heap(*v_it);
		while( v_out_edges.size() > 0 ) // for each edge v->w in order of length
		{
			OutEdge vw_oe = v_out_edges.front(); // v->w
			std::pop_heap(v_out_edges.begin(),v_out_edges.end()); v_out_edges.pop_back();
			
			OLPGEdgeInfo vw_info = boost::get( boost::edge_kind_t(), *this, vw_oe.edge);
			if( vw_info.etype == OLPG_ETYPE_DASHED ) continue;
			
			Vertex w = boost::target(vw_oe.edge,*this);
			
			OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );			
			bool v_type = v_info < w_info ? vw_info.type_1 : vw_info.type_2;
			
// 			int32_t start_vw = v_info.id() < w_info.id() ? vw_info.start_1 : vw_info.start_2;
// 			int32_t end_vw = v_info.id() < w_info.id() ? vw_info.end_1 : vw_info.end_2;
// 			bool w_type = ( start_vw == 0 || end_vw == 0 ) ? OLPG_BDETYPE_BEGIN : OLPG_BDETYPE_END;
			
			Edge w_succ;
			
			std::vector< OutEdge > w_out_edges = get_out_edge_heap(w,!v_type);
			if( w_out_edges.size() > 0 ) w_succ = w_out_edges.front().edge;
			
			while( w_out_edges.size() > 0 )
			{
				OutEdge wx_oe = w_out_edges.front(); // w->x
				std::pop_heap(w_out_edges.begin(),w_out_edges.end()); w_out_edges.pop_back();
				
				OLPGEdgeInfo wx_info = boost::get( boost::edge_kind_t(), *this, wx_oe.edge);
				if( wx_info.etype == OLPG_ETYPE_DASHED ) continue;
				
				uint32_t wx_len = this->get_out_edge_len(wx_oe.edge); // len(w->x)
				
				if( wx_len < FUZZ || wx_oe.edge == w_succ )
				{
					Vertex x = boost::target(wx_oe.edge,*this);
					if( mark[ m_index[x] ] == OLPG_VTYPE_INPLAY ) mark[ m_index[x] ] = OLPG_VTYPE_ELIMINATED;
				}
			}
		}
		
		for( boost::tie(e_it,e_end) = boost::out_edges(*v_it,*this); e_it != e_end; ++e_it )
		{
			OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
			if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
			
			Vertex w = boost::target(*e_it,*this);
			if( mark[ m_index[w] ] == OLPG_VTYPE_ELIMINATED ) reduce.insert(*e_it);
			
			mark[ m_index[w] ] = OLPG_VTYPE_VACANT;
		}
	}
	
	// removing edges from graph
	for( std::set<Edge>::iterator it = reduce.begin(); it != reduce.end(); ++it )
		boost::remove_edge(*it,*this);
}



void OverlapGraph::collapse_paths()
{
	VertexIterator v_it,v_end;
	size_t v_num = boost::num_vertices(*this);
	
	std::vector<bool> visited(v_num,false);
	
	// find junction vertices
	std::vector<Vertex> junction_vertices;
	std::vector<bool> is_junction(v_num,false);
	for( boost::tie(v_it,v_end) = boost::vertices(*this); v_it != v_end; ++v_it )
		if( this->is_junction_vertex(*v_it) ){ junction_vertices.push_back(*v_it); is_junction.at(m_index[*v_it]) = true; }
	
	// for each junction vertex v, DFS + path compression
	for( std::vector<Vertex>::iterator v = junction_vertices.begin(); v != junction_vertices.end(); ++v )
	{
		visited.at(m_index[*v]) = true;
		
		std::list<Vertex> compressed;
		std::stack<Vertex> adj_vertices; // v's adjacent vertices
		
		AdjacencyIterator a_it, a_end;
		for( boost::tie(a_it,a_end) = boost::adjacent_vertices(*v,*this); a_it != a_end; ++a_it ) adj_vertices.push(*a_it);
		
		while( not adj_vertices.empty() ) // for each adjacent vertex w
		{
			Vertex w = adj_vertices.top(); adj_vertices.pop();
			
			if( visited.at(m_index[w]) ) continue; // skip nodes on paths already visited (cycles)
			if( is_junction.at(m_index[w]) ) continue; // skip paths which do not need compression
			
			visited.at(m_index[w]) = true;
			
			Edge e = boost::edge( *v, w, *this ).first;
			
			OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
			OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, *v );
			
			OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, e);
			if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
			
			bool v_new_in_type, w_in_type;
			std::list< ExtInfo > pos_1, pos_2;
			
			if( v_info < w_info )
			{
				w_in_type = e_info.type_1;
				v_new_in_type = e_info.type_2;
				pos_1.splice( pos_1.end(), e_info.pos_1 ); //push_back( std::make_pair(e_info.start_1,e_info.end_1) );
				pos_2.splice( pos_2.begin(), e_info.pos_2 ); //push_back( std::make_pair(e_info.start_2,e_info.end_2) );
			}
			else
			{
				w_in_type = e_info.type_2;
				v_new_in_type = e_info.type_1;
				pos_1.splice( pos_1.end(), e_info.pos_2 ); //push_back( std::make_pair(e_info.start_2,e_info.end_2) );
				pos_2.splice( pos_2.begin(), e_info.pos_1 ); //push_back( std::make_pair(e_info.start_1,e_info.end_1) );
			}
			
			Vertex w_new; bool v_new_out_type;
			boost::tie(w_new,v_new_out_type) = this->collapse_path( w, w_in_type, is_junction, pos_1, pos_2, visited, compressed );
			
			OLPG_VertexInfo w_new_info = boost::get( boost::vertex_kind_t(), *this, w_new );
			
			OLPGEdgeInfo new_edge_info;
			new_edge_info.etype  = OLPG_ETYPE_SOLID;
			
			if( v_info == w_new_info )
			{
				new_edge_info.type_1 = v_new_out_type;
				new_edge_info.type_2 = v_new_in_type;
				new_edge_info.pos_1.splice( new_edge_info.pos_1.begin(), pos_1 );
				new_edge_info.pos_2.splice( new_edge_info.pos_2.begin(), pos_2 );
			}
			else
			{
				new_edge_info.type_1 = v_info < w_new_info? v_new_out_type : v_new_in_type;
				new_edge_info.type_2 = w_new_info < v_info ? v_new_out_type : v_new_in_type;
				new_edge_info.pos_1.splice( new_edge_info.pos_1.begin(), (v_info < w_new_info ? pos_1 : pos_2) );
				new_edge_info.pos_2.splice( new_edge_info.pos_2.begin(), (w_new_info < v_info ? pos_1 : pos_2) );
			}
			
			// add new edge between the junction nodes
			Edge new_edge = boost::add_edge(*v,w_new,*this).first;
			boost::put( boost::edge_kind_t(), *this, new_edge, new_edge_info );
			//std::cout << "added edge = <" << v_info.id().first << "," << v_info.id().second << "> -- <"
			//	<< w_new_info.id().first << "," << w_new_info.id().second << ">" << std::endl;
		}
		
		for( std::list<Vertex>::iterator it = compressed.begin(); it != compressed.end(); ++it )
		{
			boost::clear_vertex(*it,*this);
			boost::remove_vertex(*it,*this);
		}
	}
	
	// re-initialize vertices' index
	this->init_vertices_index();
}


std::pair<OverlapGraph::Vertex,bool> OverlapGraph::collapse_path( 
	const Vertex &v, bool v_in_type, std::vector<bool> &is_junction,
	std::list< ExtInfo >  &pos_1, std::list< ExtInfo >  &pos_2,
	std::vector<bool> &visited, std::list<Vertex> &compressed )
{
	visited.at(m_index[v]) = true;
	
	Vertex w;
	bool w_in_type, v_out_type;
	
	OutEdgeIterator e_it, e_end;
	for( boost::tie(e_it,e_end) = boost::out_edges(v,*this); e_it != e_end; ++e_it )
	{
		w = boost::target(*e_it,*this);
		OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
		OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, v );
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
		
		v_out_type = v_info < w_info ? e_info.type_2 : e_info.type_1; // direction of arrow pointing vertex v
		w_in_type = v_info < w_info ? e_info.type_1 : e_info.type_2; // direction of arrow pointing vertex w
		
		if( v_in_type == v_out_type ) continue;
		
		if( v_info < w_info )
		{
			pos_1.splice( pos_1.end(), e_info.pos_1 );
			pos_2.splice( pos_2.begin(), e_info.pos_2 );
		}
		else
		{
			pos_1.splice( pos_1.end(), e_info.pos_2 );
			pos_2.splice( pos_2.begin(), e_info.pos_1 );
		}
		
		//std::cout << "removing <" << v_info.id().first << "," << v_info.id().second << ">";// << std::endl;
		compressed.push_back(v);
		//boost::clear_vertex(v,*this);
		//boost::remove_vertex(v,*this);
		
		if(is_junction.at(m_index[w])) return std::make_pair(w,w_in_type);
			else return this->collapse_path( w, w_in_type, is_junction, pos_1, pos_2, visited, compressed );
	}
	
	std::cerr << "ERROR in OverlapGraph::collapse_path: control must NOT reach this point" << std::endl;
	return std::make_pair(w,w_in_type);
}


void OverlapGraph::degree( const Vertex &v, int &in_deg, int &out_deg )
{
	in_deg  = 0;
	out_deg = 0;
	
	OutEdgeIterator e_it, e_end;
	for( boost::tie(e_it,e_end) = boost::out_edges(v,*this); e_it != e_end; ++e_it )
	{
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
		if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
		
		Vertex w = boost::target(*e_it,*this);
		
		OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
		OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, v );
		
		bool type = v_info < w_info ? e_info.type_2  : e_info.type_1;
		
		if( type == OLPG_BDETYPE_IN ) in_deg++; else out_deg++;
	}
}


bool OverlapGraph::is_junction_vertex( const Vertex &v )
{
	int in_deg = 0, out_deg = 0;
	this->degree( v, in_deg, out_deg );
	
	return (in_deg != 1 || out_deg != 1);
}


void OverlapGraph::build_unitigs( const SequenceCollection& fasta, std::list<Unitig>& unitigs )
{
	size_t v_num = boost::num_vertices(*this);
	std::vector<bool> visited(v_num,false);
	
	VertexIterator v_it,v_end;
	for( boost::tie(v_it,v_end) = boost::vertices(*this); v_it != v_end; ++v_it ) // for each vertex v
	{
		if( visited.at(m_index[*v_it]) ) continue; // skip vertex, if it has been already visited
		
		int in_deg, out_deg; this->degree( *v_it, in_deg, out_deg );
		if( in_deg != 0 && out_deg != 0 ) continue; // skip if it's a inner vertex
		
		visited.at(m_index[*v_it]) = true;
		
		OutEdgeIterator e_it, e_beg, e_end;
		boost::tie(e_beg,e_end) = boost::out_edges(*v_it,*this);
		
		if( e_beg == e_end ) // v is not connected to any other vertex
		{
			Unitig utg;
			OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, *v_it );
			this->build_unitig( fasta, v_info, utg );
			unitigs.push_back(utg);
			
			continue;
		}
		
		for( e_it = e_beg; e_it != e_end; ++e_it ) // for each incident edge v -- w
		{
			Vertex w = boost::target(*e_it, *this);
			
			if( visited.at(m_index[w]) ) continue;
			
			OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, *v_it );
			OLPG_VertexInfo w_info = boost::get( boost::vertex_kind_t(), *this, w );
			OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it );
			
			bool w_in_type; Unitig utg;
			
			if( v_info < w_info )
			{
				w_in_type = e_info.type_1;
				this->build_unitig( fasta, e_info.pos_1, utg );
			}
			else
			{
				w_in_type = e_info.type_2;
				this->build_unitig( fasta, e_info.pos_2, utg );
			}
			
			unitigs.push_back(utg);
			this->build_unitigs_dfs( w, w_in_type, visited, fasta, unitigs );
		}
	}
}


void OverlapGraph::build_unitigs_dfs( const Vertex &s, bool s_in_type, std::vector<bool> &visited, 
									  const SequenceCollection& fasta, std::list<Unitig> &unitigs )
{
	if( visited.at(m_index[s]) ) return;
	visited.at(m_index[s]) = true;
	
	OutEdgeIterator e_it, e_end;
	for( boost::tie(e_it,e_end) = boost::out_edges(s,*this); e_it != e_end; ++e_it )
	{
		Vertex t = boost::target(*e_it, *this);
		
		OLPG_VertexInfo s_info = boost::get( boost::vertex_kind_t(), *this, s );
		OLPG_VertexInfo t_info = boost::get( boost::vertex_kind_t(), *this, t );
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e_it);
		
		bool s_out_type = s_info < t_info ? e_info.type_2 : e_info.type_1;
		if( s_in_type == s_out_type ) continue;
		
		Unitig utg;
		this->build_unitig( fasta, (s_info < t_info ? e_info.pos_1 : e_info.pos_2), utg );
		unitigs.push_back(utg);
		
		bool t_in_type = s_info < t_info ? e_info.type_1 : e_info.type_2;
		this->build_unitigs_dfs( t, t_in_type, visited, fasta, unitigs );
	}
}


void OverlapGraph::build_unitig( const SequenceCollection &fasta, const int32_t id, Unitig &utg )
{
	Sequence& seq = utg.seq;
	const Sequence& ref_seq = fasta[id];
	
	seq.resize( ref_seq.length() );
	for( size_t i=0; i < ref_seq.length(); ++i ) seq[i] = ref_seq[i];
	
	utg.add_descriptor( ref_seq.str_id(), 0, int32_t(seq.size())-1 ); //utg.add_descriptor( id, 0, int32_t(seq.size())-1 );
}


void OverlapGraph::build_unitig( const SequenceCollection &fasta, const std::list<ExtInfo> &ext_list, Unitig &utg )
{
	Sequence& seq = utg.seq;
	
	//int32_t prev_tail,prev_start,prev_end,prev_len;
	
	for( std::list<ExtInfo>::const_iterator ext = ext_list.begin(); ext != ext_list.end(); ++ext )
	{
		if( ext == ext_list.begin() )
		{
			const Sequence& ref_seq = fasta.at( ext->gid );
			Sequence ext_seq = std::move( ref_seq.subseq( ext->start, ext->end ) );
			
			seq.resize( ext_seq.size() );
			for( size_t i=0; i < ext_seq.size(); i++ ) seq.at(i) = ext_seq.at(i);
			
			utg.add_descriptor( ref_seq.str_id(), ext->start, ext->end ); //utg.add_descriptor( ext->gid, ext->start, ext->end );
			
			//prev_start = ext->start;
			//prev_end = ext->end;
			//prev_len = ref_seq.length();
		}
		else
		{
			size_t seq_size = seq.size();
			
			const Sequence& ref_seq = fasta.at( ext->gid );
			Sequence ext_seq = std::move( ref_seq.subseq( ext->start, ext->end ) );
			
			seq.resize( seq_size + ext_seq.size() - ext->prev_tail );
			for( size_t i=0; i < ext_seq.size(); i++ ) seq.at( i + seq_size - ext->prev_tail ) = ext_seq.at(i);
			
			utg.add_descriptor( ref_seq.str_id(), ext->start, ext->end ); //utg.add_descriptor( ext->gid, ext->start, ext->end );
			
			//prev_start = ext->start;
			//prev_end = ext->end;
			//prev_len = ref_seq.length();
		}
	}
	
	/// DEBUG
	if( utg.length() == 0 )
	{
		std::cerr << "Unitig of null size (ext_list size = " << ext_list.size() << "). Descriptors list = ";
		for( std::list< Unitig::CtgDesc >::const_iterator d = utg.desc.begin(); d != utg.desc.end(); ++d )
		{
			if( d != utg.desc.begin() ) std::cerr << ", ";
			std::cerr << "(" << d->str_id << ";s=" << d->start << ";e=" << d->end << ")";
		}
		
		std::cerr << std::endl;
	}
}


void OverlapGraph::write_graphviz( const std::string &filename, const SequenceCollection& fasta )
{
	std::ofstream os( filename.c_str() );
	
	os << "graph OverlapGraph {" << std::endl;
	//os << "   rankdir=LR;" << std::endl;
	
	// print vertices
	VertexIterator vbegin,vend;
	boost::tie(vbegin,vend) = boost::vertices(*this);
	
	for( VertexIterator v=vbegin; v!=vend; ++v )
	{
		using namespace ham::colors;

		OLPG_VertexInfo v_info = boost::get( boost::vertex_kind_t(), *this, *v );

		int32_t v_pid { fasta.at(v_info).pid() };
		std::string vcolor = v_pid < svg_colors ? svg_scheme[v_pid] : std::string("white");

		os << "\t" << m_index[*v] << "[label=\"" << fasta.at(v_info).pid() << "\"" 
			<< ",fillcolor=\"" << vcolor << "\",style=\"filled,solid\""
			//(this->is_junction_vertex(*v) ? ",color=\"turquoise\",style=\"filled\"" : "") 
			<< "];" << std::endl;
	}
	
	// print edges
	EdgeIterator ebegin,eend;
	boost::tie(ebegin,eend) = boost::edges(*this);
	
	for( EdgeIterator e = ebegin; e != eend; e++ )
	{
		Vertex s = boost::source(*e,*this); OLPG_VertexInfo s_info = boost::get( boost::vertex_kind_t(), *this, s );
		Vertex t = boost::target(*e,*this); OLPG_VertexInfo t_info = boost::get( boost::vertex_kind_t(), *this, t );
		OLPGEdgeInfo e_info = boost::get( boost::edge_kind_t(), *this, *e);
		
		if( e_info.etype == OLPG_ETYPE_DASHED )
		{
			os << "\t" << m_index[s] << "--" << m_index[t] << "[color=\"black\",style=\"dashed\"];" << std::endl;
			continue;
		}
		//if( e_info.etype == OLPG_ETYPE_DASHED ) continue;
		
		bool s_type = s_info < t_info ? e_info.type_2 : e_info.type_1;
		bool t_type = t_info < s_info ? e_info.type_2 : e_info.type_1;
		
		int32_t s_len = 0, t_len = 0;
		const std::list<ExtInfo> &s_pos = s_info < t_info ? e_info.pos_1 : e_info.pos_2;
		const std::list<ExtInfo> &t_pos = s_info < t_info ? e_info.pos_2 : e_info.pos_1;
		
		for( std::list<ExtInfo>::const_iterator it=s_pos.begin(); it!=s_pos.end(); ++it )
			s_len += abs(it->start - it->end);
		
		for( std::list<ExtInfo>::const_iterator it=t_pos.begin(); it!=t_pos.end(); ++it )
			t_len += abs(it->start - it->end);
		
		std::stringstream ss;
		//ss << _index[s] << " (" << (s_info < t_info ? e_info.start_1 : e_info.start_2) << "," << (s_info < t_info ? e_info.end_1 : e_info.end_2) << ")\n";
		//ss << _index[t] << " (" << (t_info < s_info ? e_info.start_1 : e_info.start_2) << "," << (t_info < s_info ? e_info.end_1 : e_info.end_2) << ")";
		
		//os << "\t" << _index[s] << "--" << _index[t] << "[color=black,label=\"" << ss.str() << "\"];" << std::endl;
		os << "\t" << m_index[s] << "--" << m_index[t] << "[color=\"black\",dir=\"both\"," <<
			"taillabel=\"" /*<< t_len*/ << "\", headlabel=\"" /*<< s_len*/ << "\"," <<
			"arrowtail=\"" << (s_type == OLPG_BDETYPE_IN ? "empty" : "invempty") << "\"," <<
			"arrowhead=\"" << (t_type == OLPG_BDETYPE_IN ? "empty" : "invempty") << "\"];" << std::endl;
	}
	
	os << "}" << std::endl;
	
	os.close();
}

