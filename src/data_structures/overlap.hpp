#ifndef OVERLAP_HPP
#define OVERLAP_HPP

#include <cstdlib>
#include <cstdint>

struct Overlap
{
	int32_t qid;
	int32_t rid;

	int32_t q_beg;
	int32_t q_end;
	int32_t r_beg;
	int32_t r_end;

	size_t  hits_num;
	bool    is_rev;
	int     sw_score;
	double  ident;

	bool operator<( const Overlap &o ) const
	{
		return qid < o.qid || (qid == o.qid && rid < o.rid );
	}
};

#endif //OVERLAP_HPP
