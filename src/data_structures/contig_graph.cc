#include "contig_graph.hpp"

#include <iostream>
#include <fstream>


//ContigGraph::ContigGraph() {}


ContigGraph::ContigGraph( std::list<Overlap>& overlaps )
{
    this->init_graph( overlaps );
	
	// obtain a property map for the vertex_index property
	this->_index = boost::get( boost::vertex_index, *this );
	
	// initialize the vertex_index property values
	size_t cnt = 0;
	ContigGraph::VertexIterator vi, vend;

	for( boost::tie(vi,vend) = boost::vertices(*this); vi != vend; ++vi) 
		boost::put(_index, *vi, cnt++);
}


ContigGraph::~ContigGraph()
{
	/*EdgeIterator ebegin,eend;
	boost::tie(ebegin,eend) = boost::edges(*this);
	
	for( EdgeIterator e = ebegin; e != eend; e++ )
	{
		CGEdgeInfo *edge_info = boost::get( boost::edge_kind_t(), *this, *e );
		delete edge_info;
	}*/
}


void ContigGraph::init_graph( std::list<Overlap>& overlaps )
{
	this->init_vertices(overlaps);
	
	Edge e;
	bool exists;

	for( auto& olp : overlaps )
	{
		int32_t id1 = olp.rid; Vertex s = _id2vertex[id1];
		int32_t id2 = olp.qid; Vertex t = _id2vertex[id2];

		boost::tie(e,exists) = boost::edge(s,t,*this);

		if( not exists )
		{
			//CGEdgeInfo *edge_info = new CGEdgeInfo;
			//edge_info->_blocks.push_back(b);
			e = boost::add_edge(s,t,*this).first;
			boost::put( boost::edge_kind_t(), *this, e, olp );
		}
		/*else
		{
			CGEdgeInfo *edge_info = boost::get( boost::edge_kind_t(), *this, e );
			(edge_info->_blocks).push_back(b);
		}*/
	}
}


void ContigGraph::init_vertices( std::list<Overlap>& overlaps )
{
	std::set< int32_t > ids; // set of contig identifiers
	
	// retrieve all the contig identifiers from the blocks
	for( auto& o : overlaps )
	{
		ids.emplace( o.rid );
		ids.emplace( o.qid );
	}

	// create vertices and add their property
	for( auto it = ids.begin(); it != ids.end(); ++it )
	{
		Vertex v = boost::add_vertex(*this);
		boost::put( boost::vertex_kind_t(), *this, v, *it );
		_id2vertex[*it] = v;
	}
}


ContigGraph::Vertex ContigGraph::get_vertex( int32_t id )
{
	//CGVertexInfo id(pid,cid);
	return _id2vertex.find(id)->second;
}


bool ContigGraph::check_incl_vertex( Vertex v, Vertex u )
{
	bool linked = true;
	
	// check consistency
	AdjacencyIterator ai, ae;
	boost::tie(ai,ae) = adjacent_vertices(v,*this);
	for( AdjacencyIterator a = ai; a != ae; ++a )
	{
		if( *a != u ) linked = linked && boost::edge(*a,u,*this).second;
	}
	
	return linked;
}


void ContigGraph::remove_incl_vertex( Vertex v )
{
	/*OutEdgeIterator ei, ee;
	for( boost::tie(ei,ee) = boost::out_edges(v,*this); ei != ee; ++ei )
	{
		CGEdgeInfo *einfo = boost::get( boost::edge_kind_t(), *this, *ei );
		
		std::list<Block*>::iterator it;
		for( it = einfo->_blocks.begin(); it != einfo->_blocks.end(); it = einfo->_blocks.erase(it) ) delete *it;
		
		delete einfo;
	}*/
	
	boost::clear_vertex(v,*this);
	boost::remove_vertex(v,*this);
}


void ContigGraph::remove_edge( Edge e )
{
	/*CGEdgeInfo *einfo = boost::get( boost::edge_kind_t(), *this, e);
	
	std::list<Block*>::iterator it;
	for( it = einfo->_blocks.begin(); it != einfo->_blocks.end(); it = einfo->_blocks.erase(it) ) delete *it;
	
	delete einfo;*/
	
	boost::remove_edge(e,*this);
}


void ContigGraph::write_graphviz( const std::string &filename, const SequenceCollection &sc )
{
	std::ofstream os( filename.c_str() );
	
	os << "graph ContigGraph {" << std::endl;
	//os << "   rankdir=LR;" << std::endl;
	
	// print vertices
	VertexIterator vbegin,vend;
	boost::tie(vbegin,vend) = boost::vertices(*this);
	
	for( VertexIterator v=vbegin; v!=vend; ++v )
	{
		int32_t gid = boost::get( boost::vertex_kind_t(), *this, *v );
		os << "\t" << _index[*v] << "[label=\"<" << sc[gid].pid() << "," << sc[gid].sid() << ">\"];" << std::endl;
	}
	
	// print edges
	Overlap olp;
	EdgeIterator ebegin,eend;
	boost::tie(ebegin,eend) = boost::edges(*this);
	
	for( EdgeIterator e = ebegin; e != eend; e++ )
	{
		Vertex s = boost::source(*e,*this);
		Vertex t = boost::target(*e,*this);
		
		olp = boost::get( boost::edge_kind_t(), *this, *e );
		os << "\t" << _index[s] << "--" << _index[t] << "[color=black,label=\"" << (olp.is_rev ? '-' : '+') << "\"];" << std::endl;
	}
	
	os << "}" << std::endl;
	
	os.close();
}

