#include "range.hpp"

#include <iostream>

std::istream& operator>>( std::istream& is, ham::range_t& range )
{
	is >> range.left >> range.right;
	return is;
}
