#include "sequence_collection.hpp"

#include <assert.h>
#include <limits>
#include <fstream>
#include <sstream>

#include "io/fasta_reader.hpp"


SequenceCollection::SequenceCollection() { }


SequenceCollection::SequenceCollection( const std::string& pool, int32_t min_len )
{
	std::vector<std::string> pools(1,pool);
	this->load_pools(pools, min_len);
}


SequenceCollection::SequenceCollection( const std::vector<std::string>& pools, int32_t min_len )
{
	this->load_pools(pools, min_len);
}


SequenceCollection::~SequenceCollection() { }


size_t
SequenceCollection::pools() const
{
	return m_pid2i.size();
}


size_t
SequenceCollection::size() const
{
	return m_sequences.size();
}


size_t
SequenceCollection::sequences() const
{
	return this->size();
}


const Sequence&
SequenceCollection::at( size_t i  ) const
{
	return m_sequences.at(i);
}


const Sequence&
SequenceCollection::operator[]( size_t i ) const
{
	return m_sequences[i];
}


const Sequence&
SequenceCollection::at( size_t pid, size_t sid ) const
{
	assert( pid < m_pid2i.size() &&
			m_pid2i.at(pid) + sid < m_sequences.size() );

	return m_sequences[ m_pid2i[pid] + sid ];
}


Sequence&
SequenceCollection::at( size_t pid, size_t sid )
{
	assert( pid < m_pid2i.size() &&
			m_pid2i.at(pid) + sid < m_sequences.size() );

	return m_sequences[ m_pid2i[pid] + sid ];
}

size_t
SequenceCollection::gid( size_t pid, size_t sid ) const
{
	assert( pid < m_pid2i.size() &&
			m_pid2i.at(pid) + sid < m_sequences.size() );

	return m_pid2i[pid] + sid;
}


size_t
SequenceCollection::pool_size( size_t pid )
{
	assert( pid < m_pid2i.size() );

	if( pid+1 == m_pid2i.size() ) return m_sequences.size() - m_pid2i[pid];

	return m_pid2i[pid+1] - m_pid2i[pid];
}


size_t
SequenceCollection::pool_idx( size_t pid )
{
	assert( pid < m_pid2i.size() );

	return m_pid2i[pid];
}


void
SequenceCollection::clear()
{
	m_sequences.resize(0);
	m_pid2i.resize(0);
}


void
SequenceCollection::load_pools( const std::vector<std::string>& filenames, int32_t min_len )
{
	this->clear();

	for( size_t pid=0; pid < filenames.size(); ++pid )
	{
		m_pid2i.push_back( m_sequences.size() );
		this->load_pool( filenames[pid], static_cast<int32_t>(pid), min_len );
	}
}


void
SequenceCollection::load_pool( const std::string& filename, int32_t pid, int32_t min_len )
{
	Sequence seq;
    int32_t sid = 0;

	FastaReader reader(filename);
	while( reader.next_sequence(seq) )
	{
		if( seq.length() < min_len ) continue;

		std::stringstream newid_ss;
		newid_ss << pid << "." << sid;

		seq.set_pid(pid);
		seq.set_sid(sid++);

		seq.set_str_id( newid_ss.str() );

		m_sequences.push_back(seq);
	}
}


void
SequenceCollection::write_to_file( const std::string &filename ) const
{
	std::ofstream ofs;
	ofs.open( filename.c_str() );

	size_t sc_size = this->sequences();

	for( size_t i=0; i < sc_size; ++i ) ofs << m_sequences[i] << "\n";

	ofs.close();
}
