#ifndef SEQUENCE_INDEX_HPP
#define SEQUENCE_INDEX_HPP

#include "3d_party/bwa/bwa.h"

#include "common/types.hpp"
#include "kmer.hpp"
#include "sequence_collection.hpp"


namespace ham
{

	namespace index
	{

		class sequence_index
		{

		private:

			bwt_t *    bwt() const { return m_idx->bwt; }
			bntseq_t * bns() const { return m_idx->bns; }
			uint8_t *  pac() const { return m_idx->pac; }

			const SequenceCollection & sc() const { return m_sc; }

		public:

			sequence_index( const SequenceCollection & );
			sequence_index( const SequenceCollection &, const std::string & );

			~sequence_index();

			// build an FM-index for a sequence collection and write it on disk
			void build( const std::string & );

			// load an FM-index from file
			void load( const std::string & );

		private:

			const SequenceCollection & m_sc;
			bwaidx_t* m_idx {nullptr};

		public:

			friend 
			std::vector<KmerHit> 
			exact_kmer_align( const sequence_index &index, const Kmer & kmer, int32_t pid = -1 );
		};


	} // namespace ham::index

} // namespace ham

// auxiliary functions for bwa's index construction

int64_t bwa_seq_len(const char *fn_pac);
bwt_t * bwt_pac2bwt(const char *fn_pac, int use_is);


#endif // SEQUENCE_INDEX_HPP
