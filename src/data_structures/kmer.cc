#include "kmer.hpp"

#include <iostream>
#include <algorithm>
#include <assert.h>

#include "common/limits.hpp"
#include "common/constants.hpp"
#include "common/utils.hpp"
#include "3d_party/city/city.h"


uint32_t Kmer::s_length; // length of all k-mers.
uint64_t Kmer::s_mask;   // mask to be applied so that equal k-mers have the same m_data value


void Kmer::set_length( uint32_t k )
{
	assert( k>= ham::limits::MIN_KMER && k <= ham::limits::MAX_KMER );
	s_length = k;

	Kmer::set_mask(k);
}


uint32_t Kmer::length()
{
	return s_length;
}


void Kmer::set_mask( uint32_t k )
{
	assert( k >= ham::limits::MIN_KMER && k <= ham::limits::MAX_KMER );
	s_mask = ~(~(static_cast<uint64_t>(0)) << (k<<1));
}


uint64_t Kmer::mask()
{
	return s_mask;
}


Kmer Kmer::reverse_complement( const Kmer& k )
{
	Kmer rc(k);
	rc.reverse_complement();
	return rc;
}


void Kmer::reverse_complement()
{
	// reverse
	m_data = ((m_data >> 2)  & 0x3333333333333333UL) | ((m_data & 0x3333333333333333UL) << 2);
	m_data = ((m_data >> 4)  & 0x0F0F0F0F0F0F0F0FUL) | ((m_data & 0x0F0F0F0F0F0F0F0FUL) << 4);
	m_data = ((m_data >> 8)  & 0x00FF00FF00FF00FFUL) | ((m_data & 0x00FF00FF00FF00FFUL) << 8);
	m_data = ((m_data >> 16) & 0x0000FFFF0000FFFFUL) | ((m_data & 0x0000FFFF0000FFFFUL) << 16);
	m_data = ( m_data >> 32                        ) | ( m_data                         << 32);

	// complement
	m_data = ~m_data;

	// shift-right according k-mer length
	m_data = m_data >> ((8*sizeof(m_data))-(s_length<<1));
}


void Kmer::set_data( uint64_t data )
{
	m_data = data;
}


uint64_t Kmer::data() const
{
	return m_data;
}


size_t Kmer::hash() const
{
	Kmer rc = Kmer::reverse_complement(*this);

	return m_data < rc.m_data ?
		   city_hash_64( &m_data, sizeof(m_data) ) :
		   city_hash_64( &rc.m_data, sizeof(rc.m_data) );
}


Kmer& Kmer::shift_append( base_t b )
{
	m_data = ((m_data << 2) & s_mask) | static_cast<uint64_t>(b);
	return *this;
}


Kmer& Kmer::shift_prepend( base_t b )
{
	assert( s_length > 0 );
	m_data = (m_data >> 2) | (static_cast<uint64_t>(b) << 2*(s_length-1)); 
	return *this;
}


void Kmer::to_string( std::string &str ) const
{
	uint32_t k = Kmer::s_length;
	uint64_t base_mask = static_cast<uint64_t>(3);
	uint64_t kmer_code = m_data;

    str.resize(k);

	for( size_t i=1; i <= k; ++i )
	{
		str[k-i] = ham::constants::base2char[ kmer_code&base_mask ];
		kmer_code >>= 2;
	}
}


std::string Kmer::to_string() const
{
	std::string str;

	uint32_t k = Kmer::s_length;
	uint64_t base_mask = static_cast<uint64_t>(3);
	uint64_t kmer_code = m_data;

    str.resize(k);

	for( size_t i=1; i <= k; ++i )
	{
		str[k-i] = ham::constants::base2char[ kmer_code&base_mask ];
		kmer_code >>= 2;
	}

	return str;
}


bool Kmer::operator==( const Kmer& o )
{
	return m_data == o.m_data;
}


std::ostream& operator<<( std::ostream& os, const Kmer& kmer )
{
	os << kmer.to_string();
	return os;
}
