#ifndef KMER_COLLECTION_HPP
#define KMER_COLLECTION_HPP

#include <iosfwd>
#include <string>
#include <vector>
#include <limits>
#include <google/sparse_hash_map>

#include "data_structures/kmer.hpp"


class KmerCollection
{

public:

    typedef google::sparse_hash_map< Kmer, int32_t, Kmer::kmerHash, Kmer::kmerEq > KmerFreqMap;

    typedef KmerFreqMap::const_iterator const_iterator;
    typedef KmerFreqMap::iterator iterator;

private:

	KmerFreqMap m_map;

public:

	KmerCollection();

    size_t size() const;

    void add_kmer( const Kmer& k );

    void clear();

    int32_t operator[]( const Kmer& k ) const;

    int32_t get_freq( const Kmer& k ) const;

    // store/load kmer collection to/from a file
    void store( const std::string& filename );
    void load( const std::string& filename );

    void write_hist_to_file( const std::string& filename ) const;

    void write_hist_to_file( const char* filename ) const;

}; // class KmerCollection


#endif // KMER_COLLECTION_HPP
