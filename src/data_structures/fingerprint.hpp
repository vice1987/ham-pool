/*
 * File:   fingerprint.hpp
 * Author: riccardo
 *
 * Created on November 27, 2013, 5:50 PM
 */

#ifndef FINGERPRINT_HPP
#define	FINGERPRINT_HPP

#include "data_structures/kmer.hpp"

#include <vector>


class Fingerprint
{

private:

    std::vector<Kmer> m_kmers;
    std::vector<size_t> m_gaps;

public:

    Fingerprint();

    // add k-mer
	void push_back( const Kmer& kmer );
	// add gap
	void push_back( const size_t gap );
	// add both k-mer and gap
    void push_back( const Kmer& kmer, size_t gap );

    void clear();

    size_t kmers() const;
    size_t gaps() const;

    const Kmer& kmer_at( const size_t& i ) const;
    Kmer& kmer_at( const size_t& i );

    const size_t& gap_at( const size_t& i ) const;
    size_t& gap_at( const size_t& i );
};


#endif	/* FINGERPRINT_HPP */

