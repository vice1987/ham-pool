#ifndef CONTIG_GRAPH_HPP
#define	CONTIG_GRAPH_HPP

#include <set>
#include <list>

#include "common/boost_includes.hpp"
#include "data_structures/overlap.hpp"
#include "data_structures/sequence_collection.hpp"

/*struct CGVertexInfo
{
	int32_t pid;	// pool id
	int32_t cid;	// contig id
	
	CGVertexInfo() : pid(0), cid(-1) {}
	CGVertexInfo( int32_t _pid, int32_t _cid ) : pid(_pid), cid(_cid) {}
	
	friend bool operator<( const CGVertexInfo &v, const CGVertexInfo &u )
	{
		return (v.pid < u.pid) || (v.pid == u.pid && v.cid < u.cid);
	}
};


struct CGEdgeInfo
{
	std::list< Overlap* > _blocks;
};*/


class ContigGraph : public boost::adjacency_list< boost::listS, boost::listS, boost::undirectedS, // graph type
	boost::property< boost::vertex_kind_t, int32_t, boost::property<boost::vertex_index_t, size_t> >, // vertex property
	boost::property<boost::edge_kind_t,Overlap> >	// edge property
{
	
public:
	
	typedef boost::adjacency_list< boost::listS, boost::listS, boost::undirectedS, 
		boost::property<boost::vertex_kind_t,int32_t, boost::property<boost::vertex_index_t, size_t> >,
		boost::property<boost::edge_kind_t,Overlap> > Graph;
		
	typedef boost::property_map<Graph,boost::vertex_index_t>::type IndexMap;
	typedef boost::graph_traits<ContigGraph>::vertex_descriptor Vertex;
	typedef boost::graph_traits<ContigGraph>::vertex_iterator VertexIterator;
	typedef boost::graph_traits<ContigGraph>::edge_descriptor Edge;
	typedef boost::graph_traits<ContigGraph>::edge_iterator EdgeIterator;
	typedef boost::graph_traits<ContigGraph>::out_edge_iterator OutEdgeIterator;
	typedef boost::graph_traits<ContigGraph>::adjacency_iterator AdjacencyIterator;
	
private:
	
	IndexMap _index;
	std::map< int32_t, Vertex > _id2vertex;
	
	void init_graph( std::list<Overlap>& overlaps );
	void init_vertices( std::list<Overlap>& overlaps );
	
public:
	
	//ContigGraph();
	ContigGraph( std::list<Overlap>& overlaps );
	
	virtual ~ContigGraph();
	
	Vertex get_vertex( int32_t id );
	
	bool check_incl_vertex( Vertex v, Vertex u );
	void remove_incl_vertex( Vertex v );
	void remove_edge( Edge e );
	
	void write_graphviz( const std::string &filename, const SequenceCollection &sc );
};


void init_graph_index(ContigGraph &cg);


#endif	/* CONTIG_GRAPH_HPP */

