/* 
 * File:   Unitig.hpp
 * Author: riccardo
 *
 * Created on February 21, 2013, 5:26 PM
 */

#ifndef UNITIG_HPP
#define	UNITIG_HPP

#include <iostream>
#include <vector>
#include <string>
#include <list>

#include "data_structures/sequence.hpp"

class Unitig
{
	
public:
	
	struct CtgDesc
	{
		std::string str_id;
		//int32_t gid;
		//uint32_t pid; 
		//int32_t  cid;
		int32_t  start; 
		int32_t  end;
	};
	
	std::string id; // ID
	Sequence seq; // Unitig Sequence
	std::list<CtgDesc> desc; // Contigs used to build the Unitig
	
	Unitig();
	Unitig( const Unitig &utg );
	
	inline size_t length() const { return seq.size(); }
	inline size_t size() const { return seq.size(); }
	
	inline base_t& at( const size_t& i ){ return seq.at(i); }
	inline base_t& operator[]( const size_t& i ){ return seq[i]; }
	inline const base_t& at( const size_t& i ) const { return seq.at(i); }
	inline const base_t& operator[]( const size_t& i ) const { return seq[i]; }
	
	void add_descriptor( const std::string& str_id, int32_t start, int32_t end );
	std::ostream& print_descriptors( std::ostream& os ) const;
	
	friend std::ostream& operator<<( std::ostream& os, const Unitig& utg );
	
}; // end class Unitig

#endif	/* UNITIG_HPP */

