#ifndef BLOOM_FILTER_HPP
#define BLOOM_FILTER_HPP

#include <cmath>
#include <boost/dynamic_bitset.hpp>

#include "3d_party/murmur/murmur3.hpp"

class BloomFilter 
{

private:
	
	boost::dynamic_bitset<> _bf;
	
	uint64_t _n;   // estimated number of elements
	uint64_t _m;   // size of the filter
	double _p;     // probability of false positives
	int _k;        // number of hash values to be computed for each key
	
	inline uint64_t get_index( const void *key, uint key_len, uint h_i )
	{
		uint64_t buf[2];
		MurmurHash3_x64_128( key, key_len, h_i, &buf );
		
		return buf[0] % _bf.size();
	}
	
public:
	
	BloomFilter( uint64_t n, double p )
	{
		_n = n;
		_p = p;
		_m = std::ceil( (n * std::fabs(std::log(p))) / (std::log(2) * std::log(2)) );
		_k = std::ceil( (_m * std::log(2)) / n );
		
		_bf.resize(_m);
	}
	
	inline uint64_t n(){ return _n; }
	inline uint64_t m(){ return _m; }
	inline double p(){ return _p; }
	inline int k(){ return _k; }
	
	inline bool contains( const void* key, uint32_t key_len )
	{
		if( key_len == 0 ) 
			return false;
		
		for( uint h_i = 0; h_i < _k; ++h_i ) 
			if( not _bf.test( this->get_index(key,key_len,h_i) ) ) 
				return false;
		
		return true;
	}
	
	inline bool insert( const void* key, uint32_t key_len )
	{
		if( key_len == 0 ) return false;
		for( uint h_i = 0; h_i < _k; ++h_i ) _bf.set( this->get_index(key,key_len,h_i) );
		
		return true;
	}
	
	inline void reset() { _bf.reset(); }
};


#endif // BLOOM_FILTER_HPP
