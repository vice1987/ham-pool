#include "kmer_collection.hpp"

#include <iostream>
#include <fstream>

KmerCollection::KmerCollection() { }


size_t KmerCollection::size() const
{
	return m_map.size();
}


void KmerCollection::add_kmer( const Kmer& k )
{
	assert( m_map[k] < std::numeric_limits<int32_t>::max() );
	m_map[k] += 1;
}


void KmerCollection::clear()
{
	m_map.clear();
}


int32_t KmerCollection::operator[]( const Kmer& k ) const
{
	return this->get_freq(k);
}


int32_t KmerCollection::get_freq( const Kmer& k ) const
{
    const_iterator p = m_map.find(k);
    return p != m_map.end() ? p->second : 0;
}


void KmerCollection::store( const std::string& filename )
{
    FILE *fp = fopen(filename.c_str(), "w");

    if( fp == NULL )
    {
        std::cerr << "Couldn't save KmerCollection to file:" << filename << std::endl;
        exit(1);
    }
    else
    {
        m_map.write_metadata(fp);
        m_map.write_nopointer_data(fp);
        fclose(fp);
    }
}


void KmerCollection::load( const std::string& filename )
{
    this->clear();

    FILE *fp = fopen(filename.c_str(), "r");

    if( fp == NULL )
    {
        std::cerr << "Couldn't open KmerCollection file:" << filename << std::endl;
        exit(1);
    }
    else
    {
        m_map.read_metadata(fp);
        m_map.read_nopointer_data(fp);
        fclose(fp);
    }
}


void  KmerCollection::write_hist_to_file( const std::string& filename ) const
{
	this->write_hist_to_file(filename.c_str());
}


void KmerCollection::write_hist_to_file( const char* filename ) const
{
    std::vector<uint32_t> v_freq;
    size_t step = 1;

    for( KmerCollection::const_iterator p = m_map.begin(); p != m_map.end(); ++p )
    {
		assert( p->second >= 0 );
        int32_t freq = p->second;
		size_t bin = size_t(freq)/step;

        if( bin >= v_freq.size() ) v_freq.resize( bin+1 );

        v_freq[bin]++;
    }

    std::ofstream ofs(filename);

    for( size_t i=0; i < v_freq.size(); ++i )
        if( v_freq[i] > 0 ) ofs << i << "\t" << v_freq[i] << "\n";

    ofs.close();
}
