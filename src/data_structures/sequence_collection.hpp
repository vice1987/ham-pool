/*
 * File:   sequence_collection.hpp
 * Author: riccardo
 *
 * Created on November 21, 2013, 5:15 PM
 */

#ifndef SEQUENCE_COLLECTION_HPP
#define	SEQUENCE_COLLECTION_HPP

#include "data_structures/sequence.hpp"


class SequenceCollection
{

public:

    typedef std::vector<Sequence> SequencePool;
	typedef std::vector<size_t> PoolIndex;

public:

    SequenceCollection();

    SequenceCollection( const std::string& pool, int32_t min_len=0 );

    SequenceCollection( const std::vector<std::string>& pools, int32_t min_len=0 );

    ~SequenceCollection();

    // return number of pools
	size_t pools() const;

	// return number of all sequences
    size_t size() const;
	size_t sequences() const;

	// return sequence reference with global index
    const Sequence& at( size_t i ) const;
    const Sequence& operator[]( size_t i ) const;

	// return sequence reference with pool and sequence id
	const Sequence& at( size_t pid, size_t sid ) const;
	Sequence& at( size_t pid, size_t sid );

	// given pool and sequence IDs, return global identifier.
	size_t gid( size_t pid, size_t sid ) const;

	// return number of sequences in a pool
	size_t pool_size( size_t pid );

	// return index in the collection of the first sequence in a pool
	size_t pool_idx( size_t pid );

	void clear();

    void load_pools( const std::vector<std::string> &filenames, int32_t min_len=0 );

	void write_to_file( const std::string &filename ) const;

private:

	void load_pool( const std::string &filename, int32_t pid, int32_t min_len=0 );

private:

	PoolIndex m_pid2i;
	SequencePool m_sequences;

}; // class SequenceCollection


#endif	/* SEQUENCE_COLLECTION_HPP */

