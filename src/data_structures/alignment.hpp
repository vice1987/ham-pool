/*
 * File:   alignment.hpp
 * Author: riccardo
 *
 * Created on January 9, 2014, 3:40 PM
 */

#ifndef ALIGNMENT_HPP
#define	ALIGNMENT_HPP

#include <stdint.h>
#include <vector>

class Alignment
{

public:

	int32_t  ref_pid;     // reference pool id
	int32_t  ref_sid;     // reference sequence id
	uint32_t ref_begin;   // alignment begin on the reference
	uint32_t ref_end;     // alignment end on the reference

	int32_t  query_pid;   // query pool id
	int32_t  query_sid;   // query sequence id
	uint32_t query_begin; // alignment begin on the query
	uint32_t query_end;   // alignment end on the query

	int score;            // best alignment score
	int score_2;          // second best alignment score

	uint32_t ref_end_2;   // alignment end on the reference of the second best alignment

	// BAM-encoded CIGAR vector:
	// length = 28 most-significant bits
	// M/I/D/S/X (0/1/2/4/8) = 4 least-significant bits
	std::vector<uint32_t> cigar;

public:

    Alignment();

};

#endif	/* ALIGNMENT_HPP */

