#include "unitig.hpp"

#include "common/constants.hpp"

Unitig::Unitig() : id("") {}


Unitig::Unitig( const Unitig &utg ) : id(utg.id), seq(utg.seq), desc(utg.desc) {}


void Unitig::add_descriptor( const std::string &str_id, int32_t start, int32_t end )
{
	CtgDesc d = {str_id,start,end}; //d.pid = pid; d.cid = cid; d.start = start; d.end = end;
	desc.push_back(d);
}


std::ostream& Unitig::print_descriptors( std::ostream& os ) const
{
	for( std::list<CtgDesc>::const_iterator d = desc.begin(); d != desc.end(); ++d )
		os << id << '\t' << d->str_id << '\t' << d->start << '\t' << d->end << std::endl;
	
	return os;
}


std::ostream& operator<<( std::ostream &os, const Unitig &utg )
{
	os << '>' << utg.id << std::endl;
	
	for( size_t i = 0; i < utg.seq.size(); i++ )
		os << ham::constants::base2char[utg[i]];
	
	return os;
}
