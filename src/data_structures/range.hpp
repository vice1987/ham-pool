#ifndef RANGE_HPP
#define RANGE_HPP

#include <iosfwd>
#include <cassert>

namespace ham
{
	struct range_t
	{

	public:

		size_t left {0};
		size_t right {0};

	public:

		range_t() = default;
		range_t( const range_t& ) = default;

		range_t( size_t l, size_t r ) : 
			left{l}, 
			right{r} 
		{ }

		bool valid() 
		{ 
			return left <= right; 
		}

		// precondition: right >= left
		size_t length() 
		{ 
			return right - left;
		}

		size_t ltail()
		{
			return left;
		}

		size_t rtail( size_t len )
		{
			assert( len >= right );
			return len-right;
		}

		bool is_containment( size_t len, size_t max_tail )
		{
			return ( this->ltail() <= max_tail and this->rtail(len) <= max_tail );
		}
	};
}

std::istream& operator>>( std::istream& is, ham::range_t& range );

#endif // RANGE_HPP
