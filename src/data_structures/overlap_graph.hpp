#ifndef OVERLAP_GRAPH_HPP
#define	OVERLAP_GRAPH_HPP

#include <map>
#include <list>

#include "common/boost_includes.hpp"

#include "data_structures/overlap.hpp"
#include "data_structures/sequence_collection.hpp"
#include "data_structures/unitig.hpp"


// edge types
#define OLPG_ETYPE_SOLID 	0	// good (consistent) overlap edge
#define OLPG_ETYPE_DASHED 	1	// bad (inconsistent) overlap edge
#define OLPG_ETYPE_REMOVED 	2	// removed edge

// bi-directional-edge types
#define OLPG_BDETYPE_IN		true
#define OLPG_BDETYPE_OUT	false

// graph reduction vertices types
#define OLPG_VTYPE_VACANT 		0
#define OLPG_VTYPE_INPLAY 		1
#define OLPG_VTYPE_ELIMINATED 	2

// DFS visit colors
#define OLPG_DFS_WHITE	0 // not visited
#define OLPG_DFS_GRAY 	1 // visit still in progress
#define OLPG_DFS_BLACK	2 // visited


/*class OLPG_VertexInfo
{
	
private:
	
	uint32_t m_pid;		// pool identifier
	int32_t  m_cid;		// contig identifier
	//std::list< std::pair<uint32_t,int32_t> > _ids;			// identifiers of contigs included in the unitig
	//std::list< std::pair<int32_t,int32_t> > _ctgInUnitigPos;	// positions in the unitig of corresponding contig in ids
	//std::list< std::pair<int32_t,int32_t> > _ctgPos;			// positions used in the original contig
	
public:
	
	OLPG_VertexInfo() : _pid(0), _cid(-1) {}
	OLPG_VertexInfo( uint32_t pid, int32_t cid ){ _pid = pid; _cid = cid; }
	
	inline std::pair<uint32_t,int32_t> id(){ return std::make_pair(_pid,_cid); }
};*/

typedef int32_t OLPG_VertexInfo;


struct ExtInfo
{
	int32_t gid; // global id in the SequenceCollection loaded
	//uint32_t pid;
	//int32_t cid;
	
	int32_t start;
	int32_t end;
	int32_t prev_tail;
};


struct OLPGEdgeInfo
{
	uint8_t etype;
	bool type_1; 
	bool type_2;
	
	std::list< ExtInfo >  pos_1;
	std::list< ExtInfo >  pos_2;
};


class OverlapGraph : public boost::adjacency_list< boost::listS, boost::listS, boost::undirectedS, // graph type
	boost::property< boost::vertex_kind_t, OLPG_VertexInfo, boost::property<boost::vertex_index_t, size_t> >, // vertex property
	boost::property< boost::edge_kind_t, OLPGEdgeInfo > >	// edge property
{
	
public:
	
	typedef boost::adjacency_list< boost::listS, boost::listS, boost::undirectedS, 
		boost::property< boost::vertex_kind_t, OLPG_VertexInfo, boost::property<boost::vertex_index_t, size_t> >,
		boost::property< boost::edge_kind_t, OLPGEdgeInfo > > Graph;
		
	typedef boost::property_map<Graph,boost::vertex_index_t>::type IndexMap;
	typedef boost::graph_traits<OverlapGraph>::vertex_descriptor Vertex;
	typedef boost::graph_traits<OverlapGraph>::vertex_iterator VertexIterator;
	typedef boost::graph_traits<OverlapGraph>::edge_descriptor Edge;
	typedef boost::graph_traits<OverlapGraph>::edge_iterator EdgeIterator;
	typedef boost::graph_traits<OverlapGraph>::out_edge_iterator OutEdgeIterator;
	typedef boost::graph_traits<OverlapGraph>::adjacency_iterator AdjacencyIterator;
	
private:
	
	struct OutEdge
	{
		Edge edge;
		int32_t len;
		
		friend bool operator<( const OutEdge &e1, const OutEdge &e2 ){ return e1.len > e2.len; }
	};
	
	IndexMap m_index;
	//std::map< std::pair<uint32_t,int32_t>, Vertex > _id2vertex; // contig_id => vertex_descriptor
	
	uint32_t get_out_edge_len( const Edge &e );
	std::vector< OutEdge > get_out_edge_heap( const Vertex &v );
	std::vector< OutEdge > get_out_edge_heap( const Vertex &v, const bool type );
	
	void build_unitigs_dfs( 
		const Vertex &s, bool s_in_type, std::vector<bool> &visited,
		const SequenceCollection& fasta, std::list<Unitig> &unitigs 
	);
	
	// build unitig from a single contig
	void build_unitig( const SequenceCollection& fasta, const int32_t id, Unitig &utg );
	
	// build unitig from a list of contig sub-sequences
	void build_unitig( const SequenceCollection& fasta, const std::list<ExtInfo> &ext_list, Unitig &utg );

	int32_t check_inclusion( const Overlap& o, const SequenceCollection& fasta, int32_t max_tail );
	
public:
	
	//OverlapGraph();
	OverlapGraph( const std::list<Overlap>& oll, const SequenceCollection& fasta, int32_t max_tail );
	
	void init_vertices_index();
	void init_graph( const std::list<Overlap>& oll, const SequenceCollection& fasta, int32_t max_tail );
	
	void remove_transitive_edges();
	
	void collapse_paths();
	
	std::pair<Vertex,bool> collapse_path( 
		const Vertex &v, bool v_in_type, std::vector<bool> &is_junction,
		std::list< ExtInfo >  &pos_1, std::list< ExtInfo >  &pos_2,
		std::vector<bool> &visited, std::list<Vertex> &compressed 
	);
	
	void degree( const Vertex &v, int &in_deg, int &out_deg );
	bool is_junction_vertex( const Vertex &v );
	void build_unitigs( const SequenceCollection& fasta, std::list<Unitig> &unitigs );
	
	void write_graphviz( const std::string &filename, const SequenceCollection& fasta );
};


#endif	//OVERLAP_GRAPH_HPP

