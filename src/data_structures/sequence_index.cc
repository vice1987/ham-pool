#include "sequence_index.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <zlib.h>

#include "3d_party/bwa/bntseq.h"
#include "3d_party/bwa/bwamem.h"
#include "3d_party/bwa/bwt.h"
#include "3d_party/bwa/is.h"
#include "3d_party/bwa/utils.h"

#include "errors/exit_exception.hpp"

#ifdef USE_MALLOC_WRAPPERS
#include "3d_party/bwa/malloc_wrap.h"
#endif

#include <iostream>

#include <boost/filesystem.hpp>
namespace boost_fs = boost::filesystem;

namespace ham
{
	namespace index
	{

		sequence_index::sequence_index( const SequenceCollection &sc ) :
			m_sc(sc)
		{ }

		
		sequence_index::sequence_index( const SequenceCollection &sc, const std::string &idx_prefix ) :
			sequence_index(sc)
		{ 
			this->build(idx_prefix);
		}


		sequence_index::~sequence_index()
		{
			bwa_idx_destroy(m_idx);
		}


		void
		sequence_index::build( const std::string &idx_prefix )
		{
			// create fasta
			std::string pools_file = idx_prefix + ".pools";
			m_sc.write_to_file( pools_file );

			char *prefix, *str, *str2, *str3;
			int algo_type;

			clock_t t;
			int64_t l_pac;

			prefix = strdup(idx_prefix.c_str());

			str  = (char*)calloc(strlen(prefix) + 10, 1);
			str2 = (char*)calloc(strlen(prefix) + 10, 1);
			str3 = (char*)calloc(strlen(prefix) + 10, 1);

			{ // nucleotide indexing
				const char *fasta = pools_file.c_str();
				gzFile fp = xzopen(fasta, "r");
				t = clock();
				fprintf(stderr, "[bwa_index] Pack FASTA... ");
				l_pac = bns_fasta2bntseq(fp, prefix, 0);
				fprintf(stderr, "%.2f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
				err_gzclose(fp);
			}

			algo_type = l_pac > 50000000 ? 2 : 3; // automatically set the algorithm for generating BWT

			{
				strcpy(str, prefix); strcat(str, ".pac");
				strcpy(str2, prefix); strcat(str2, ".bwt");
				t = clock();
				fprintf(stderr, "[bwa_index] Construct BWT for the packed sequence...\n");
				if (algo_type == 2) bwt_bwtgen(str, str2);
				else if (algo_type == 1 || algo_type == 3) {
					bwt_t *bwt;
					bwt = bwt_pac2bwt(str, algo_type == 3);
					bwt_dump_bwt(str2, bwt);
					bwt_destroy(bwt);
				}
				fprintf(stderr, "[bwa_index] %.2f seconds elapse.\n", (float)(clock() - t) / CLOCKS_PER_SEC);
			}

			{
				bwt_t *bwt;
				strcpy(str, prefix); strcat(str, ".bwt");
				t = clock();
				fprintf(stderr, "[bwa_index] Update BWT... ");
				bwt = bwt_restore_bwt(str);
				bwt_bwtupdate_core(bwt);
				bwt_dump_bwt(str, bwt);
				bwt_destroy(bwt);
				fprintf(stderr, "%.2f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
			}

			{
				const char *fasta = pools_file.c_str();
				gzFile fp = xzopen(fasta, "r");
				t = clock();
				fprintf(stderr, "[bwa_index] Pack forward-only FASTA... ");
				l_pac = bns_fasta2bntseq(fp, prefix, 1);
				fprintf(stderr, "%.2f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
				err_gzclose(fp);
			}

			{
				bwt_t *bwt;
				strcpy(str, prefix); strcat(str, ".bwt");
				strcpy(str3, prefix); strcat(str3, ".sa");
				t = clock();
				fprintf(stderr, "[bwa_index] Construct SA from BWT and Occ... ");
				bwt = bwt_restore_bwt(str);
				bwt_cal_sa(bwt, 32);
				bwt_dump_sa(str3, bwt);
				bwt_destroy(bwt);
				fprintf(stderr, "%.2f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
			}

			free(str3); free(str2); free(str); free(prefix);

			try
			{
				boost_fs::remove( pools_file.c_str() );
			}
			catch( boost_fs::filesystem_error &fe )
			{
				std::cerr << "\n"
						  << "[error] filesystem_error: " << fe.what() << "\n"
				          << "[error] sequence_index::build: could not delete file \"" << pools_file << "\"" 
				          << std::endl;
		        
		        //throw ExitException(2);
			}
		}


		void 
		sequence_index::load( const std::string & idx_prefix )
		{
			m_idx = bwa_idx_load( idx_prefix.c_str(), BWA_IDX_ALL );

			if( m_idx == nullptr )
			{
				std::cerr << "[error] could not load index of file '" << idx_prefix << "'\n";
				throw ExitException(EXIT_FAILURE);
			}

			std::cerr << "[debug] index successfully loaded: " << m_idx << std::endl;
		}


		std::vector<KmerHit> 
		exact_kmer_align( const sequence_index &idx, const Kmer &kmer, int32_t pid )
		{
			std::vector<KmerHit> hits;

			std::string kmer_str{ kmer.to_string() }; // compute k-mer string

			mem_opt_t *mem_opt = mem_opt_init();

			mem_alnreg_v ar = mem_align1( mem_opt, idx.bwt(), idx.bns(), idx.pac(),
			                              static_cast<int>( kmer_str.length() ), kmer_str.c_str() );

			for( size_t h = 0; h < ar.n; ++h )  // for each hit
			{
				// if non-exact match (i.e., score < k), skip it
				if( ar.a[h].score < static_cast<int>( Kmer::length() ) ) continue;

				// retrieve reference id, mapping position, and whether it is forward or reverse match
				mem_aln_t a = mem_reg2aln( mem_opt, idx.bns(), idx.pac(),
				                           static_cast<int>( kmer_str.length() ), kmer_str.c_str(),
				                           &ar.a[h] );

				// skip hits against sequences with smaller (or equal) pool id and hits between different sequences
				if( idx.sc().at( a.rid ).pid() > pid and a.pos + kmer_str.length() <= idx.sc().at(a.rid).length() )
					hits.emplace_back( -1, a.rid, static_cast<int32_t>( a.pos ), a.is_rev > 0 );

				free( a.cigar );
			}

			free( ar.a ); // deallocate BWA-MEM`s hit list
			free( mem_opt ); // deallocate options

			return hits;
		}


	} // namespace ham::index

} // namespace ham


int64_t bwa_seq_len(const char *fn_pac)
{
	FILE *fp;
	int64_t pac_len;
	ubyte_t c;
	fp = xopen(fn_pac, "rb");
	err_fseek(fp, -1, SEEK_END);
	pac_len = err_ftell(fp);
	err_fread_noeof(&c, 1, 1, fp);
	err_fclose(fp);
	return (pac_len - 1) * 4 + (int)c;
}

bwt_t *bwt_pac2bwt(const char *fn_pac, int use_is)
{
	bwt_t *bwt;
	ubyte_t *buf, *buf2;
	int pac_size;
	size_t i;
	FILE *fp;

	// initialization
	bwt = (bwt_t*)calloc(1, sizeof(bwt_t));
	bwt->seq_len = bwa_seq_len(fn_pac);
	bwt->bwt_size = (bwt->seq_len + 15) >> 4;
	fp = xopen(fn_pac, "rb");

	// prepare sequence
	pac_size = (bwt->seq_len>>2) + ((bwt->seq_len&3) == 0? 0 : 1);
	buf2 = (ubyte_t*)calloc(pac_size, 1);
	err_fread_noeof(buf2, 1, pac_size, fp);
	err_fclose(fp);
	memset(bwt->L2, 0, 5 * 4);
	buf = (ubyte_t*)calloc(bwt->seq_len + 1, 1);
	for (i = 0; i < bwt->seq_len; ++i) {
		buf[i] = buf2[i>>2] >> ((3 - (i&3)) << 1) & 3;
		++bwt->L2[1+buf[i]];
	}
	for (i = 2; i <= 4; ++i) bwt->L2[i] += bwt->L2[i-1];
	free(buf2);

	// Burrows-Wheeler Transform
	if (use_is) {
		bwt->primary = is_bwt(buf, bwt->seq_len);
	} else {
#ifdef _DIVBWT
		bwt->primary = divbwt(buf, buf, 0, bwt->seq_len);
#else
		err_fatal_simple("libdivsufsort is not compiled in.");
#endif
	}
	bwt->bwt = (u_int32_t*)calloc(bwt->bwt_size, 4);
	for (i = 0; i < bwt->seq_len; ++i)
		bwt->bwt[i>>4] |= buf[i] << ((15 - (i&15)) << 1);
	free(buf);
	return bwt;
}
