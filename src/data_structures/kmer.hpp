#ifndef KMER_HPP
#define KMER_HPP

#include <iosfwd>
#include <string>
#include <stdint.h>
#include <cassert>

#include "common/constants.hpp"
#include "common/prng_singleton.hpp"
#include "common/types.hpp"


class Kmer
{

protected:

    static uint32_t s_length;
    static uint64_t s_mask;

private:

	uint64_t m_data;

public:

    Kmer() : m_data(0) {}
    Kmer( uint64_t data ) : m_data(data) {}

    Kmer( std::string str ) : m_data{0}
    {
        assert( str.length() == s_length && "Kmer(std::string): string length does not match k-mer length" );

        uint64_t base_code {0};

        for( size_t j=0; j < s_length; ++j )
        {
            base_code = str[j] != 'N' ? ham::constants::int2base[static_cast<int>(str[j])] : PrngSingleton::instance().rand_base();
            m_data = (m_data << 2) | base_code;
        }
    }

    static void set_length( uint32_t k );
    static uint32_t length();
    static void set_mask( uint32_t k );
	static uint64_t mask();

    // returns a k-mer which is the reverse complement of a given k-mer
    static Kmer reverse_complement( const Kmer& k );

    // k-mer hash operator
    struct kmerHash
    {
        size_t operator()( const Kmer& k ) const { return k.hash(); }
    };

    // k-mer equivalence operator
    struct kmerEq
    {
        bool operator()( const Kmer &k1, const Kmer &k2 ) const
        {
            if( k1.m_data == k2.m_data ) return true;

			Kmer k2_r = Kmer::reverse_complement(k2);
			return k1.m_data == k2_r.m_data;
        }
    };

	void set_data( uint64_t data );
	uint64_t data() const;

	// reverse complement of the k-mer
    void reverse_complement();

    // return a hash value for the k-mer
    size_t hash() const;

	// left-shift the k-mer appending base b and removing leftmost base
    // assumption: base < 4 (i.e., base has to belong to {A,C,T,G})
    Kmer& shift_append( base_t b );
    Kmer& shift_prepend( base_t b );

	// get string representing k-mer
	void to_string( std::string &str ) const;
	std::string to_string() const;

	// equivalence operator
    bool operator==( const Kmer& o );

    friend std::ostream& operator<<( std::ostream& os, const Kmer& kmer );
};


#endif // KMER_HPP
