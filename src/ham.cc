#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

#include <list>
#include <vector>

#include "common/function_stats.hpp"
#include "common/utils.hpp"
#include "errors/exit_exception.hpp"
#include "options/options_ham.hpp"
#include "modules/module_ham.hpp"

int main(int argc, char *argv[])
{
	try
	{
		FunctionStats fstats(__func__);

		OptionsHam options(argc,argv);

		ModuleHam ham;
		ham.execute(options);
	}
	catch( ExitException &e )
	{
		exit(e.code);
	}

	return EXIT_SUCCESS;
}
