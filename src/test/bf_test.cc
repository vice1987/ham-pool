#include "common/algorithms.hpp"
#include "common/function_stats.hpp"
#include "common/utils.hpp"
#include "data_structures/bloom_filter.hpp"
#include "data_structures/sequence_collection.hpp"
#include "data_structures/kmer.hpp"

#include <fstream>

int main(int argc, char *argv[])
{
	if( argc != 2 )
	{
		std::cout << "usage: " << argv[0] << " <fasta_list>\n" << std::endl;
		return EXIT_FAILURE;
	}

	uint32_t k_len = 16;
	Kmer::set_length(k_len);
	
	std::vector<std::string> pool_fasta;

	// load fasta filenames
    load_filenames( argv[1], pool_fasta );

	// load pools' sequences
    std::cout << "[ham] loading sequences..." << std::flush;
    SequenceCollection sc(pool_fasta,5000);
	std::cout << "done." << std::endl;

	KmerCollection kc;
	std::cout << "[ham] computing kmer collection..." << std::flush;
	{
		FunctionStats fstats("kmer_collection_load");
		
		for( size_t seq_id{0}; seq_id < sc.size(); ++seq_id )
		{
			const Sequence& seq = sc.at(seq_id);
			
			size_t seq_size = seq.size();
			if( seq_size < k_len ) continue;

			Kmer kmer;
		    size_t last_n = std::string::npos;
		    size_t last_i = seq_size - k_len; // last position where it is possible to load a k-mer

		    size_t i = 0;      // position of first base to load
		    size_t j = i+k_len-1;  // position of last base to load
		    bool shift = false;  // whether it is possible to shift_append last k-mer
		    bool is_next_base_n; // whether base following k-mer at position i is unknown

		    while( i <= last_i )
		    {
		        if( !shift ) // add k-mer at position i if it does not contain unknown bases
		        {
		            last_n = kmer_load(kmer,seq,i);
		            if( last_n == std::string::npos ) kc.add_kmer(kmer);
		        }
		        else // add shifted k-mer to the collection
		        {
		            kc.add_kmer( kmer_shift_append(kmer,seq[j]) );
		        }

		        if( j+1 >= seq_size ) break;

		        is_next_base_n = seq[j+1] == ham::constants::BASE_N;
		        last_n = is_next_base_n ? j+1 : last_n;

		        shift = (last_n == std::string::npos);
		        i = shift ? i+1 : last_n+1;
		        j = i+k_len-1;
		    }
		}
	}
    std::cout << "done. " << kc.size() << " kmers loaded." << std::endl;

    size_t taken{0};
    std::cout << "[ham] computing fingerprint..." << std::flush;
	{
		FunctionStats fstats("fingerprint_build");

		std::vector<std::string> fingerprint;
		std::ofstream ofs("out.fp.fasta");
		
		for( size_t seq_id{0}; seq_id < sc.size(); ++seq_id )
		{
			fingerprint.clear();

			const Sequence& seq = sc.at(seq_id);
			
			size_t seq_size = seq.size();
			if( seq_size < k_len ) continue;

			Kmer kmer;
		    size_t last_n = std::string::npos;
		    size_t last_i = seq_size - k_len; // last position where it is possible to load a k-mer

		    size_t i = 0;      // position of first base to load
		    size_t j = i+k_len-1;  // position of last base to load
		    bool shift = false;  // whether it is possible to shift_append last k-mer
		    bool is_next_base_n; // whether base following k-mer at position i is unknown

		    int32_t last_freq{0};
		    size_t kmers_taken{0};

		    while( i <= last_i )
		    {
		        if( !shift ) // add k-mer at position i if it does not contain unknown bases
		        {
		            last_n = kmer_load(kmer,seq,i);
		            if( last_n == std::string::npos )
		            {
		            	int32_t curr_freq = kc.get_freq(kmer);

		            	if( curr_freq != last_freq and curr_freq >= 2 and curr_freq <= 10 )
		            	{
		            		++kmers_taken;
		            		fingerprint.emplace_back( kmer.to_string() );
		            	}

		            	last_freq = curr_freq;
		            }
		        }
		        else // add shifted k-mer to the collection
		        {
		        	kmer_shift_append(kmer,seq[j]);
		        	int32_t curr_freq = kc.get_freq(kmer);

		        	if( curr_freq != last_freq and curr_freq >= 2 and curr_freq <= 10 )
		        	{
		        		++kmers_taken;
		            	fingerprint.emplace_back( kmer.to_string() );
		        	}

		        	last_freq = curr_freq;
		        }

		        if( j+1 >= seq_size ) break;

		        is_next_base_n = seq[j+1] == ham::constants::BASE_N;
		        last_n = is_next_base_n ? j+1 : last_n;

		        shift = (last_n == std::string::npos);
		        i = shift ? i+1 : last_n+1;
		        j = i+k_len-1;
		    }

		    taken += kmers_taken;

		    if( fingerprint.size() > 0 )
		    {
		    	ofs << ">" << seq.str_id() << "\n";
			    for( size_t i{0}; i < fingerprint.size(); ++i )
			    	ofs << (i==0 ? "" : "N") << fingerprint[i];
			    ofs << std::endl;
		    }
		}
	}
    std::cout << "done. fp mean size = " << (1.0*taken)/sc.size() << " kmers loaded." << std::endl;

	return EXIT_SUCCESS;
}
