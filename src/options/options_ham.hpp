#ifndef OPTIONS_HAM_HPP
#define OPTIONS_HAM_HPP

#include "options/options.hpp"


class OptionsHam : public Options
{

public:

    OptionsHam() : Options() {}
    OptionsHam( int argc, char *argv[] ) : Options() { process(argc,argv); }

    virtual ~OptionsHam() {}

    void process( int argc, char *argv[] );
};


#endif /* OPTIONS_HAM_HPP */
