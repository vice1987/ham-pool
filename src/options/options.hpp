#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <stdint.h>
#include <string>
#include <vector>

#include "common/boost_includes.hpp"


class Options
{

public:

    int m_argc;
    char **m_argv;

    // INPUT PARAMETERS

	int32_t m_min_seq_size;

    uint32_t m_k;            // size of k-mers

	uint32_t m_min_shared_k;
    int32_t  m_max_gap;

	int32_t m_max_freq;
	int32_t m_min_fp_size;

    int32_t m_max_tail;
	int32_t m_min_ol;
	double  m_min_ident;

	int32_t m_threads_num;

    bool m_load_index; // load a previously created index
	std::string m_index_prefix;

	std::string m_fasta_list;
    std::string m_olp_file;
    std::string m_output_prefix;

    // HIDDEN PARAMETERS

    bool m_debug;
    bool m_output_graphs;

public:

    Options() { set_defaults(); }
    Options(const Options &orig);

    virtual ~Options() {}

    virtual void process(int argc, char *argv[]) = 0;

protected:

    void set_defaults();

};


#endif /* OPTIONS_HPP */
