#include "options.hpp"


void Options::set_defaults()
{
    // INPUT PARAMETERS
	m_min_seq_size = 0;
    m_k = 21;
    m_min_shared_k = 2;
    m_max_gap = 100;
	m_max_freq = 10;
	m_min_fp_size = 5;
	m_min_ol = 500;
	m_min_ident = 0.95;
	m_max_tail = 100;

	m_load_index = false;
	m_index_prefix = "";

    m_fasta_list = "";
    m_olp_file = "";

	m_threads_num = 1;

    // OUTPUT PARAMETERS
    m_output_prefix = "out";

    // HIDDEN PARAMETERS
    m_debug = false;
    m_output_graphs = false;
}
