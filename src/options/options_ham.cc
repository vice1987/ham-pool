#include "options_ham.hpp"

#include <iostream>

#include "common/boost_includes.hpp"
#include "common/limits.hpp"
#include "data_structures/kmer.hpp"
#include "errors/exit_exception.hpp"


void OptionsHam::process( int argc, char *argv[] )
{
    m_argc = argc;
    m_argv = argv;

    std::string command_basename = boost_fs::basename(argv[0]);

    // PROCESS PARAMETERS

    std::stringstream desc_ss;
    desc_ss << "\nProgram: " << command_basename << " (Hierarchical Assemblies Merger)\n"
            << "Version: 0.1\n\nUsage";

    bpo::options_description user_opts(desc_ss.str().c_str());
    user_opts.add_options()

        // COMMON PARAMETERS
        ("help,h", "produce help message")
        ("version,v", "print version and exit")

        // INPUT PARAMETERS
        ("fasta,f", bpo::value<std::string>(&m_fasta_list)->required(),
                "input file containing a list of fasta files (one for each pool)")
    	("min-seq-size,s", bpo::value<int32_t>(&m_min_seq_size),
                "minimum size of pool sequences")
    	("ham-index", bpo::value<std::string>(&m_index_prefix),
    			"load previously created indexes files \"<ham-index>.hamidx.*\" "
    			"(if input files changes this parameter should NOT be used)")
        ("olp-file", bpo::value<std::string>(&m_olp_file),
                "load .olp file instead of re-computing alignments")
        ("k,k", bpo::value<uint32_t>(&m_k)->required(),
    			"size of k-mers (it must be an odd integer between 11 and 31)")
        ("min-shared-kmers,c", bpo::value<uint32_t>(&m_min_shared_k),
                "minimum number of kmers to be shared between a fingerprint and a "
    			"contig to perform a proper alignment")
        ("max-gap", bpo::value<int32_t>(&m_max_gap),
    			"maximum allowed gap for k-mers used in fingerprints")
    	("max-freq", bpo::value<int32_t>(&m_max_freq),
    			"maximum k-mer frequency to use it in fingerprints")
    	("min-fp-size", bpo::value<int32_t>(&m_min_fp_size),
    			"minimum number of k-mers to build a fingerprint")
    	("min-olp", bpo::value<int32_t>(&m_min_ol),
    			"minimum length used in overlap detection")
        ("min-ident", bpo::value<double>(&m_min_ident),
                "minimum identity used in overlap detection (it must be a real in [0,1])")
    	("threads,t", bpo::value<int32_t>(&m_threads_num),
    			"number of threads")

        // OUTPUT PARAMETERS
        ("prefix,p", bpo::value<std::string>(&m_output_prefix), "output files' prefix")
    ;

    // HIDDEN OPTIONS
    bpo::options_description hidden_opts("Hidden options");
    hidden_opts.add_options()
        ("debug", "enable additional output files.")
        ("output-graphs", "write dot files of the overlap graphs in ./tmp/ sub-directory.")
    ;

    bpo::options_description all_opts("Available options");
    all_opts.add(user_opts).add(hidden_opts);

    bpo::variables_map vm;
    try
    {
        bpo::store(bpo::parse_command_line(argc, argv, all_opts), vm);

        if (vm.count("help"))
        {
            std::cout << user_opts << std::endl;
            throw ExitException(EXIT_SUCCESS);
        }

        if (vm.count("version"))
        {
            std::cout << "Program: " << command_basename << "\n"
                      << "Version: 0.1"
                      << std::endl;
            throw ExitException(EXIT_SUCCESS);
        }

        bpo::notify(vm);
    }
    catch (bpo::error & error)
    {
        std::cerr << "[error] " << error.what() << ", try -h for help." << std::endl;
        throw ExitException(EXIT_FAILURE);
    }


    //// PROCESS INPUT PARAMETERS

	if(vm.count("ham-index")) m_load_index = true;

	if(vm.count("min-seq-size") && m_min_seq_size < 0)
	{
		std::cerr << "[error] -s parameter must be a non-negative integer." << std::endl;
		throw ExitException(EXIT_FAILURE);
	}

    // check k parameter
    if( m_k < ham::limits::MIN_KMER || m_k > ham::limits::MAX_KMER )
    {
        std::cerr << "[error] -k parameter must be an integer between 11 and 31" << std::endl;
        throw ExitException(EXIT_FAILURE);
    }
	else if( m_k % 2 == 0 )
	{
		std::cerr << "[error] -k parameter must be odd" << std::endl;
		throw ExitException(EXIT_FAILURE);
	}
    else
    {
        Kmer::set_length(m_k);
    }

    // check max gap value
    if( m_max_gap < 0 )
    {
        std::cerr << "[error] --max-gap parameter must be at least 0" << std::endl;
        throw ExitException(EXIT_FAILURE);
    }

    // check identity parameter
    if( vm.count("min-ident") )
    {
        if( m_min_ident < 0.0 )
        {
            m_min_ident = 0.0;
            std::cerr << "[warning] minimum identity is less than 0... it has been set to 0.0." << std::endl;
        }

        if( m_min_ident > 1.0 )
        {
            m_min_ident = 1.0;
            std::cerr << "[warning] minimum identity is greater than 1... it has been set to 1.0." << std::endl;
        }
    }

	// check threads parameter
	if( vm.count("threads") && m_threads_num < 1 ) m_threads_num = 1;

    // process HIDDEN PARAMETERS

    if (vm.count("debug")) m_debug = true;
    if (vm.count("output-graphs")) m_output_graphs = true;

    // PROCESS OUTPUT PARAMETERS
}
