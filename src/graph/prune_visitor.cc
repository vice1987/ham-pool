#include "prune_visitor.hpp"

#include <iostream>
#include <array>

namespace ham
{
	namespace graph
	{
		void 
		prune_visitor::previsit( string_graph<> & ) 
		{
			// nothing to do
		}


		void 
		prune_visitor::visit( string_graph<> &g, vertex_descriptor vd )
		{
			auto in_edges  = g.at(vd).adj_edges(edge_kind::IN);
			auto out_edges = g.at(vd).adj_edges(edge_kind::OUT);

			if( in_edges.size() == 0 and out_edges.size() == 0) // connected component consisting of a single vertex
			{
				// retain only long enough and well-covered sequences
			}
			else if( in_edges.size() == 0 and out_edges.size() == 1 )
			{
				const edge e = out_edges.front();
				edge_kind mate_kind = ham::graph::mate(e,g).kind();

				if( g.at(e.target()).degree(mate_kind) > 1 ) // current vertex is on a dead-end branch
				{
					ham::graph::clear_vertex(vd,g);
					ham::graph::delete_vertex(vd,g);
				}	
			}
			else if( out_edges.size() == 0 and in_edges.size() == 1 )
			{
				const edge e = in_edges.front();

				edge_kind mate_kind = ham::graph::mate(e,g).kind();

				if( g.at(e.target()).degree(mate_kind) > 1 ) // current vertex is on a dead-end branch
				{
					ham::graph::clear_vertex(vd,g);
					ham::graph::delete_vertex(vd,g);
				}	
			}
		}


		void 
		prune_visitor::postvisit( string_graph<> & g )
		{
			// actual deletion of edges marked for removal
			for( vertex_descriptor vd{0}; vd < g.num_vertices(); ++vd )
				ham::graph::delete_marked_edges(vd,g);
		}
	}
}
