#ifndef STRING_GRAPH_HPP
#	error "This is a private file, include string_graph.hpp instead."
#endif

namespace ham
{
	namespace graph
	{

		template< typename GID >
		string_graph<GID>::string_graph() 
			: m_deleted(), m_vertices(), m_gid2vd()
		{ 
			m_gid2vd.set_deleted_key( ham::limits::MAX_SEQUENCE_ID );
		}

		// Number of vertices
		template< typename GID >
		inline 
		size_t string_graph<GID>::num_vertices() const 
		{ 
			return m_vertices.size();
		}

		// Vertices iterators
		template< typename GID >
		inline
		std::pair< typename string_graph<GID>::iterator, typename string_graph<GID>::iterator > 
		string_graph<GID>::vertices()
		{
			return { m_vertices.begin(), m_vertices.end() };
		}

		// input: vertex descriptor
		// return: vertex const_reference
		template< typename GID >
		inline
		const vertex& string_graph<GID>::at( vertex_descriptor vd ) const
		{
			return m_vertices.at(vd);
		}

		// input: vertex descriptor
		// output: vertex reference
		template< typename GID >
		inline
		vertex& string_graph<GID>::at( vertex_descriptor vd )
		{
			return m_vertices.at(vd);
		}
		
		// input: sequence ID, sequence length
		template< typename GID >
		inline
		vertex_descriptor string_graph<GID>::add_vertex( GID gid, size_t len )
		{
			auto it = m_gid2vd.insert( {gid,m_vertices.size()} );

			if( it.second ) // sequence had not been inserted yet
			{
				m_deleted.push_back(false);
				m_vertices.emplace_back(gid,len);
			}

			return (it.first)->second; // return vertex descriptor
		}

		// mark a vertex as deleted
		template< typename GID >
		inline
		void string_graph<GID>::delete_vertex( vertex_descriptor vd )
		{
			assert( vd < m_deleted.size() );
			m_deleted[vd] = true;
		}


		// return whether a vertex is valid
		template< typename GID >
		inline
		bool string_graph<GID>::has_vertex( vertex_descriptor vd )
		{
			assert( vd < m_deleted.size() );
			return !m_deleted[vd];
		}


		// precondition: edge does not exist
		// input: source/target vertex descriptors, overlap of source in target, edge direction
		template< typename GID >
		inline
		edge_descriptor string_graph<GID>::add_edge( vertex_descriptor source_vd, 
													 vertex_descriptor target_vd, 
													 ham::range_t overlap, edge_dir edir, edge_kind etype )
		{
			// add edge in source adjacency list
			size_t index = m_vertices.at(source_vd).link_to( target_vd, overlap, edir, etype );

			return {source_vd,index};
		}


		template< typename GID >
		inline
		bool string_graph<GID>::has_edge( edge_descriptor ed )
		{
			vertex_descriptor source{ed.first};
			const edge& e = m_vertices.at(ed.first).at(ed.second);
			vertex_descriptor target{e.target()};

			return has_vertex(source) and 
			       has_vertex(target) and 
			       e.good();
		}


		template< typename GID > template< typename visitor_t >
		inline
		void string_graph<GID>::accept( visitor_t &visitor )
		{
			visitor.previsit(*this);
			
			for( vertex_descriptor vd=0; vd < m_vertices.size(); ++vd )
				if(has_vertex(vd)) visitor.visit(*this,vd);
			
			visitor.postvisit(*this);
		}


		template< typename GID >
		vertex_descriptor
		source( edge_descriptor ed, const string_graph<GID> & )
		{
			return ed.first;
		}


		template< typename GID >
		vertex_descriptor
		target( edge_descriptor ed, const string_graph<GID> &g )
		{
			return g.at(ed.first).at(ed.second).target();
		}


		template< typename GID >
		edge_descriptor
		mate( edge_descriptor ed, const string_graph<GID> &g )
		{
			const edge& e = g.at(ed.first).at(ed.second);
			return { e.target(), e.mate_index() };
		}


		template< typename GID >
		edge&
		mate( const edge& e, string_graph<GID> &g )
		{
			return g.at( e.target() ).at( e.mate_index() );
		}		


		template< typename GID >
		void 
		bind_mates( edge_descriptor ed_1, edge_descriptor ed_2, string_graph<GID> &g )
		{
			g.at( ed_1.first ).at( ed_1.second ).set_mate_index( ed_2.second );
			g.at( ed_2.first ).at( ed_2.second ).set_mate_index( ed_1.second );
		}


		template< typename GID >
		void sort_out_edges_by_length( vertex &v, string_graph<GID> &g )
		{
			auto & v_adj = v.edges();

			std::sort( v_adj.begin(), v_adj.end(),
					   
					   [&](const edge& a, const edge& b) 
					   {
					   		return (!a.inclusion() and !b.inclusion() and label_length(a,g) < label_length(b,g)) or
					   			   (!a.inclusion() and b.inclusion()); 
					   } );

			update_adj_indices(v,g);
		}


		template< typename GID >
		size_t label_length( const edge& e, const string_graph<GID> &g )
		{
			size_t seq_len = g.at(e.target()).length();
			assert( e.overlap().right <= seq_len );

			ham::range_t overlap = e.overlap();
			return std::max( overlap.left, seq_len - overlap.right );
		}


		template< typename GID >
		void delete_vertex( vertex_descriptor vd, string_graph<GID> &g )
		{
			g.delete_vertex(vd);
		}


		template< typename GID >
		void clear_vertex( vertex_descriptor vd, string_graph<GID> &g )
		{
			for( auto &e : g.at(vd).edges() )
			{
				e.mark_edge();
				ham::graph::mate(e,g).mark_edge();
			}
		}


		template< typename GID >
		void delete_marked_edges( vertex_descriptor vd, string_graph<GID> &g )
		{
			auto & v_adj = g.at(vd).edges();

			v_adj.erase(std::remove_if(v_adj.begin(), 
									   v_adj.end(), 
									   [](const edge& e){return e.marked();} ), 
						v_adj.end());

			update_adj_indices(g.at(vd),g);
		}


		template< typename GID >
		void update_adj_indices( vertex& v, string_graph<GID> &g )
		{
			auto & v_adj = v.edges();

			for( size_t idx{0}; idx < v_adj.size(); ++idx ) // for each v -> w
				g.at( v_adj[idx].target() ).at( v_adj[idx].mate_index() ).set_mate_index(idx);
		}
	}
}
