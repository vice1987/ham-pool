#ifndef CONTREM_VISITOR_HPP
#define CONTREM_VISITOR_HPP

#include <iostream>
#include <fstream>

#include "base_visitor.hpp"
#include "data_structures/sequence_collection.hpp"

namespace ham
{
	namespace graph
	{
		class contrem_visitor : base_visitor
		{

		public:

			contrem_visitor() { }

			void previsit( string_graph<> & g ) override;
			void visit( string_graph<> & g, vertex_descriptor vd ) override;
			void postvisit( string_graph<> & g ) override;
		};
	}
}

#endif // CONTREM_VISITOR_HPP
