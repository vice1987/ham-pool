#ifndef STRING_GRAPH_HPP
#define	STRING_GRAPH_HPP

#include <cassert>
#include <limits>
#include <vector>

#include <boost/dynamic_bitset.hpp>
#include <google/sparse_hash_map>

#include "common/limits.hpp"
#include "vertex.hpp"

namespace ham
{
	namespace graph
	{
		template< typename GID = size_t >
		class string_graph
		{

		public:

			// typedefs

			using boost_bitset_t = boost::dynamic_bitset<>;
			using iterator = std::vector<vertex>::iterator;

			// constructors

			string_graph();
			string_graph( const string_graph &  ) = default;
			string_graph( string_graph && ) = default;

			string_graph& operator=( const string_graph & ) = default;
			string_graph& operator=( string_graph && ) = default;

			// vertices functions

			size_t num_vertices() const;

			std::pair<iterator,iterator> vertices();
			
			const vertex&  at( vertex_descriptor vd ) const;

			vertex& at( vertex_descriptor vd );
			
			vertex_descriptor add_vertex( GID gid, size_t len );

			void delete_vertex( vertex_descriptor vd );

			bool has_vertex( vertex_descriptor vd );

			// edges functions

			edge_descriptor add_edge( vertex_descriptor source_vd, 
									  vertex_descriptor target_vd, 
									  ham::range_t overlap, edge_dir edir, edge_kind etype );

			bool has_edge( edge_descriptor ed );

			// others
			template< typename visitor_t >
			void accept( visitor_t &visitor );

		private:

			// bitvector to track whether a vertex has been deleted
			boost_bitset_t m_deleted;

			// vector of vertices
			std::vector<vertex> m_vertices;
			
			// hashtable: sequence identifier -> vertex descriptor
			google::sparse_hash_map<GID,vertex_descriptor> m_gid2vd;

		}; // class string_graph


		// **********************************************************
		// Utility functions which operate on a string_graph object
		// **********************************************************

		template< typename GID >
		vertex_descriptor source( edge_descriptor ed, const string_graph<GID> &g );


		template< typename GID >
		vertex_descriptor target( edge_descriptor ed, const string_graph<GID> &g );


		template< typename GID >
		edge_descriptor mate( edge_descriptor ed, const string_graph<GID> &g );


		template< typename GID >
		edge& mate( const edge& e, string_graph<GID> &g );


		template< typename GID >
		void bind_mates( edge_descriptor ed_1, edge_descriptor ed_2, string_graph<GID> &g );


		template< typename GID >
		void sort_out_edges_by_length( vertex &v, string_graph<GID> &g );	


		template< typename GID >
		size_t label_length( const edge& e, const string_graph<GID> &g );


		template< typename GID >
		void delete_vertex( vertex_descriptor vd, string_graph<GID> &g );

		template< typename GID >
		void clear_vertex( vertex_descriptor vd, string_graph<GID> &g );

		template< typename GID >
		void delete_marked_edges( vertex_descriptor vd, string_graph<GID> &g );


		template< typename GID >
		void update_adj_indices( vertex& v, string_graph<GID> &g );

	} // namespace ham::graph

} // namespace ham


#include "impl/string_graph.impl.hpp"

#endif	//STRING_GRAPH_HPP

