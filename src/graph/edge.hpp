#ifndef EDGE_HPP
#define EDGE_HPP

#include "data_structures/range.hpp"

namespace ham
{
	namespace graph
	{

		typedef size_t vertex_descriptor;
		typedef std::pair<vertex_descriptor,size_t> edge_descriptor;

		enum class edge_kind : uint8_t 
		{ 
			IN       = 0, 
			OUT      = 3,
			INCLUDED = 1,
			CONTAINS = 2
		};

		inline
		edge_kind complement( edge_kind k ) 
		{ 
			return static_cast<edge_kind>( ~(static_cast<uint8_t>(k)) & uint8_t{3} ); 
		}


		enum class edge_dir : uint8_t
		{
			FORWARD = 0,
			REVERSE = 1
		};
		
		
		class edge
		{

		public:

			// precondition target id fits in 63 bits
			edge( vertex_descriptor target, ham::range_t overlap, edge_dir dir, edge_kind kind ) :
				m_target{target}, m_overlap{overlap}, m_dir{dir}, m_kind{kind}
			{ }

			// return whether the edge had been marked for delete
			bool good() const
			{ 
				return m_target >> 63 == 0; 
			}

			bool marked() const
			{
				return m_target >> 63 != 0;
			}

			// mark the edge as "deleted" without actually removing it
			void mark_edge() 
			{ 
				m_target |= size_t{1} << 63; 
			}

			// set mate-edge index w.r.t. target's adjacency list
			void set_mate_index( size_t index ) 
			{ 
				m_mate = index; 
			}

			// return mate's index w.r.t. target's adjacency list
			size_t mate_index() const 
			{ 
				return m_mate; 
			}

			// return target vertex descriptor
			vertex_descriptor target() const 
			{ 
				return (m_target & m_mask); 
			}

			// return edge kind
			edge_kind kind() const 
			{ 
				return m_kind; 
			}

			// returns true if source is included in target, false otherwise
			bool included() const
			{
				return m_kind == edge_kind::INCLUDED;
			}

			// returns true if source contains target, false otherwise
			bool contains() const
			{
				return m_kind == edge_kind::CONTAINS;
			}

			// returns true if either target includes source or viceversa
			bool inclusion() const
			{
				return this->included() or this->contains();
			}

			// returns true if either target contains source or viceversa
			bool containment() const
			{
				return this->inclusion();
			}

			// return range of overlap in target
			ham::range_t overlap() const
			{
				return m_overlap;
			}

			// return overlap length
			size_t overlap_len() const
			{ 
				assert( m_overlap.left < m_overlap.right );
				return m_overlap.right - m_overlap.left;
			}

			//bool set_valid() { m_vid &= ~(size_t{1} << 63); }

		private:

			vertex_descriptor m_target;       // deleted:1, vertex descriptor:63 (counting from MSB to LSB)
			size_t            m_mate;
			ham::range_t      m_overlap;
			
			edge_dir          m_dir  : 1; // whether the reverse complement of target should be considered
			edge_kind         m_kind : 2; // whether the overlap is a containment/inclusion or a left/right-extreme overlap

			static const vertex_descriptor m_mask { ~(vertex_descriptor{1} << 63) }; // mask to retrieve target's vertex_descriptor

		}; // class edge
	}
}

#endif // EDGE_HPP
