#include "ap_visitor.hpp"

#include <iostream>
#include <sstream>

#include "common/constants.hpp"

namespace ham
{
	namespace graph
	{
		void 
		ap_visitor::previsit( string_graph<> &g ) 
		{
			size_t num_vertices{ g.num_vertices() };
			
			m_disc.resize(num_vertices);
			m_low.resize(num_vertices);
			m_parent.resize(num_vertices,NIL);

			m_visited.reset();
			m_visited.resize(num_vertices);

			m_ap.reset();
			m_ap.resize(num_vertices);
		}

		void 
		ap_visitor::visit( string_graph<> &g, vertex_descriptor vd )
		{
			if( not m_visited[vd] ) 
				this->ap_find(g,vd);
		}


		void 
		ap_visitor::ap_find( string_graph<> &g, vertex_descriptor vd )
		{
			// Mark the current node as visited
			m_visited[vd] = true;

			size_t children{0}; // number of children in the dfs-tree
		 
		    // update discovery time and low value
		    m_time++;
		    m_low[vd] = m_time;
		    m_disc[vd] = m_time;

		    for( const auto &e : g.at(vd).edges() )
		    {
		    	vertex_descriptor td{ e.target() };

		    	if( not m_visited[td] ) // "forward-edge"
		    	{
		    		// update dfs-tree and make the recursive call
		    		children++;
		    		m_parent[td] = vd;
		    		this->ap_find(g,td);

		    		m_low[vd] = std::min( m_low[vd], m_low[td] );

		    		// vd is the root of a dfs-tree which has at least two children
		    		if( m_parent[vd] == NIL and children >= 2 )
		    			m_ap[vd] = true;

		    		// vd is not a root and the low value of one of its children is 
		    		// greater than the discovery value of vd.
		            if( m_parent[vd] != NIL && m_low[td] >= m_disc[vd] )
		            	m_ap[vd] = true;
		    	}
		    	else if( td != m_parent[vd] ) // "back-edge"
		    	{
		    		m_low[vd] = std::min( m_low[vd], m_disc[td] );
		    	}
		    }
		}

		void 
		ap_visitor::postvisit( string_graph<> & )
		{
			m_disc.resize(0);
			m_low.resize(0);
			m_parent.resize(0);
			m_visited.resize(0);
		}
	}
}
