#ifndef VERTEX_HPP
#define VERTEX_HPP

#include "edge.hpp"

#include <deque>

namespace ham
{
	namespace graph
	{
		enum class vertex_color : uint8_t
		{
			WHITE,
			GRAY,
			BLACK
		};


		class vertex
		{

		private:

			struct subseq_t
			{
				size_t gid;
				ham::range_t range;
			};


		public:


			vertex( size_t gid, size_t len ) : 
				m_gid{gid}, 
				m_len{len},
				m_incl_len{len},
				m_desc{ {gid,{0,len}} }
			{ }


			size_t gid() const 
			{ 
				return m_gid; 
			}


			size_t length() const
			{
				return m_len;
			}


			// input: target sequence ID, overlap, edge type
			// returns the index of the inserted edge in the adjacency list
			// note: be aware that the adjacency list allows duplicates
			size_t link_to( vertex_descriptor target, ham::range_t overlap, edge_dir dir, edge_kind type )
			{
				m_edges.emplace_back( target, overlap, dir, type );
				return m_edges.size()-1;
			}


			void embed_vertex( const vertex & v )
			{
				m_incl_len += v.m_incl_len;
			}


			// computed using exclusively included sequences
			double coverage()
			{
				return static_cast<double>(m_incl_len)/m_len;
			}


			const std::vector<edge>& edges() const 
			{ 
				return m_edges; 
			}


			std::vector<edge>& edges()
			{ 
				return m_edges; 
			}


			std::vector<edge> out_edges( edge_kind kind )
			{
				std::vector<edge> out;
				// out.reserve( m_edges.size() );
				std::copy_if(m_edges.begin(), m_edges.end(), std::back_inserter(out), 
					[=]( edge const& e ) { return !e.inclusion() and e.kind() == kind; }
				);

				return out;
			}


			std::vector<edge> adj_edges( edge_kind kind ) // the same as out_edges(kind)
			{
				std::vector<edge> out;
				// out.reserve( m_edges.size() );
				std::copy_if(m_edges.begin(), m_edges.end(), std::back_inserter(out), 
					[=]( edge const& e ) { return !e.inclusion() and e.kind() == kind; }
				);

				return out;
			}


			size_t degree()
			{
				size_t deg{0};

				for( const auto & e : m_edges )
					if( e.good() ) ++deg;

				return deg;
			}


			size_t degree( edge_kind kind )
			{
				size_t deg{0};

				for( const auto & e : m_edges )
					if( e.good() and e.kind() == kind ) deg++;

				return deg;
			}


			bool is_linear()
			{
				size_t in_deg  = this->degree( edge_kind::IN );
				size_t out_deg = this->degree( edge_kind::OUT );

				return in_deg == 1 and out_deg == 1;
			}


			bool is_junction()
			{
				size_t in_deg  = this->degree( edge_kind::IN );
				size_t out_deg = this->degree( edge_kind::OUT );

				return in_deg != 1 or out_deg != 1;
			}


			const edge&
			at( size_t i ) const
			{
				return m_edges.at(i);
			}


			edge&
			at( size_t i )
			{
				return m_edges.at(i);
			}


		private:

			// TODO: remove following member
			size_t m_gid; // global sequence identifier
			size_t m_len; // sequence length

			size_t m_incl_len; // length of included nodes (sequences) which have been spliced/removed
			std::deque< subseq_t > m_desc;
			
			std::vector< edge > m_edges; // incident edges

		}; // class vertex

	} // namespace graph

} // namespace ham

#endif // VERTEX_HPP
