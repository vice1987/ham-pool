#ifndef PRUNE_VISITOR_HPP
#define PRUNE_VISITOR_HPP

#include <iosfwd>
#include <sstream>
#include <fstream>

#include "base_visitor.hpp"
#include "data_structures/sequence_collection.hpp"

namespace ham
{
	namespace graph
	{
		class prune_visitor : base_visitor
		{

		public:

			prune_visitor( const SequenceCollection &sc, const char *prefix ) :
				m_sc(sc),
				m_prefix{prefix}
			{ }

			void previsit( string_graph<> & g ) override;
			void visit( string_graph<> & g, vertex_descriptor vd ) override;
			void postvisit( string_graph<> & g ) override;

		private:

			const SequenceCollection& m_sc;
			const std::string m_prefix;

			std::ofstream m_ofs;
		};
	}
}

#endif // PRUNE_VISITOR_HPP
