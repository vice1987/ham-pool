#ifndef BASE_VISITOR_HPP
#define BASE_VISITOR_HPP

#include "string_graph.hpp"

namespace ham
{
	namespace graph
	{

		// interface of a generic string_graph visitor
		class base_visitor
		{

		public:

			virtual void previsit( string_graph<> & ) = 0;
			virtual void visit( string_graph<> &, vertex_descriptor ) = 0;
			virtual void postvisit( string_graph<> & ) = 0;

			virtual ~base_visitor() { }

		};

	}
}


#endif // BASE_VISITOR_HPP
