#ifndef AP_VISITOR_HPP
#define AP_VISITOR_HPP

#include <iostream>
#include <fstream>

#include "base_visitor.hpp"
#include "data_structures/sequence_collection.hpp"

namespace ham
{
	namespace graph
	{
		class ap_visitor : base_visitor
		{

		public:

			using boost_bitset_t = boost::dynamic_bitset<>;

			void previsit( string_graph<> & g ) override;
			void visit( string_graph<> & g, vertex_descriptor vd ) override;
			void postvisit( string_graph<> & g ) override;

			bool is_ap( vertex_descriptor vd )
			{
				return m_ap.test(vd);
			}

		private:

			void ap_find( string_graph<> & g, vertex_descriptor vd );

		private:

			const size_t NIL = std::numeric_limits<size_t>::max();

			boost_bitset_t m_visited;
			boost_bitset_t m_ap;

			std::vector<size_t> m_disc;
			std::vector<size_t> m_low;
			std::vector<size_t> m_parent;

			size_t m_time{0};
		};
	}
}

#endif // AP_VISITOR_HPP
