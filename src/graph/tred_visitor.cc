#include "tred_visitor.hpp"

#include <iostream>
#include <array>

#include "common/constants.hpp"

namespace ham
{
	namespace graph
	{
		void 
		tred_visitor::previsit( string_graph<> &g ) 
		{
			// mark all colors as vacant
			m_mark.resize( g.num_vertices(), vertex_color::WHITE );

			// sort vertices adjacency lists by increasing label length
			auto vertices = g.vertices();
			for( auto v_it = vertices.first; v_it != vertices.second; ++v_it )
				sort_out_edges_by_length(*v_it,g);
		}

		void 
		tred_visitor::visit( string_graph<> &g, vertex_descriptor vd )
		{
			const std::array<edge_kind,2> kinds {{ edge_kind::IN, edge_kind::OUT }};

			for( auto k : kinds )
			{
				size_t longest{0};
				std::vector<edge> v_adj = g.at(vd).out_edges(k); // TODO: explicit std::move?

				// mark adjacent edges as inplay
				for( auto &vw : v_adj ) // for each v -> w
				{
					m_mark.at(vw.target()) = vertex_color::GRAY; // mark w as inplay
					longest = std::max( longest, label_length(vw,g) );
				}

				longest += m_fuzz;

				// first scan of v adj list
				for( const auto& vw : v_adj ) // for each v -> w
				{
					vertex_descriptor w = vw.target();

					if( m_mark.at(w) == vertex_color::GRAY ) // if w is inplay
					{
						const edge& wv = mate(vw,g);
						std::vector<edge> w_adj = g.at(w).out_edges( complement(wv.kind()) );

						for( const auto& wx : w_adj ) // for each w -> x
						{
							if( label_length(vw,g) + label_length(wx,g) <= longest )
							{
								// if x is inplay, mark it as removed
								if( m_mark.at(wx.target()) == vertex_color::GRAY ) 
									m_mark.at(wx.target()) = vertex_color::BLACK;
								
								continue;
							}

							break;
						}
					}
				}

				// second scan of v adj list
				for( const auto& vw : v_adj ) // for each v -> w
				{
					const edge& wv = mate(vw,g);
					std::vector<edge> w_adj = g.at(vw.target()).out_edges( complement(wv.kind()) );

					size_t count = 1;
					for( const auto& wx : w_adj ) // for each w -> x
					{
						if( label_length(wx,g) <= m_fuzz || count++ == 1 )
						{
							// if x is inplay, mark it as removed
							if( m_mark.at(wx.target()) == vertex_color::GRAY ) 
								m_mark.at(wx.target()) = vertex_color::BLACK;
							
							continue;
						}

						break;
					}
				}

				// mark transitive edges for removal
				for( edge& vw : g.at(vd).edges() ) // for each v -> w
				{
					if( m_mark.at(vw.target()) == vertex_color::BLACK )
					{
						vw.mark_edge();
						mate(vw,g).mark_edge();
					}

					m_mark.at(vw.target()) = vertex_color::WHITE;
				}
			}
		}

		void 
		tred_visitor::postvisit( string_graph<> &g )
		{
			for( vertex_descriptor vd{0}; vd < g.num_vertices(); ++vd )
				ham::graph::delete_marked_edges(vd,g);
		}
	}
}
