#include "contrem_visitor.hpp"

#include <iostream>
#include <array>

#include "common/constants.hpp"

namespace ham
{
	namespace graph
	{
		void 
		contrem_visitor::previsit( string_graph<> & ) 
		{

		}


		void 
		contrem_visitor::visit( string_graph<> &g, vertex_descriptor vd )
		{
			bool is_included{false};
			vertex_descriptor cd; // node (sequence) which includes vd

			for( const auto &e : g.at(vd).edges() )
			{
				if( e.kind() == edge_kind::INCLUDED )
				{
					is_included = true;
					cd = e.target();
					break;
				}
			}

			if( is_included )
			{
				// update included length of the "container" vertex
				g.at(cd).embed_vertex(g.at(vd));
				
				ham::graph::clear_vertex(vd,g);
				ham::graph::delete_vertex(vd,g);
			}
		}


		void 
		contrem_visitor::postvisit( string_graph<> &g )
		{
			// delete edges marked for removal
			for( vertex_descriptor vd{0}; vd < g.num_vertices(); ++vd )
				ham::graph::delete_marked_edges(vd,g);
		}
	}
}
