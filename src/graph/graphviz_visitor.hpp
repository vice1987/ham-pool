#ifndef GRAPHVIZ_VISITOR_HPP
#define GRAPHVIZ_VISITOR_HPP

#include <iostream>
#include <fstream>

#include "data_structures/sequence_collection.hpp"

#include "base_visitor.hpp"
#include "ap_visitor.hpp"

namespace ham
{
	namespace graph
	{
		class graphviz_visitor : base_visitor
		{

		public:

			using boost_bitset_t = boost::dynamic_bitset<>;

			graphviz_visitor( const SequenceCollection &sc, const char *prefix ) :
				m_sc(sc),
				m_prefix{prefix}
			{ }

			void previsit( string_graph<> & g ) override;
			void visit( string_graph<> & g, vertex_descriptor vd ) override;
			void postvisit( string_graph<> & g ) override;

		private:

			void cc_visit( string_graph<> & g, vertex_descriptor vd );

		private:

			const SequenceCollection& m_sc;
			const std::string m_prefix;
			
			boost_bitset_t m_visited;
			std::ofstream  m_ofs;
			ap_visitor     m_ap_visit;

			size_t num_cc{0};
			
		};
	}
}

#endif // GRAPHVIZ_VISITOR_HPP
