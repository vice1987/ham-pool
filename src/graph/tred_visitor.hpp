#ifndef TRED_VISITOR_HPP
#define TRED_VISITOR_HPP

#include <iostream>
#include <fstream>

#include "base_visitor.hpp"
#include "data_structures/sequence_collection.hpp"

namespace ham
{
	namespace graph
	{
		class tred_visitor : base_visitor
		{

		public:

			tred_visitor() : m_mark() { }
			tred_visitor( size_t fuzz ) : m_fuzz{fuzz}, m_mark() { }

			void previsit( string_graph<> & g ) override;
			void visit( string_graph<> & g, vertex_descriptor vd ) override;
			void postvisit( string_graph<> & g ) override;

		private:
			
			const size_t m_fuzz {100};
			std::vector<vertex_color> m_mark;
		};
	}
}

#endif // TRED_VISITOR_HPP
