#include "graphviz_visitor.hpp"

#include <iostream>
#include <iomanip>
#include <sstream>

#include "common/constants.hpp"

namespace ham
{
	namespace graph
	{
		void 
		graphviz_visitor::previsit( string_graph<> &g ) 
		{
			m_visited.reset();
			m_visited.resize( g.num_vertices(), false );

			// retrieve articulation points
			g.accept(m_ap_visit);
		}

		void 
		graphviz_visitor::visit( string_graph<> &g, vertex_descriptor vd )
		{
			if( m_visited[vd] ) return;

			std::stringstream ss;
			ss << m_prefix << "_" << num_cc	<< ".dot";
			m_ofs.open( ss.str().c_str() );

			m_ofs << "graph string_graph_" << num_cc << "\n{\n";
			this->cc_visit(g,vd);
			m_ofs << "}\n";
			
			m_ofs.close();

			++num_cc;
		}


		void 
		graphviz_visitor::cc_visit( string_graph<> &g, vertex_descriptor vd )
		{
			m_visited[vd] = true;

			// return if vertex had been removed
			if( !g.has_vertex(vd) ) return;

			// write vertex

			const Sequence& seq = m_sc.at(g.at(vd).gid());
			size_t pool_id = static_cast<size_t>(seq.pid());
			size_t ctg_id  = static_cast<size_t>(seq.sid());

			{
				using namespace ham::colors; // svg_colors, svg_scheme

				std::string color = g.at(vd).is_junction() ? "red" : "white";

				// color it green if it is a putative mis-assembly
				if( m_ap_visit.is_ap(vd) and g.at(vd).is_linear() )
				{
					const std::array<edge_kind,2> kinds {{ edge_kind::IN, edge_kind::OUT }};
					size_t junction_nodes{0};

					for( auto k : kinds )
					{
						const edge e = g.at(vd).out_edges(k).front();
						edge_kind mate_kind = ham::graph::mate(e,g).kind();

						if( g.at(e.target()).degree(mate_kind) == 2 ) 
							++junction_nodes;
					}

					// putative mis-assembly
					if( junction_nodes == 2 ) color = "green";
				}

				std::stringstream label;
				label << pool_id << "." << ctg_id << "\n"
				      << std::setprecision(2) << g.at(vd).coverage() << "X";

				m_ofs << "\t" << vd << " [label=\"" << label.str() << "\"" 
					  << ",fillcolor=\"" << color // (pool_id < svg_colors ? svg_scheme.at(pool_id) : std::string("white"))
					  << "\",style=\"filled,solid\""
					  << "];\n";
			}

			//

			for( const auto &e : g.at(vd).edges() )
			{	
				if( m_visited[e.target()] ) continue; // edge had already been written to output
				if( !g.has_vertex(e.target()) or !e.good() ) continue; // either edge or target vertex had been deleted

				// retrieve mate edge
				const edge& mate = ham::graph::mate(e,g);

				assert( mate.target() == vd );

				if( e.kind() == edge_kind::IN or e.kind() == edge_kind::OUT )
				{
					// print undirected edge
					m_ofs << "\t" 
						  << mate.target() << "--" << e.target() << " [color=\"black\",dir=\"both\","
						  << "taillabel=\"" << ham::graph::label_length(mate,g) << "\", headlabel=\"" << ham::graph::label_length(e,g) << "\","
						  << "arrowtail=\"" << (e.kind() == edge_kind::IN ? "empty" : "invempty") << "\","
						  << "arrowhead=\"" << (mate.kind() == edge_kind::IN ? "empty" : "invempty") 
						  << "\"];\n";
				}
				else
				{
					// print undirected edge
					m_ofs << "\t" 
						  << mate.target() << "--" << e.target() << " [color=\"red\",dir=\"both\","
						  //<< "taillabel=\"" << t_len << "\", headlabel=\"" << s_len << "\","
						  << "arrowtail=\"" << (e.kind() == edge_kind::CONTAINS ? "normal" : "none") << "\","
						  << "arrowhead=\"" << (e.kind() == edge_kind::CONTAINS ? "none" : "normal") 
						  << "\"];\n";
				}
			}

			for( auto &e : g.at(vd).edges() )
			{
				if( e.good() and g.has_vertex(e.target()) and !m_visited[e.target()] )
					this->cc_visit(g,e.target());
			}
		}

		void 
		graphviz_visitor::postvisit( string_graph<> & )
		{
			// nothing to do
		}
	}
}
