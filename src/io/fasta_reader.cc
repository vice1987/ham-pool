#include "fasta_reader.hpp"

#include <sstream>
#include <cctype>
#include <assert.h>

#include "common/constants.hpp"


const std::string& FastaReader::filename() const
{
	return m_filename;
}


const std::ifstream& FastaReader::ifs() const
{
	return m_ifs;
}


void FastaReader::open( const std::string &filename )
{
    this->open( filename.c_str() );
}

void FastaReader::open( const char *filename )
{
	m_filename = filename;
    m_ifs.open(filename);

    if(!m_ifs)
    {
        std::stringstream ss;
		ss << "open: could not open fasta file '" << filename << "'";
		throw FastaReaderException(ss.str());
    }
}


void FastaReader::close()
{
    m_filename = "";
	m_ifs.close();
}


bool FastaReader::next_sequence( Sequence& seq )
{
    bool success;
    seq.clear();

    success = this->get_next_id(seq);
    if(success) success = this->get_next_sequence(seq);

    return success;
}


bool FastaReader::get_next_id( Sequence& seq )
{
    int c;
    size_t pos;
    std::string id;
    std::string line;

	c = m_ifs.peek();

	while( m_ifs.good() and (c == ' ' or c == '\n' or c == '\r') )
	{
		m_ifs.ignore(1);
		if( m_ifs.good() ) c = m_ifs.peek();
	}

	if( m_ifs.good() and c != '>' )
	{
		std::stringstream ss;
		ss << "get_next_id: character '" << c << "' found, '>' was expected";
		throw FastaReaderException(ss.str());
	}

	// end of file reached
	if( m_ifs.eof() ) return false;

	std::getline( m_ifs, line ); // get identifier line
	id = line.substr(1); // remove beginning '>' character

    // discard everything after the first white space
    pos = id.find(' '); if( pos != std::string::npos ) id = id.substr(0,pos);
    pos = id.find('\t'); if( pos != std::string::npos ) id = id.substr(0,pos);

	// if the identifier is empty, throw an exception
    if( id == "" ) throw FastaReaderException("get_next_id: empty identifier");

    seq.set_str_id(id);

	return true;
}


void FastaReader::rewind()
{
	m_ifs.seekg(0);
}


bool FastaReader::get_next_sequence( Sequence& seq )
{
	assert( seq.size() == 0 );

	int c;

    c = m_ifs.peek();

	while( m_ifs.good() and (c == ' ' or c == '\n' or c == '\r') )
	{
		m_ifs.ignore(1);
		if( m_ifs.good() ) c = m_ifs.peek();
	}

	while( m_ifs.good() and c != '>' )
	{
		c = m_ifs.get();

		if( c != '>' and c != ' ' and c != '\n' and c != '\r' and !m_ifs.eof() )
		{
            if( not isalpha(c) ) throw FastaReaderException("get_next_sequence: incorrect FASTA sequence");
            seq.push_back( ham::constants::int2base[c] ); //seq.push_back( toupper(c) );
		}
	}

    if( m_ifs.good() ) m_ifs.unget();

    if( seq.size() == 0 ) throw FastaReaderException("get_next_sequence: found empty sequence");

	return true;
}
