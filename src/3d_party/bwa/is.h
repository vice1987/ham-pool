/*
 * File:   is.h
 * Author: riccardo
 *
 * Created on January 10, 2014, 6:58 PM
 */

#ifndef IS_H
#define	IS_H

#ifdef	__cplusplus
extern "C"
{
#endif


typedef unsigned char ubyte_t;

int is_sa(const ubyte_t *T, int *SA, int n);
int is_bwt(ubyte_t *T, int n);


#ifdef	__cplusplus
}
#endif

#endif	/* IS_H */

