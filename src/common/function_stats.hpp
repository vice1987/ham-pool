/*
 * File:   function_stats.hpp
 * Author: Riccardo Vicedomini
 *
 * Created on November 14, 2013, 5:16 PM
 */

#ifndef FUNCTION_STATS_HPP
#define	FUNCTION_STATS_HPP

#include <ctime>
#include <string>


class FunctionStats
{

private:

	std::string m_fname;
	time_t m_start;

public:

	FunctionStats();
	FunctionStats( const std::string &fname );
	~FunctionStats();

};


#endif	/* FUNCTION_STATS_HPP */
