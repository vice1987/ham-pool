/*
 * File:   ham_index.hpp
 * Author: riccardo
 *
 * Created on January 10, 2014, 5:48 PM
 */

#ifndef HAM_INDEX_HPP
#define	HAM_INDEX_HPP

#include <string>

#include "data_structures/sequence_collection.hpp"

void ham_idx_build( const SequenceCollection &sc, const std::string &idx_prefix );

void ham_idx_load( void **ham_index, const std::string &idx_prefix );

void ham_idx_destroy( void *ham_index );

#endif	/* HAM_INDEX_HPP */

