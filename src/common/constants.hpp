#ifndef NUCLEOTIDE_HPP
#define NUCLEOTIDE_HPP

#include <vector>
#include <string>

#include "common/types.hpp"

namespace ham
{
	namespace constants
	{
		// WARNING: do not change the following constants
		// A = 0, C = 1, G = 2, T = 3, N = 4
		const base_t BASE_A = 0;
		const base_t BASE_C = 1;
		const base_t BASE_G = 2;
		const base_t BASE_T = 3;
		const base_t BASE_N = 4;

		const char * const base2char = "ACGTN";

		// lookup table for int to base_t conversion,
		// BASE_N is assigned for unknown bases
		const base_t int2base[256] =
		{
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
		};

		// reverse complement lookup array for BASE_X constants
		const base_t base_rc[5] = { 3, 2, 1, 0, 4 };
	}

	namespace colors
	{
		const std::vector<std::string> svg_scheme {
			"aliceblue", "antiquewhite", "aqua", "aquamarine", "azure",
			"beige", "bisque", "black", "blanchedalmond", "blue",
			"blueviolet", "brown", "burlywood", "cadetblue", "chartreuse",
			"chocolate", "coral", "cornflowerblue", "cornsilk", "crimson",
			"cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray",
			"darkgreen", "darkgrey", "darkkhaki", "darkmagenta", "darkolivegreen",
			"darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen",
			"darkslateblue", "darkslategray", "darkslategrey", "darkturquoise", "darkviolet",
			"deeppink", "deepskyblue", "dimgray", "dimgrey", "dodgerblue",
			"firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro",
			"ghostwhite", "gold", "goldenrod", "gray", "grey",
			"green", "greenyellow", "honeydew", "hotpink", "indianred",
			"indigo", "ivory", "khaki", "lavender", "lavenderblush",
			"lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan",
			"lightgoldenrodyellow", "lightgray", "lightgreen", "lightgrey", "lightpink",
			"lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightslategrey",
			"lightsteelblue", "lightyellow", "lime", "limegreen", "linen",
			"magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid",
			"mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise",
			"mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin",
			"navajowhite", "navy", "oldlace", "olive", "olivedrab",
			"orange", "orangered", "orchid", "palegoldenrod", "palegreen",
			"paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru",
			"pink", "plum", "powderblue", "purple", "red",
			"rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown",
			"seagreen", "seashell", "sienna", "silver", "skyblue",
			"slateblue", "slategray", "slategrey", "snow", "springgreen",
			"steelblue", "tan", "teal", "thistle", "tomato",
			"turquoise", "violet", "wheat", "whitesmoke",
			"yellow", "yellowgreen"
		};

		const size_t svg_colors { svg_scheme.size() };
	}
}

#endif // NUCLEOTIDE_HPP





