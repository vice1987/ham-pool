#include "utils.hpp"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>

#include "3d_party/city/city.h"

char * getPathBaseName( char *path )
{
    char *ptr = strrchr(path, '/');
    return ptr ? ptr + 1 : path;
}


std::string getPathBaseName( const std::string path )
{
    size_t found = path.rfind ( '/' );
    return (found != std::string::npos) ? path.substr(found+1) : std::string( path.c_str() );
}


std::string getBaseFileName( std::string filename )
{
    size_t found = filename.rfind('.');
    return filename.substr(0,found);
}


void load_filenames(const std::string &input_file, std::vector<std::string> &names)
{
	std::string line; // read line
	std::ifstream ifs( input_file.c_str() );

	while( ifs.good() )
	{
		getline(ifs,line);
		if( line != "" ) names.push_back(line);
	}

	ifs.close();
}


std::string format_time( time_t seconds )
{
	std::stringstream out;
	long h = seconds / 3600;
	long m = (seconds % 3600) / 60;
	long s = (seconds % 3600) % 60;

	if( h > 0 ) out << h << "h";
	if( m > 0 ) out << m << "m";
	out << s << "s";

	return out.str();
}


void mem_usage( double& vm_usage, double& resident_set )
{
	vm_usage     = 0.0;
	resident_set = 0.0;

	std::ifstream stat_stream( "/proc/self/stat", std::ios_base::in );

	// dummy vars for leading entries in stat that we don't care about
	std::string pid, comm, state, ppid, pgrp, session, tty_nr;
	std::string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	std::string utime, stime, cutime, cstime, priority, nice;
	std::string O, itrealvalue, starttime;

	// the two needed fields
	unsigned long vsize;
	long rss;

	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	>> utime >> stime >> cutime >> cstime >> priority >> nice
	>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	vm_usage     = vsize / 1024.0;
	resident_set = rss * page_size_kb;
}


std::pair<std::string,std::string> mem_usage()
{
    double vm_usage, rss_usage;
	mem_usage(vm_usage, rss_usage);

    std::string vm_suff = "KB", rss_suff = "KB";

	if( vm_usage > 1024 ) // possibly compute memory usage in MB or GB
	{
		vm_usage = vm_usage / 1024;
		if( vm_usage <= 1024 ) vm_suff = "MB";
		if( vm_usage > 1024 ){ vm_usage = vm_usage / 1024; vm_suff = "GB"; }
	}

	if( rss_usage > 1024 ) // possibly compute memory usage in MB or GB
	{
		rss_usage = rss_usage / 1024;
		if( rss_usage <= 1024 ) rss_suff = "MB";
		if( rss_usage > 1024 ){ rss_usage = rss_usage / 1024; rss_suff = "GB"; }
	}

    std::stringstream vm_ss, rss_ss;
    vm_ss << vm_usage << vm_suff;
    rss_ss << rss_usage << rss_suff;

    return std::make_pair( vm_ss.str(), rss_ss.str() );
}


size_t city_hash_64( const void *p, size_t n )
{
	return CityHash64(static_cast<const char*>(p), n);
}
