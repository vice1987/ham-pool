#ifndef PRNG_SINGLETON_HPP
#define PRNG_SINGLETON_HPP

#include <memory>
#include <random>

#include "common/boost_includes.hpp"
#include "common/constants.hpp"

class PrngSingleton
{

private:

	//static std::unique_ptr<PrngSingleton> s_instance;
	PrngSingleton() : m_rd(), m_gen(m_rd()), m_base_dist(0,3) { }

public:

	static PrngSingleton& instance() 
	{
		static PrngSingleton s_instance{};
		return s_instance;
	}

	base_t rand_base()
	{
		boost::mutex::scoped_lock l(m_base_mutex);
		return static_cast<base_t>(m_base_dist(m_gen)); 
	}

private:

	std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_int_distribution<> m_base_dist;

    boost::mutex m_base_mutex;
};

#endif
