#include "algorithms.hpp"

#include <cmath>

#include "common/prng_singleton.hpp"


void reverse_complement( uint8_t *seq, size_t len )
{
	size_t j = 0, i = len;

	while( j+1 <= i )
	{
		i--;

		if( j+1 < i )
		{
			std::swap(seq[j],seq[i]);
			seq[j] = ham::constants::base_rc[seq[j]];
			seq[i] = ham::constants::base_rc[seq[i]];
		}
		else // j+1 == i
		{
			seq[j] = ham::constants::base_rc[seq[j]];
		}

		j++;
	}
}

// note: [start,end) is a left-open interval
void reverse_coordinates( int32_t &start, int32_t &end, int32_t len )
{
	int32_t old_start = start;
	start = len-end;
	end = start + (end-old_start);
}


size_t kmer_load( Kmer& kmer, const Sequence& seq, size_t i )
{
    uint32_t k = Kmer::length();
    assert( i+k <= seq.length() );

    uint64_t base_code;
    
    size_t last_n_pos = std::string::npos;
    uint64_t k_data = 0;

    for( size_t j=0; j < k; ++j )
    {
        base_code = seq[i+j];
        k_data = (k_data << 2) | base_code;

        if( base_code == ham::constants::BASE_N ) last_n_pos = i+j;
    }

    kmer.set_data(k_data);

    return last_n_pos;
}


void kmer_load_rand( Kmer& kmer, const Sequence& seq, size_t i )
{
	using ham::constants::BASE_N;

    uint32_t k = Kmer::length();
    assert( i+k <= seq.length() );

    uint64_t base_code = 0, k_data = 0;

    for( size_t j=0; j < k; ++j )
    {
        base_code = seq.at(i+j);
        k_data = (k_data << 2) | ( base_code != BASE_N ? base_code : PrngSingleton::instance().rand_base() );
    }

    kmer.set_data(k_data);
}


Kmer& kmer_shift_append( Kmer& kmer, const base_t &b )
{
    assert( b != ham::constants::BASE_N );
    return kmer.shift_append(b);
}

Kmer& kmer_shift_prepend( Kmer& kmer, const base_t &b )
{
	assert( b != ham::constants::BASE_N );
	return kmer.shift_prepend(b);
}


Kmer& kmer_shift_append_rand( Kmer& kmer, const base_t &b )
{
	using ham::constants::BASE_N;
    return kmer.shift_append( b != BASE_N ? b : PrngSingleton::instance().rand_base() );
}

Kmer& kmer_shift_prepend_rand( Kmer& kmer, const base_t &b )
{
	using ham::constants::BASE_N;
	return kmer.shift_prepend( b != BASE_N ? b : PrngSingleton::instance().rand_base() );
}



void kmer_collection_load( KmerCollection &kc, const Sequence& seq )
{
    uint32_t k = Kmer::length();
    size_t seq_size = seq.size();

    if( seq_size < k ) return;

    Kmer kmer;
    size_t last_n = std::string::npos;
    size_t last_i = seq_size - k; // last position where it is possible to load a k-mer

    size_t i = 0;      // position of first base to load
    size_t j = i+k-1;  // position of last base to load
    bool shift = false;  // whether it is possible to shift_append last k-mer
    bool is_next_base_n; // whether base following k-mer at position i is unknown

    while( i <= last_i )
    {
        if( !shift ) // add k-mer at position i if it does not contain unknown bases
        {
            last_n = kmer_load(kmer,seq,i);
            if( last_n == std::string::npos ) kc.add_kmer(kmer);
        }
        else // add shifted k-mer to the collection
        {
            kc.add_kmer( kmer_shift_append(kmer,seq[j]) );
        }

        if( j+1 >= seq_size ) break;

        is_next_base_n = seq[j+1] == ham::constants::BASE_N;
        last_n = is_next_base_n ? j+1 : last_n;

        shift = (last_n == std::string::npos);
        i = shift ? i+1 : last_n+1;
        j = i+k-1;
    }
}


void kmer_collection_load( KmerCollection &kc, const SequenceCollection& sc )
{
	size_t sc_size = sc.size();
	for( size_t i=0; i < sc_size; ++i ) kmer_collection_load( kc, sc[i] );
}


void overlap_load_from_file( std::istream& is, const SequenceCollection& sc, std::list<Overlap>& oll )
{
	Overlap o;
	int32_t q_pid, q_sid, r_pid, r_sid;
	char rev;

	while( is >> q_pid >> q_sid >> r_pid >> r_sid >> rev >> o.q_beg >> o.q_end >> o.r_beg >> o.r_end >> o.ident )
	{
		o.qid = sc.gid(q_pid,q_sid);
		o.rid = sc.gid(r_pid,r_sid);
		o.is_rev = rev == '-';

		oll.push_back(o);
	}
}


// TODO: check if both ends of an overlap are below max_tail threshold
ham::graph::string_graph<>
build_string_graph( std::list< Overlap >& overlaps, const SequenceCollection& sc, size_t max_tail, size_t min_len )
{
    using namespace ham::graph;

    string_graph<> graph{};

    for( const auto & o : overlaps )
    {
        ham::range_t query_range(o.q_beg,o.q_end);
        ham::range_t ref_range(o.r_beg,o.r_end);

        assert( query_range.valid() && ref_range.valid() );

        // skip overlap if it is too short
        if( std::min( query_range.length(), ref_range.length() ) < min_len ) continue;

        size_t query_len = sc.at( o.qid ).length();
        size_t ref_len   = sc.at( o.rid ).length();

        // possibly add vertices and retrieve descriptors
        auto query_vd = graph.add_vertex( o.qid, query_len );
        auto ref_vd   = graph.add_vertex( o.rid, ref_len );

        // set edge direction
        edge_dir dir = o.is_rev ? edge_dir::REVERSE : edge_dir::FORWARD;

        // set edge arrow-head kind 
        edge_kind ekind, mkind;
        
        if( query_range.is_containment(query_len,max_tail) )
        {
            ekind = edge_kind::INCLUDED;
            mkind = edge_kind::CONTAINS;
        }
        else if( ref_range.is_containment(ref_len,max_tail) )
        {
            ekind = edge_kind::CONTAINS;
            mkind = edge_kind::INCLUDED;
        }
        else
        {
            ekind = query_range.ltail() < max_tail ? edge_kind::IN : edge_kind::OUT;
            mkind = ref_range.ltail() < max_tail ? edge_kind::IN : edge_kind::OUT;
        }

        edge_descriptor qr_edge = graph.add_edge( query_vd, ref_vd, ref_range, dir, ekind );
        edge_descriptor qr_mate = graph.add_edge( ref_vd, query_vd, query_range, dir, mkind );

        bind_mates( qr_edge, qr_mate, graph );

        /*
        // debug code

        edge_descriptor aaa = mate(qr_edge,graph);
        edge_descriptor bbb = mate(qr_mate,graph);

        std::cerr << "\n-----------------------------\n";
        std::cerr << "overlap " << query_pool_id << "." << query_ctg_id << ";" << ref_pool_id << "." << ref_ctg_id << " found.\n";
        std::cerr << "added edge: (" << qr_edge.first << "," << qr_edge.second << ")->" << target(qr_edge,graph) << "\tmate: (" << aaa.first << "," << aaa.second << ")->" << target(aaa,graph) << "\n";
        std::cerr << "added edge: (" << qr_mate.first << "," << qr_mate.second << ")->" << target(qr_mate,graph) << "\tmate: (" << bbb.first << "," << bbb.second << ")->" << target(bbb,graph);

        if( target(qr_mate,graph) != qr_edge.first or target(qr_edge,graph) != qr_mate.first ) std::cerr << "\nerror, inconsistent edges!!!!";

        */
    }

    return graph;
}


/*bool compute_fingerprint( const Sequence& seq, const KmerCollection &kc,
        Fingerprint &fp, const Options &opt )
{
    size_t k = Kmer::length();
    size_t seq_size = seq.size();

    fp.clear();

    if( seq_size < k ) return false;

    Kmer kmer;
    size_t last_n = 0;				// last position where a N has been found, std::string::npos otherwise
    size_t last_i = seq_size - k;	// last position where it is possible to load a k-mer

    size_t i = 0;     // position of first base to load
    size_t j = i+k-1; // position of last base to load

    int32_t gap;
	int32_t f_i = 0;

    // first k-mer
    while( last_n != std::string::npos || f_i < 2 || f_i > opt.m_max_freq )
    {
        if( i > last_i ) return false;

        last_n = kmer_load(kmer,seq,i);
		f_i = kc.get_freq(kmer);

		if( last_n != std::string::npos )
		{
			i = last_n+1; // can be improved checking whether base at (i+k) is N (might not be worth)
			j = i+k-1;
		}
		else if( f_i < 2 || f_i > opt.m_max_freq )
		{
			i++;
			j++;
		}
    }

	gap = static_cast<int32_t>(i);
    fp.push_back(kmer,gap);

    size_t prev_i = j; // last kmer-covered position
    int32_t prev_f = kc.get_freq(kmer); // last added kmer frequency

    i++;
    j++;

    bool shift = false;
    bool is_next_base_n;

    while( i <= last_i )
    {
        if( !shift ) // add k-mer at position i if it does not contain unknown bases
        {
            last_n = kmer_load(kmer,seq,i);

            if( last_n == std::string::npos ) // if it is a valid k-mer (N's not found)
            {
                // get k-mer's frequency and distance from the previous k-mer added
                f_i = kc.get_freq(kmer);
                gap = static_cast<int32_t>(i-prev_i)-1;

                // if requirements are met add the k-mer to the fingerprint
                if( (gap > opt.m_max_gap || std::abs(f_i-prev_f) > opt.m_fdiff)
						&& f_i >= 2 && f_i <= opt.m_max_freq )
                {
                    prev_i = j;
                    prev_f = f_i;
                    fp.push_back(kmer,gap);
                }
            }
        }
        else // add shifted k-mer to the collection
        {
            kmer_shift_append(kmer,seq[j]);

			// get k-mer's frequency and distance from the previous k-mer added
            f_i = kc.get_freq(kmer);
            gap = static_cast<int32_t>(i-prev_i)-1;

            if( (gap > opt.m_max_gap || std::abs(f_i-prev_f) > opt.m_fdiff)
					&& f_i >= 2 && f_i <= opt.m_max_freq )
            {
                prev_i = i+k-1;
                prev_f = f_i;
                fp.push_back(kmer,gap);
            }
        }

        is_next_base_n = seq[j+1] == ham::constants::BASE_N;
        last_n = is_next_base_n ? j+1 : last_n;

        shift = (last_n == std::string::npos);
        i = shift ? i+1 : last_n+1;
        j = i+k-1;
    }

	// add last gap
	gap = static_cast<int32_t>(seq_size-prev_i)-1;
	fp.push_back(gap);

    return true;
}*/
