#include "function_stats.hpp"

#include <iostream>

#include "common/utils.hpp"


FunctionStats::FunctionStats() :
	m_fname(""), m_start(time(NULL))
{}


FunctionStats::FunctionStats( const std::string &fname ) :
	m_fname(fname),
	m_start(time(NULL))
{}


FunctionStats::~FunctionStats()
{
	std::pair<std::string,std::string> mem = mem_usage();

	std::cerr << "[" << m_fname << "] time = "
			<< format_time( time(NULL)-m_start ) << " "
			<< "vm = " << mem.first << " rss = " << mem.second << std::endl;
}
