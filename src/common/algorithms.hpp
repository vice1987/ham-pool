/*
 * File:   algorithms.hpp
 * Author: riccardo
 *
 * Created on November 29, 2013, 12:08 PM
 */

#ifndef ALGORITHMS_HPP
#define	ALGORITHMS_HPP

#include <list>
#include <iostream>

#include "common/constants.hpp"
#include "data_structures/kmer.hpp"
#include "data_structures/kmer_collection.hpp"
#include "data_structures/overlap.hpp"
#include "data_structures/sequence.hpp"
#include "data_structures/sequence_collection.hpp"
#include "data_structures/fingerprint.hpp"
#include "graph/string_graph.hpp"
#include "options/options.hpp"


/*
 * Reverse complement sequence data
 */
void reverse_complement( uint8_t *seq, size_t len );
void reverse_coordinates( int32_t &start, int32_t &end, int32_t len );


/*
 * Load a k-mer from a sequence at position i and returns the
 * position where last N occurred (std::string::npos otherwise).
 * It is assumed i is a valid position to extract a full k-mer.
 */
size_t kmer_load( Kmer& kmer, const Sequence& seq, size_t i );

/*
 * Load a k-mer from a sequence at position i, assuming it is a valid position.
 * Unknown bases are replaced by a random base in {A,C,T,G}
 */
void kmer_load_rand( Kmer& kmer, const Sequence& seq, size_t i );

/*
 * Shift-left/right a k-mer and append/prepend a new base. 
 * The base is expected to be different than N.
 * Returns a reference to the k-mer.
 */
Kmer& kmer_shift_append( Kmer& kmer, const base_t &base );
Kmer& kmer_shift_prepend( Kmer& kmer, const base_t &base );

/*
 * Shift-left/right a k-mer and append/prepend a new base.
 * If the base is unknown it is replaced by a random base in {A,C,T,G}
 * Returns a reference to the k-mer.
 */
Kmer& kmer_shift_append_rand( Kmer& kmer, const base_t &base );
Kmer& kmer_shift_prepend_rand( Kmer& kmer, const base_t &base );


/*
 * Adds each k-mer of a sequence to a k-mer collection.
 * k-mers containing uncertain bases are skipped.
 */
void kmer_collection_load( KmerCollection &kc, const Sequence& seq );


/*
 * Adds each k-mer of a sequence collection to a k-mer collection.
 * k-mers containing uncertain bases are skipped.
 */
void kmer_collection_load( KmerCollection &kc, const SequenceCollection& sc );


/*
 * Compute fingerprint of a sequence.
 * Returns whether it has been successfully built.
 */
bool compute_fingerprint2( const Sequence& seq, const KmerCollection &kc,
        Fingerprint &fp, const Options &opt );

//bool compute_fingerprint( const Sequence& seq, const KmerCollection &kc,
//        Fingerprint &fp, const Options &opt );


void overlap_load_from_file( std::istream& is, const SequenceCollection& sc, std::list<Overlap>& oll );

// load a graph from an input stream associated to an overlap-file
ham::graph::string_graph<>
build_string_graph( std::list< Overlap >& overlaps, const SequenceCollection& sc, size_t max_tail, size_t min_len );

#endif	/* ALGORITHMS_HPP */
