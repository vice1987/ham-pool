/*
 * File:   utils.hpp
 * Author: riccardo
 *
 * Created on February 20, 2013, 12:42 PM
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <vector>
#include <string>


char* getPathBaseName( char *path );

std::string getPathBaseName( const std::string path );

std::string getBaseFileName( std::string filename );

void load_filenames(const std::string &input_file, std::vector<std::string> &names);

std::string format_time( time_t seconds );

void mem_usage( double& vm_usage, double& resident_set );

std::pair<std::string,std::string> mem_usage();

size_t city_hash_64( const void *p, size_t n );


#endif	/* UTILS_HPP */

