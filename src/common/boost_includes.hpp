/*
 * File:   boost_includes.hpp
 * Author: riccardo
 *
 * Created on December 6, 2013, 4:57 PM
 */

#ifndef BOOST_INCLUDES_HPP
#define	BOOST_INCLUDES_HPP

// disable compiler warnings
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
#pragma clang diagnostic ignored "-Wconditional-uninitialized"
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wextra-semi"
#pragma clang diagnostic ignored "-Wglobal-constructors"
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#pragma clang diagnostic ignored "-Wold-style-cast"
#pragma clang diagnostic ignored "-Wshadow"
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wundef"
#endif

#include <boost/program_options.hpp>
namespace bpo = boost::program_options;
#include <boost/filesystem.hpp>
namespace boost_fs = boost::filesystem;
#include <boost/thread.hpp>
#include <boost/detail/endian.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

// graphs' edges and vertices properties
namespace boost
{
	enum edge_kind_t { edge_kind };
	enum vertex_kind_t { vertex_kind };
	
	BOOST_INSTALL_PROPERTY(edge, kind);
	BOOST_INSTALL_PROPERTY(vertex, kind);
}

// restore compiler warnings
#if defined(__clang__)
#pragma clang diagnostic pop
#endif

#endif	/* BOOST_INCLUDES_HPP */

