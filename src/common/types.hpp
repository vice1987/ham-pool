/*
 * File:   types.hpp
 * Author: riccardo
 *
 * Created on January 7, 2014, 3:01 PM
 */

#ifndef TYPES_HPP
#define	TYPES_HPP

#include <cstdint>
#include <iostream>

// TODO: move this typedef in ham namespace
typedef uint8_t base_t;

struct KmerHit
{
	int32_t kid;     // kmer id (e.g., index in a fingerprint)
	int32_t rid;     // reference id
	int32_t beg;     // end = beg+k-1
	bool    is_rev;  // whether it is a fwd/rc hit
	bool    is_mult; // whether it has other hits against the same contig (rid)

	KmerHit( int32_t k = -1, int32_t r = -1, int32_t b = -1, bool rev = false, bool mult = false )
		: kid(k), rid(r), beg(b), is_rev(rev), is_mult(mult) {}

	bool operator < (const KmerHit &kh) const
	{
		return rid < kh.rid || (rid == kh.rid && kid < kh.kid);
	}
};

#endif	/* TYPES_HPP */

