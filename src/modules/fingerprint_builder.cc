#include "fingerprint_builder.hpp"

#include <random>
#include <iostream>

#include "3d_party/bwa/bwamem.h"
#include "common/algorithms.hpp"
#include "common/boost_includes.hpp"

FingerprintBuilder::FingerprintBuilder(
    const Options &opt,
    const SequenceCollection &sc,
    const KmerCollection &kc,
    FingerprintVector &fingerprints )
    :
    m_opt(opt),
    m_sc(sc),
    m_kc(kc),
    m_fingerprints(fingerprints)
{
    m_fingerprints.resize(sc.size());
}


void FingerprintBuilder::fingerprints_build()
{
    boost::thread_group threads;
    size_t n_threads = static_cast<size_t>(m_opt.m_threads_num);

    for ( size_t tid = 0; tid < n_threads; ++tid )
        threads.create_thread( boost::bind( &this->fingerprints_build_thread, this, tid ) );

    threads.join_all();
}


void FingerprintBuilder::fingerprints_build_thread( FingerprintBuilder *fpb, size_t tid )
{
    std::stringstream ss; ss << "[thread " << tid << " launched]" << std::endl;
    std::cerr << ss.str();

    size_t sequences = (fpb->m_fingerprints).size();
    size_t n_threads = static_cast<size_t>((fpb->m_opt).m_threads_num);

    for ( size_t i = tid; i < sequences; i += n_threads )
    {
        const Sequence &seq = (fpb->m_sc).at(i);
        Fingerprint &fp = (fpb->m_fingerprints).at(i);

        FingerprintBuilder::compute_fingerprint( seq, fpb->m_kc, fp, fpb->m_opt );
    }
}


bool FingerprintBuilder::compute_fingerprint( const Sequence& seq, const KmerCollection& kc, Fingerprint& fp, const Options& opt )
{
    size_t k = Kmer::length();
    size_t seq_size = seq.size();

    fp.clear(); // this should not be needed, the fingerprint should already be empty

    // large-enough sequences are expected. anyway skip very short sequences
    if( seq_size < k ) return false;
    if( seq_size < static_cast<size_t>(opt.m_max_gap) + 2*k ) return false;

    Kmer kmer;
    size_t last_i { seq_size-k };  // last position where it is possible to load a k-mer

    bool is_lf {false};
    bool is_prev_lf {false};

    std::vector<size_t> lfr;
    std::vector<size_t> fpi;

    size_t hf_kmers = 0;
    size_t lf_kmers = 0;

    size_t z = 0;

    //std::cerr << "seq_size = " << seq.size() << " max-gap = " << opt.m_max_gap << std::endl;

    // FIRST PASS

    // find k-mers which are boundaries of low-frequency regions
    for( size_t i=0; i <= last_i; ++i, is_prev_lf = is_lf )
    {
        size_t j = i+k-1;

        if( i == 0 ) // first k-mer
        {
            kmer_load_rand(kmer,seq,i);
            is_lf = kc.get_freq(kmer) > opt.m_max_freq ? false : true;
        }
        else // shift previous k-mer
        {
            kmer_shift_append_rand(kmer,seq[j]);
            is_lf = kc.get_freq(kmer) > opt.m_max_freq ? false : true;
        }

        // update HF/LF k-mer counters
        is_lf ? lf_kmers++ : hf_kmers++;

        // beginning of a new "low-frequency" region
        if( !is_prev_lf && is_lf != is_prev_lf )
        {
            // remove previous k-mer if the max-gap constraint remains valid
            if( z >= 2 && i-lfr[z-2] <= static_cast<size_t>(opt.m_max_gap) + k ){ lfr.pop_back(); --z; }
            lfr.push_back(i); 
            ++z;
        }

        // end of a "low-frequency" region
        if( is_prev_lf && is_lf != is_prev_lf )
        {
            // skip duplicate
            if( z >= 1 && i == lfr[z-1]+1 ) continue;
            // remove previous k-mer if the max-gap constraint remains valid
            if( z >= 2 && i-lfr[z-2] <= static_cast<size_t>(opt.m_max_gap) + k+1 ){ lfr.pop_back(); --z; }

            lfr.push_back(i-1); // i-1 > 0 is assured. a previous low-frequency k-mer (index) must have been inserted
            ++z;
        }
    }
    // (possibly) add last low-frequency k-mer (if not duplicate)
    if( is_prev_lf && (z < 1 || last_i != lfr[z-1]) )
    {
        if( z >= 2 && last_i-lfr[z-2] <= static_cast<size_t>(opt.m_max_gap) + k ){ lfr.pop_back(); --z; }
        lfr.push_back(last_i);
        ++z;
    }

    // skip repetitive sequences (i.e., with more high-frequency k-mers than low-frequency`s)
    //if( lf_kmers < hf_kmers ) return false;

    // SECOND PASS

    // take care of first uncovered region
    if( not lfr.empty() && lfr[0] > static_cast<size_t>(opt.m_max_gap) )
    {
        for( size_t w=0; w < lfr[0]; w += k + static_cast<size_t>(opt.m_max_gap) )
            fpi.push_back(w);
    }

    // take care of middle uncovered regions
    for( size_t i=0; i+1 < lfr.size(); i++ )
    {
        size_t curr_idx { lfr[i] };
        size_t next_idx { lfr[i+1] };
        int32_t gap { static_cast<int32_t>(next_idx-curr_idx) - static_cast<int32_t>(k) };

        fpi.push_back(curr_idx);

        // possibly pick k-mers between curr_idx and next_idx in order to fulfill "max-gap" constraint
        if( gap > opt.m_max_gap )
        {
            for( size_t w = curr_idx + k + static_cast<size_t>(opt.m_max_gap); w < next_idx; w += k + static_cast<size_t>(opt.m_max_gap) ) 
                fpi.push_back(w);
        }
    }

    // (possibly) add last k-mer and take care of last uncovered region
    if( not lfr.empty() ) 
    {
        size_t last_idx { lfr.back() };

        fpi.push_back(last_idx);

        if( seq_size-last_idx-k > static_cast<size_t>(opt.m_max_gap) ) // seq_size-last_idx-k > 0 should hold
        {
            //size_t last_idx = fpi.back();
            size_t gap = seq_size-last_idx-k;

            if( gap > static_cast<size_t>(opt.m_max_gap) )
            {
                for( size_t w = last_idx + k + static_cast<size_t>(opt.m_max_gap); w <= last_i; w += k + static_cast<size_t>(opt.m_max_gap) ) 
                    fpi.push_back(w);
            }
        }
    }
    else // take care of the whole contig if there were not low-frequency regions
    {
        for( size_t w = static_cast<size_t>(opt.m_max_gap); w <= last_i; w += k + static_cast<size_t>(opt.m_max_gap) ) 
            fpi.push_back(w);
    }
    

    // add k-mers to fingerprint
    for( size_t i=0; i < fpi.size(); i++ )
    {
        kmer_load_rand(kmer,seq,fpi[i]);
        fp.push_back(kmer);
        fp.push_back(fpi[i]);
    }

    //std::cerr << "fp_size = " << fpi.size() << std::endl;

    return lf_kmers >= hf_kmers;
}
