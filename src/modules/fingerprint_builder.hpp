/*
 * File:   fingerprint_aligner.hpp
 * Author: riccardo
 *
 * Created on January 9, 2014, 6:35 PM
 */

#ifndef FINGERPRINT_BUILDER_HPP
#define	FINGERPRINT_BUILDER_HPP

#include <vector>

#include "common/boost_includes.hpp"
#include "common/constants.hpp"
#include "data_structures/fingerprint.hpp"
#include "data_structures/kmer_collection.hpp"
#include "data_structures/sequence_collection.hpp"
#include "options/options.hpp"

class FingerprintBuilder
{

public:

	typedef std::vector<Fingerprint> FingerprintVector;

public:

	FingerprintBuilder(	
		const Options &opt,
		const SequenceCollection &sc,
		const KmerCollection &kc,
		FingerprintVector &fingerprints 
	);

	void fingerprints_build();

private:

	static void fingerprints_build_thread( 
		FingerprintBuilder *fp_aligner, 
		size_t tid 
	);

	static bool compute_fingerprint( 
		const Sequence& seq, 
		const KmerCollection& kc,
		Fingerprint& fp, 
		const Options& opt 
	);

private:

	// input data
	const Options &m_opt;
	const SequenceCollection &m_sc;
	const KmerCollection &m_kc;

	// output data reference
	FingerprintVector &m_fingerprints;

};

#endif	/* FINGERPRINT_BUILDER_HPP */

