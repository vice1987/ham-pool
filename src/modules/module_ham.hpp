#ifndef MODULE_HAM_HPP
#define MODULE_HAM_HPP

#include <vector>
#include <string>
#include <list>
#include <set>

#include "data_structures/overlap.hpp"
#include "data_structures/overlap_graph.hpp"
#include "data_structures/sequence.hpp"
#include "modules/module.hpp"


class ModuleHam : public Module
{

private:

	std::string m_index_prefix;
	std::vector< std::string > m_fasta_files; // vector of fasta filenames (one for each pool)

	std::vector< std::list<Overlap> > m_cc2blocks;
	std::list< OverlapGraph* > m_og_pool;
	std::list< Unitig > m_unitigs;

	std::list< Overlap > m_overlaps;

	void check_input_files();


public:

	ModuleHam() {}
	virtual ~ModuleHam() {}

	void execute( const Options &opt );

}; // end of class ModuleFilter

#endif /* MODULE_HAM_HPP */
