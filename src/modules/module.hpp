#ifndef MODULE_HPP
#define MODULE_HPP

#include "options/options.hpp"


class Module {
public:
	Module() { }
	virtual ~Module() { }

	virtual void execute( const Options & option ) = 0;
};

#endif /* MODULE_HPP */
