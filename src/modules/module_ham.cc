#include "module_ham.hpp"

#include <iostream>
#include <sstream>
#include <fstream>

#include <google/sparse_hash_map>

#include "common/algorithms.hpp"
#include "common/boost_includes.hpp"
#include "common/function_stats.hpp"
#include "common/ham_index.hpp"
#include "common/utils.hpp"
#include "errors/exit_exception.hpp"
#include "data_structures/contig_graph.hpp"
#include "data_structures/kmer_collection.hpp"
#include "data_structures/sequence_collection.hpp"

#include "fingerprint_aligner.hpp"
#include "fingerprint_builder.hpp"

#include "graph/string_graph.hpp"
#include "graph/contrem_visitor.hpp"
#include "graph/graphviz_visitor.hpp"
#include "graph/prune_visitor.hpp"
#include "graph/tred_visitor.hpp"
using namespace ham::graph;

void ModuleHam::execute( const Options &opt )
{
    // load fasta filenames
    load_filenames( opt.m_fasta_list, m_fasta_files );

    // check input files existence
    std::cout << "[ham] checking input files..." << std::flush;
    this->check_input_files();
	std::cout << "done." << std::endl;

	// load pools' sequences
    std::cout << "[ham] loading sequences..." << std::flush;
    SequenceCollection sc(m_fasta_files,opt.m_min_seq_size);
	std::cout << "done." << std::endl;

	if( opt.m_olp_file != "" ) // load an overlap file
	{
		std::cout << "[ham] loading overlap file..." << std::flush;
		std::ifstream ifs( opt.m_olp_file.c_str() );

		if( not ifs.good() )
		{
			std::cerr << "[error] cannot open overlap file \"" << opt.m_olp_file << "\"" << std::endl;
			throw ExitException(EXIT_FAILURE);
		}

		overlap_load_from_file( ifs, sc, m_overlaps );
	}
	else // otherwise, compute overlaps performing fingerprint alignments
	{
		//***** Build and load FM-Index (bwa's implementation)

		if( not opt.m_load_index ) // if index is not provided, build it on disk
		{
			std::cout << "[ham] building index..." << std::flush;
			{
				FunctionStats fstats("ham_idx_build");
				m_index_prefix = opt.m_output_prefix + ".hamidx";
				ham_idx_build( sc, m_index_prefix );
			}
			std::cout << "done." << std::endl;
		}
		else // otherwise, set path's prefix of the index to load from disk
		{
			m_index_prefix = opt.m_index_prefix;
		}

		std::cout << "[ham] loading index..." << std::flush;
		void *ham_index = NULL;
		{
			FunctionStats fstats("ham_idx_load");
			ham_idx_load( &ham_index, m_index_prefix );
		}
		std::cout << "done." << std::endl;

		//***** Build k-mer collection

		KmerCollection kc;

		std::string kc_filename = opt.m_index_prefix + ".kc";
		boost_fs::path kc_path( kc_filename.c_str() );
		if( boost_fs::exists(kc_path) and boost_fs::is_regular_file(kc_path) ) // if an kmer collection is available, load it from disk
		{
			std::cout << "[ham] loading kmer collection..." << std::flush;
			kc.load(kc_filename);
			std::cout << "done. " << kc.size() << " kmers loaded." << std::endl;
		}
		else // otherwise, compute it on-the-fly and, if debug flag is enabled, store it on disk
		{
		    std::cout << "[ham] computing kmer collection..." << std::flush;
			{
				FunctionStats fstats("kmer_collection_load");
				kmer_collection_load(kc,sc);
			}
		    std::cout << "done. " << kc.size() << " kmers loaded." << std::endl;

		    if( opt.m_debug )
		    {
		    	std::cout << "[ham] writing kmer collection..." << std::flush;
			    std::string kc_out = opt.m_output_prefix + ".hamidx.kc";
			    kc.store(kc_out);
			    std::cout << "done." << std::endl;
		    }
		}

		//***** Compute Fingerprints

		std::cout << "[ham] computing fingerprints..." << std::flush;
		std::vector<Fingerprint> fingerprints;
		{
			FunctionStats fstats("fingerprints_build");

			FingerprintBuilder fpb( opt, sc, kc, fingerprints );
			fpb.fingerprints_build();

			size_t fp_computed = 0;
			size_t fp_sizes = 0;

			for( const Fingerprint &fp : fingerprints ) 
			{
				if( fp.kmers() > 0 )
				{ 
					fp_sizes += fp.kmers(); 
					fp_computed++; 
				}
			}

			std::cout << fp_computed << "/" << fingerprints.size() << " computed (mean size = " <<
					(fp_computed > 0 ? fp_sizes/fp_computed : 0) << ")." << std::endl;
		}

		//***** Align Fingerprints

		std::cout << "[ham] aligning fingerprints..." << std::flush;
		{
			FunctionStats fstats("fingerprints_align");

			FingerprintAligner fpa( opt, ham_index, sc, fingerprints );
			fpa.fingerprints_align();

			m_overlaps = std::move(fpa.get_overlaps());
		}
		std::cout << "done." << std::endl;

		//***** Destroy FM-index (no more necessary)

		std::cout << "[ham] destroying index..." << std::flush;
		ham_idx_destroy(ham_index);
		std::cout << "done." << std::endl;
	}

	// END OF OVERLAP COMPUTATION/LOAD

	//***** Build Overlap Graph from overlaps

	string_graph<> sg;

	std::cout << "[main] building string graph..." << std::flush;
	{
		FunctionStats fstats("build_string_graph");
		sg = build_string_graph( m_overlaps, sc, opt.m_max_tail, opt.m_min_ol );
	}
	std::cout << "done." << std::endl;

	std::cout << "[main] removing contained sequences..." << std::flush;
	{
		FunctionStats fstats("remove_contained_vertices");
		contrem_visitor cv;
		sg.accept(cv);
	}
	std::cout << "done." << std::endl;

	std::cout << "[main] removing transitive edges..." << std::flush;
	{
		FunctionStats fstats("remove_transitive_edges");
		tred_visitor tv;
		sg.accept(tv);
	}
	std::cout << "done." << std::endl;

	std::cout << "[main] trimming dead-end paths..." << std::flush;
	{
		FunctionStats fstats("trim_deadends");
		prune_visitor dev( sc, opt.m_output_prefix.c_str() );
		sg.accept(dev);
	}
	std::cout << "done." << std::endl;

	std::cout << "[main] writing graphviz..." << std::flush;
	{
		FunctionStats fstats("write_graphviz");
		graphviz_visitor gv(sc,"./graphs/sg");
		sg.accept(gv);
	}
	std::cout << "done." << std::endl;

} // end of execute



void ModuleHam::check_input_files()
{
    // check existence of each provided fasta file
    for( size_t i=0; i < m_fasta_files.size(); i++ )
    {
        boost_fs::path p( m_fasta_files.at(i).c_str() );

        if( (not boost_fs::exists(p)) or (not boost_fs::is_regular_file(p)) )
        {
            std::cerr << "\n[error] fasta file \"" << m_fasta_files.at(i) << "\" does not exist." << std::endl;
            throw ExitException(2);
        }
    }
} // check_input_files
