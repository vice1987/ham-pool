/*
 * File:   fingerprint_aligner.hpp
 * Author: riccardo
 *
 * Created on January 9, 2014, 6:35 PM
 */

#ifndef FINGERPRINT_ALIGNER_HPP
#define FINGERPRINT_ALIGNER_HPP

#include <vector>

#include <google/dense_hash_map>
#include <boost/thread/mutex.hpp>

#include "common/types.hpp"
#include "data_structures/overlap.hpp"
#include "data_structures/fingerprint.hpp"
#include "data_structures/sequence_collection.hpp"
#include "options/options.hpp"

#include "3d_party/bwa/ksw.h"


class FingerprintAligner
{

public:

	typedef std::vector<Fingerprint> FingerprintVector;

public:

	FingerprintAligner(
	    const Options &opt,
	    void *index,
	    const SequenceCollection &sc,
	    const FingerprintVector &fingerprints );

	void fingerprints_align();

	std::list<Overlap>& get_overlaps() { return m_al; }

private:

	struct HitPair
	{
		int32_t sid;
		int32_t rid;
		size_t  hits_num;
		bool is_rev;
		//bool is_kmer_orderd;
		size_t brks;

		bool operator < (const HitPair &a) const
		{
			return sid < a.sid || (sid == a.sid && rid < a.rid );
		}
	};

	static void fingerprints_align_thread( FingerprintAligner *fp_aligner, int32_t tid );
	static bool is_order_consistent( const std::vector<KmerHit> &hits, size_t beg, size_t end );
	static size_t compute_order_errors( const std::vector<KmerHit> &hits, size_t beg, size_t end );

	static bool max_uniq_overlap( Overlap &olp, const Fingerprint &fp, std::vector<KmerHit> &hits, size_t beg, size_t end, 
		                          size_t rlen, size_t qlen, int32_t t_gap, int32_t t_ol );

	// a = match score, b = mismatch penalty, c = match with unknown base score
	static void init_sw_mat(int8_t mat[25], int8_t a = 1, int8_t b = 3, int8_t c = 0)
	{
		int i, j, k;
		for (i = k = 0; i < 4; ++i) {
			for (j = 0; j < 4; ++j)
				mat[k++] = i == j? a : -b;
			mat[k++] = c; // unknown base
		}
		for (j = 0; j < 5; ++j) mat[k++] = c; // unknown base
	}

	void dump_string( std::string s )
	{
		boost::mutex::scoped_lock lock(m_cerr_mutex);
		std::cerr << s << std::endl;
	}

private:

	// input data
	const Options &m_opt;
	void *m_index;
	const SequenceCollection &m_sc;
	const FingerprintVector &m_fingerprints;

	// local data
	std::list< Overlap > *m_hpl;
	boost::mutex m_cerr_mutex;

	// output data
	std::list< Overlap > m_al;

};

#endif  /* FINGERPRINT_ALIGNER_HPP */

