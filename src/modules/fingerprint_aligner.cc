#include "fingerprint_aligner.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <chrono>

#include "3d_party/bwa/bwamem.h"
#include "3d_party/bwa/ksw.h"
#include "common/boost_includes.hpp"
#include "common/constants.hpp"
#include "common/algorithms.hpp"

FingerprintAligner::FingerprintAligner(
    const Options &opt,
    void *index,
    const SequenceCollection &sc,
    const FingerprintVector &fingerprints )
	:
	m_opt( opt ),
	m_index( index ),
	m_sc( sc ),
	m_fingerprints( fingerprints ),
	m_hpl( NULL )
{

}


void FingerprintAligner::fingerprints_align()
{
	m_hpl = new std::list<Overlap>[m_opt.m_threads_num];

	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	// create the desired number of threads, each one aligning a different set of sequences
	// against the index of all pool sequences
	boost::thread_group threads;
	for( int32_t tid = 0; tid < m_opt.m_threads_num; ++tid )
	{
		threads.create_thread( boost::bind( &this->fingerprints_align_thread, this, tid ) );
	}
	threads.join_all();

	end = std::chrono::system_clock::now();

	std::chrono::duration<double> aln_time = end - start;
	std::cout << "time " << aln_time.count() << "s...";

	// collect threads`data
	for( int32_t tid = 0; tid < m_opt.m_threads_num; ++tid )
	{
		std::list<Overlap> &l = m_hpl[tid];
		for( Overlap &hp : l ) m_al.push_back( hp );
	}

	m_al.sort();

	// write statistics in output
	std::cout << "writing stats to output..." << std::flush;
	std::string out_stats_file = m_opt.m_output_prefix + ".fplinks";
	std::ofstream ofs( out_stats_file.c_str() );

	for( auto &hp : m_al )
		ofs <<  m_sc.at( hp.qid ).pid() << "." << m_sc.at( hp.qid ).sid() << ";" <<
		    m_sc.at( hp.rid ).pid() << "." << m_sc.at( hp.rid ).sid() << "\t" <<
		    hp.hits_num << "\t" << (hp.is_rev ? '-' : '+') << "\t" <<
		    "(" << (hp.q_end - hp.q_beg) << ")\t" << hp.q_beg << "\t" << hp.q_end << "\t" << m_sc.at( hp.qid ).length() << "\t" <<
		    "(" << (hp.r_end - hp.r_beg) << ")\t" << hp.r_beg << "\t" << hp.r_end << "\t" << m_sc.at( hp.rid ).length() << "\t" <<
		    hp.sw_score << "/" << (100.0 * hp.ident) << "%\n";

	ofs.close();

	// write statistics in output
	out_stats_file = m_opt.m_output_prefix + ".olp";
	ofs.open( out_stats_file.c_str() );

	for( auto &hp : m_al )
		ofs <<  m_sc.at( hp.qid ).pid() << "\t" << m_sc.at( hp.qid ).sid() << "\t" <<
		    m_sc.at( hp.rid ).pid() << "\t" << m_sc.at( hp.rid ).sid() << "\t" <<
		    (hp.is_rev ? '-' : '+') << "\t" <<
		    hp.q_beg << "\t" << hp.q_end << "\t" <<
		    hp.r_beg << "\t" << hp.r_end << "\t" <<
		    hp.ident << "\n";

	ofs.close();

	delete[] m_hpl;
}


void FingerprintAligner::fingerprints_align_thread( FingerprintAligner *fpa, int32_t tid )
{
	// get index's pointer and default alignment options
	bwaidx_t *idx = static_cast<bwaidx_t *>( fpa->m_index );
	mem_opt_t *mem_opt = mem_opt_init();

	assert( idx != NULL ); // assert bwa index has been loaded

	int32_t sequences = static_cast<int32_t>( ( fpa->m_fingerprints ).size() );
	int32_t n_threads = ( fpa->m_opt ).m_threads_num;

	for( int32_t sid = tid; sid < sequences; sid += n_threads )  // for each fingerprint assigned to the thread
	{
		const Fingerprint &fp = fpa->m_fingerprints[static_cast<size_t>( sid )];
		size_t kmers = fp.kmers();

		std::vector<KmerHit> hits; // vector which stores the hits of all k-mers in the fingerprint

		// align k-mers and find hits against sequences with greater id
		for( size_t kid = 0; kid < kmers; ++kid )
		{
			std::string kmer_str = fp.kmer_at( kid ).to_string(); // compute k-mer string

			mem_alnreg_v ar = mem_align1( mem_opt, idx->bwt, idx->bns, idx->pac,
			                              static_cast<int>( kmer_str.length() ), kmer_str.c_str() ); // get hits of k-mer fp[kid]

			for( size_t h = 0; h < ar.n; ++h )  // for each BWA-MEM`s hit
			{
				// if non-exact matching (i.e., score < k), skip it
				if( ar.a[h].score < static_cast<int>( Kmer::length() ) ) continue;

				// retrieve reference id, mapping position, and whether it is forward or reverse
				mem_aln_t a = mem_reg2aln( mem_opt, idx->bns, idx->pac,
				                           static_cast<int>( kmer_str.length() ), kmer_str.c_str(),
				                           &ar.a[h] );

				// skip hits against sequences with smaller (or equal) pool id and hits between different sequences
				if( ( fpa->m_sc ).at( a.rid ).pid() <= ( fpa->m_sc ).at( sid ).pid() || a.pos+kmer_str.length() > (fpa->m_sc).at(a.rid).length() )
				{
					free( a.cigar );
					continue;
				}

				// push it in the hits vector
				KmerHit khit( static_cast<int32_t>( kid ), a.rid, static_cast<int32_t>( a.pos ), a.is_rev > 0 );
				hits.push_back( khit );

				free( a.cigar );
			}

			free( ar.a ); // deallocate BWA-MEM`s hit list
		}

		std::sort( hits.begin(), hits.end() ); // sort hits vector first by ref_id and then by k-mer id

		std::vector<KmerHit> uniq_hits;

		size_t sha_kmers = 0; // shared k-mers between fingerprint and sequence
		size_t prev_beg  = 0;
		size_t prev_uniq_beg = 0;

		Overlap fp_olp;

		// filter hits vector, removing ambiguous hits
		for( size_t i = 0; i < hits.size(); )
		{
			// find position j of the first consecutive hit with different (greater) kid or different rid
			size_t j = i + 1;
			while( j < hits.size() && hits[i].rid == hits[j].rid && hits[i].kid == hits[j].kid )
			{
				hits[i].is_mult = true;
				hits[j].is_mult = true;
				j++;
			}

			uniq_hits.push_back( hits[i] );
			i += hits[i].is_mult ? j - i : 1;

			sha_kmers++;

			if( j >= hits.size() || ( j < hits.size() && hits[prev_beg].rid != hits[j].rid ) ) // found all unique hits against a sequence, check for overlap
			{
				int32_t rid = hits[prev_beg].rid;

				const Sequence& ref   = (fpa->m_sc).at(rid);
				const Sequence& query = (fpa->m_sc).at(sid);

				bool ol_found = max_uniq_overlap( fp_olp, fp, uniq_hits, prev_uniq_beg, uniq_hits.size(), 
					                              ref.length(), query.length(), 
					                              (fpa->m_opt).m_max_gap, (fpa->m_opt).m_min_ol );

				// DEBUG:
				/*if( (fpa->m_opt).m_debug and query.str_id() == "0.23" and (ref.str_id() == "187.46" or ref.str_id() == "207.47") )
				{
					std::cerr << "[debug] max_uniq_overlap " << query.str_id() << ";" << ref.str_id() << " found=" << ol_found << std::endl;
					std::cerr << "        ref   region: [" << fp_olp.r_beg << "," << fp_olp.r_end << "); ref   len = " << ref.length() << "\n"
					          << "        query region: [" << fp_olp.q_beg << "," << fp_olp.q_end << "); query len = " << query.length() << std::endl;

					std::cerr << "[debug] uniq_hits=" << uniq_hits.size()-prev_uniq_beg << "\n";
					for( size_t pd=prev_uniq_beg; pd < uniq_hits.size(); ++pd ) 
						std::cerr << uniq_hits[pd].kid << "[" << fp.gap_at(uniq_hits[pd].kid) << "," << uniq_hits[pd].beg << ")" << 
							(uniq_hits[pd].is_rev ? '-' : '+') << " " << (uniq_hits[pd].is_mult ? 'm' : ' ') << "\n";
					std::cerr << std::endl;
				}*/

				if( ol_found and sha_kmers >= ( fpa->m_opt ).m_min_shared_k )
				{
					// fill missing data
					fp_olp.rid = rid;
					fp_olp.qid = sid;
					fp_olp.hits_num = sha_kmers;

					// debug
					if( rid <= sid ) std::cerr << "[warning] ref id less or equal than query id, it should not be possible." << std::endl;

					// align tails
					base_t *qp, *rp;
					int32_t qoff,roff,qlen,rlen;
					int8_t mat[25]; init_sw_mat(mat);
					int xtra = KSW_XSTART;

					qp = const_cast<base_t*>(query.data());
					rp = const_cast<base_t*>(ref.data());

					if( fp_olp.is_rev )
					{
						rp = new base_t[ref.length()]; // TODO: sarebbe meglio allocare una volta sola il reverse della query (invece che ogni volta la sequenza reference)
						memcpy( rp, ref.data(), ref.length() );
						reverse_complement( rp, ref.length() );
						reverse_coordinates( fp_olp.r_beg, fp_olp.r_end, static_cast<int32_t>(ref.length()) );
					}

					//std::cerr << "fpalign: \n\tq = " << fp_olp.q_beg << " " << fp_olp.q_end << " " << query.length();
					//std::cerr << "\n\tr = " << fp_olp.r_beg << " " << fp_olp.r_end << " " << ref.length() << std::endl;

					// left tail
					qoff = std::max( 0, fp_olp.q_beg - static_cast<int32_t>(1.5*fp_olp.r_beg) );
					roff = std::max( 0, fp_olp.r_beg - static_cast<int32_t>(1.5*fp_olp.q_beg) );
					qlen = fp_olp.q_beg-qoff;
					rlen = fp_olp.r_beg-roff;
					
					if( qlen > 0 && rlen > 0 )
					{
						//std::cerr << "aligning left tail: \n\tq = " << qoff << " " << qlen << " " << query.length();
						//std::cerr << "\n\tr = " << roff << " " << rlen << " " << ref.length() << std::endl;

						// debug
						//if( qoff < 0 or qoff >= query.length() ) std::cerr << "left tail align: out of bound offset in query" << " rev? " << fp_olp.is_rev << std::endl;
						//if( roff < 0 or roff >= ref.length() ) std::cerr << "left tail align: out of bound offset in ref" << " rev? " << fp_olp.is_rev << std::endl;
						//if( qoff+qlen-1 < 0 or qoff+qlen-1 >= query.length() ) std::cerr << "left tail align: out of bound qlen in query" << " rev? " << fp_olp.is_rev << std::endl;
						//if( roff+rlen-1 < 0 or roff+rlen-1 >= ref.length() ) std::cerr << "left tail align: out of bound rlen in ref" << " rev? " << fp_olp.is_rev << std::endl;

						kswr_t a = ksw_align( qlen, qp+qoff, rlen, rp+roff, 5, mat, 5, 1, xtra, 0);

						// update fp_olp regions
						fp_olp.q_beg = qoff + a.qb;
						fp_olp.r_beg = roff + a.tb;
					}

					// right tail
					qoff = fp_olp.q_end;
					roff = fp_olp.r_end;
					qlen = std::min( static_cast<int32_t>(query.length())-qoff, static_cast<int32_t>((static_cast<int32_t>(ref.length())-roff)*1.5) );
					rlen = std::min( static_cast<int32_t>(ref.length())-roff, static_cast<int32_t>((static_cast<int32_t>(query.length())-qoff)*1.5) );
					
					if( qlen > 0 && rlen > 0 )
					{
						//std::cerr << "aligning right tail: \n\tq = " << qoff << " " << qlen << " " << query.length();
						//std::cerr << "\n\tr = " << roff << " " << rlen << " " << ref.length() << std::endl;

						// debug
						//if( qoff < 0 or qoff >= query.length() ) std::cerr << "right tail align: out of bound offset in query" << " rev? " << fp_olp.is_rev << std::endl;
						//if( roff < 0 or roff >= ref.length() ) std::cerr << "right tail align: out of bound offset in ref" << " rev? " << fp_olp.is_rev << std::endl;
						//if( qoff+qlen-1 < 0 or qoff+qlen-1 >= query.length() ) std::cerr << "right tail align: out of bound qlen in query" << " rev? " << fp_olp.is_rev << std::endl;
						//if( roff+rlen-1 < 0 or roff+rlen-1 >= ref.length() ) std::cerr << "right tail align: out of bound rlen in ref" << " rev? " << fp_olp.is_rev << std::endl;

						kswr_t a = ksw_align( qlen, qp+qoff, rlen, rp+roff, 5, mat, 5, 1, xtra, 0);

						// update fp_olp regions
						fp_olp.q_end = qoff + a.qe + 1; // +1 makes q_end exclusive
						fp_olp.r_end = roff + a.te + 1; // +1 makes r_end exclusive
					}

					qoff = fp_olp.q_beg;
					roff = fp_olp.r_beg;
					qlen = fp_olp.q_end - fp_olp.q_beg;
					rlen = fp_olp.r_end - fp_olp.r_beg;

					//std::cerr << "global: \n\tq = " << qoff << " " << qlen << " " << query.length();
					//std::cerr << "\n\tr = " << roff << " " << rlen << " " << ref.length() << std::endl;

					// debug
					//if( qoff < 0 or qoff >= query.length() ) std::cerr << "ksw_global: out of bound offset in query" << qoff << " vs " << query.length() << " rev? " << fp_olp.is_rev << std::endl;
					//if( roff < 0 or roff >= ref.length() ) std::cerr << "ksw_global: out of bound offset in ref" << roff << " vs " << ref.length() << " rev? " << fp_olp.is_rev << std::endl;
					//if( qoff+qlen-1 < 0 or qoff+qlen-1 >= query.length() ) std::cerr << "ksw_global: out of bound qlen in query" << qoff+qlen-1 << " vs " << query.length() << " rev? " << fp_olp.is_rev << std::endl;
					//if( roff+rlen-1 < 0 or roff+rlen-1 >= ref.length() ) std::cerr << "ksw_global: out of bound rlen in ref: " << roff+rlen-1 << " vs " << ref.length() << " rev? " << fp_olp.is_rev << std::endl;

					// use ksw_global to check for a real alignment
					long ldiff = std::abs(qlen-rlen);
					int n_cigar = 0;
					uint32_t *cigar = 0;
					fp_olp.sw_score = ksw_global( qlen, qp+qoff, rlen, rp+roff, 5, mat, 5, 1, 50 > ldiff*1.5 ? 50 : ldiff*1.5, &n_cigar, &cigar);

					// process cigar
					int es_len = 0, es_m = 0;
					for( int k=0, i=qoff, j=roff; k < n_cigar; ++k ) // print CIGAR
					{
						switch("MIDSH"[cigar[k]&0xf])
						{
							case 'M':
								for( int z=0; z < cigar[k]>>4; ++z, ++i, ++j ) if( qp[i] == rp[j] ) es_m++;
								break;
							case 'I':
								i += cigar[k]>>4;
								break;
							case 'D':
								j += cigar[k]>>4;
								break;
							default:
								break;
						}
						
						es_len += cigar[k]>>4;
					}

					fp_olp.ident = es_len > 0 ? static_cast<double>(es_m)/es_len : 0.0;

					// free cigar
					free(cigar);

					int32_t ltail = std::min( fp_olp.q_beg, fp_olp.r_beg );
					int32_t rtail = std::min( static_cast<int32_t>(query.length())-fp_olp.q_end, static_cast<int32_t>(ref.length())-fp_olp.r_end );

					if( fp_olp.is_rev )
					{
						// update coordinates of the alignment w.r.t. the original contig orientation
						reverse_coordinates( fp_olp.r_beg, fp_olp.r_end, static_cast<int32_t>(ref.length()) );
						// free allocated memory for the reverse complement contig
						delete[] rp;
					}

					
					if( fp_olp.ident >= (fpa->m_opt).m_min_ident and ltail <= (fpa->m_opt).m_max_tail and rtail <= (fpa->m_opt).m_max_tail ) 
					{
						(fpa->m_hpl[tid]).push_back( std::move(fp_olp) );
					}
				}

				sha_kmers = 0;
				prev_beg  = j;
				prev_uniq_beg = uniq_hits.size();
			}
		}

	} // end of fingerprint alignment

	free( mem_opt );
}





bool FingerprintAligner::is_order_consistent( const std::vector<KmerHit> &hits, size_t beg, size_t end )
{
	if( end < beg + 2 ) return true;  // if there are less than 2 elements in the region

	bool fwd = hits[beg].kid < hits[beg + 1].kid;

	size_t cur, prev;
	for( cur = beg + 2, prev = beg + 1; cur < end; cur++, prev++ )
		if( ( fwd && hits[cur].kid < hits[prev].kid ) or
		        ( !fwd && hits[cur].kid > hits[prev].kid ) ) return false;

	return true;
}


bool FingerprintAligner::max_uniq_overlap( Overlap &fp_olp, const Fingerprint &fp, std::vector<KmerHit> &hits, size_t beg, size_t end, 
	size_t rlen, size_t qlen, int32_t t_gap, int32_t t_ol )
{
	// if there are less than 2 k-mer hits, don't check for putative overlap
	if( end < beg + 2 ) return false;

	int32_t k = static_cast<int32_t>( Kmer::length() );
	
	// compute number of fwd/rev unique hits
	size_t fwd_hits{0}, rev_hits{0};
	std::for_each( hits.begin()+beg, hits.begin()+end, 
		[&](KmerHit& hit)
		{ 
			hit.is_rev ? rev_hits++ : fwd_hits++; 
		});

	// if putative overlap with the reverse complement of the reference sequence
	if( rev_hits > fwd_hits )
		std::for_each( hits.begin()+beg, hits.begin()+end, 
				[=](KmerHit& hit) // reverse coordinates
				{ 
					hit.is_rev = !hit.is_rev;
					hit.beg = rlen - hit.beg - k; 
				});

	// seek for a putative overlap
	int32_t max_miss = 3;
	int32_t max_tail = max_miss*k + t_gap*(max_miss+1);
	int32_t min_ol = std::max( t_ol - 2*t_gap, t_ol/2 );
	int32_t cur, prev;
	cur = beg; while( cur < end && hits[cur].is_mult ) cur++;

	// if uniquely mapped kmers are absent, return false
	if( cur >= end ) return false;

	int32_t ref_start = hits[cur].beg;
	int32_t ref_end   = ref_start + k;
	int32_t q_start   = static_cast<int32_t>( fp.gap_at(hits[cur].kid) );
	int32_t q_end     = q_start + k;
	
	int32_t best_reg = ref_end - ref_start;
	int32_t best_lt  = std::min( ref_start, q_start );
	int32_t best_rt  = std::min( rlen-ref_end, qlen-q_end );

	// initialize putative overlap with the first uniquely-mapped k-mer
	fp_olp.r_beg = ref_start;
	fp_olp.r_end = ref_end;
	fp_olp.q_beg = q_start;
	fp_olp.q_end = q_end;
	
	fp_olp.is_rev = rev_hits > fwd_hits; // different from the older version

	bool found = false;

	for( prev = cur, cur += 1; cur < end; prev = cur, cur += 1 )
	{
		while( cur < end && hits[cur].is_mult ) cur++;  // find next unique hit

		if( cur >= end ) break;

		// if more than "max_miss" k-mers are missing between two consecutive unique hits (or if the orientation is not consistent)
		if( cur - prev + max_miss < hits[cur].kid - hits[prev].kid || hits[prev].is_rev != hits[cur].is_rev ) 
		{
			int32_t left_tail = std::min( ref_start, q_start );
			int32_t right_tail = std::min( rlen-ref_end, qlen-q_end );

			//long diff = std::abs((q_end-q_start)-(r_end-r_start));
			
			// possibly update best putative overlap found
			if( left_tail+right_tail < best_lt+best_rt or (left_tail+right_tail == best_lt+best_rt and ref_end-ref_start > best_reg) )
			{
				best_reg = ref_end-ref_start;
				best_lt  = std::min( ref_start, q_start );
				best_rt  = std::min( rlen-ref_end, qlen-q_end );

				fp_olp.r_beg = ref_start;
				fp_olp.r_end = ref_end;
				fp_olp.q_beg = q_start;
				fp_olp.q_end = q_end;
				//fp_olp.is_rev = hits[prev].is_rev;

				if( best_lt <= max_tail and best_rt <= max_tail and best_reg+best_lt+best_rt >= t_gap ) found = true; // a putative overlap has been found
				//if( best_reg >= min_ol ) found = true; // a putative overlap has been found
			}

			// initialize new (putative) overlap region
			ref_start = hits[cur].beg;
			ref_end   = ref_start + k;
			q_start   = static_cast<int32_t>( fp.gap_at(hits[cur].kid) );
			q_end     = q_start + k;
		}
		else // extend previous region
		{
			ref_start = std::min( ref_start, hits[cur].beg );
			ref_end   = std::max( ref_end, hits[cur].beg + k );
			q_start   = std::min( q_start, static_cast<int32_t>(fp.gap_at(hits[cur].kid)) );
			q_end     = std::max( q_end, static_cast<int32_t>(fp.gap_at(hits[cur].kid)+k) );
		}
	}

	// possibly update best putative with the last found region
	int32_t left_tail = std::min( ref_start, q_start );
	int32_t right_tail = std::min( rlen-ref_end, qlen-q_end );
	
	if( left_tail+right_tail < best_lt+best_rt or (left_tail+right_tail == best_lt+best_rt and ref_end-ref_start > best_reg) )
	{
		best_reg = ref_end-ref_start;
		best_lt  = std::min( ref_start, q_start );
		best_rt  = std::min( rlen-ref_end, qlen-q_end );

		fp_olp.r_beg = ref_start;
		fp_olp.r_end = ref_end;
		fp_olp.q_beg = q_start;
		fp_olp.q_end = q_end;
		//fp_olp.is_rev = hits[prev].is_rev;

		if( best_lt <= max_tail and best_rt <= max_tail and best_reg+best_lt+best_rt >= t_gap ) found = true; // a putative overlap has been found
		//if( best_reg >= min_ol ) found = true; // a putative overlap has been found
	}

	if( rev_hits > fwd_hits )
		reverse_coordinates( fp_olp.r_beg, fp_olp.r_end, rlen );

	return found;
}


size_t FingerprintAligner::compute_order_errors( const std::vector<KmerHit> &hits, size_t beg, size_t end )
{
	if( end < beg + 2 ) return 0;  // if there are less than 2 elements

	size_t gap = 0;
	size_t brk = 0;

	bool fwd = hits[beg].kid <= hits[beg + 1].kid;

	size_t cur, prev;
	for( cur = beg + 2, prev = beg + 1; cur < end; cur++, prev++ )
	{
		if( fwd )  // forward mapping
		{
			if( hits[cur].kid < hits[prev].kid ) brk++;  // break
			if( hits[prev].kid + 1 < hits[cur].kid ) gap++;  // gap (missing k-mers)

			fwd = hits[prev].kid <= hits[cur].kid;
		}
		else // reverse mapping
		{
			if( hits[cur].kid > hits[prev].kid ) brk++;  // break
			if( hits[prev].kid - 1 > hits[cur].kid ) gap++;  // gap (missing k-mers)

			fwd = hits[prev].kid < hits[cur].kid;
		}
	}

	return brk;
}


// TODO:FIX INCLUSIONS, WHICH ARE REPORTED AS "INVALID" ALIGNMENTS
//bool FingerprintAligner::is_valid_alignment( kswr_t kswr, size_t qlen, size_t tlen )
//{
//  int qr = kswr.qe - kswr.qb + 1;
//  int tr = kswr.te - kswr.tb + 1;
//
//  if( kswr.score == qr && kswr.score == tr )
//  {
//      // check if it is an overlap
//      if( kswr.qb == 0 && (kswr.te == tlen-1 || kswr.qe == qlen-1) ) return true;
//      if( kswr.tb == 0 && (kswr.qe == qlen-1 || kswr.te == tlen-1) ) return true;
//  }
//
//  return false;
//}
